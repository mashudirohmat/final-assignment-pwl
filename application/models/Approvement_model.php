<?php 
/**
*Progammer	: Mashudi Rohmat & Bambang Wijayanto
*Projek 	: Manajemen Keuangan Proyek 2020
*Desc 		: Mode untuk manipulasi approvement
*/
class Approvement_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function insert($data) {
		return $this->db->insert('aksi_request_pengeluaran',$data);
	}

	public function get_request($params) {
		$this->db->select('ARP.*');
		$this->db->select('A.username AS username');
		$this->db->from('aksi_request_pengeluaran ARP');
		$this->db->join('admin A','ARP.id_admin = A.id_user','LEFT',TRUE);
		$this->db->where('ARP.id_proyek',$params);
		$sql = $this->db->get()->result_array();
		return $sql;
	}

	public function get_request_byid($params) {
		$this->db->select('*');
		$this->db->from('aksi_request_pengeluaran');
		$this->db->where('id_request',$params);
		$sql = $this->db->get()->result_array();

		return $sql;
	}

	public function delete($params) {
		$this->db->where('id_request',$params);
		$this->db->delete('aksi_request_pengeluaran');
	}
}

 ?>