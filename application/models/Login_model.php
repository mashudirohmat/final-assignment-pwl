<?php 
/**
*Progammer : Mashudi Rohmat & Bambang Wijayanto
*Projek : Manajemen Keuangan Proyek 2020
*Desc : Model untuk olah authentikasi
*/
class Login_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function cekUser_model($data,$tabel)
	{
		$this->db->select('*');
		$this->db->from($tabel);
		$this->db->where('username',$data['username']);
		$this->db->where('password',$data['password']);
		$query= $this->db->get();
		return $query->result_array();
	}
}

 ?>