<?php 
/**
*Progammer : Mashudi Rohmat & Bambang Wijayanto
*Projek : Manajemen Keuangan Proyek 2020
*Desc : Model untuk manipulasi project
*/
class Project_model extends CI_Model
{
	
	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	function getAdmin_model(){
		$this->db->select('*');
		$this->db->from('admin');
		$query = $this->db->get();
		return $query;
	}

	function getMandor_model(){
		$this->db->select('*');
		$this->db->from('mandor');
		$query = $this->db->get();
		return $query;
	}

	function getAllProject_model($filter){
		if ($filter!='') {
			if ($filter['hakAkses']== 'admin') {
				$sql = "SELECT DISTINCT a.*,budget, admin, mandor
						FROM proyek a
						LEFT JOIN trans_admin_proyek b ON a.id_proyek = b.id_proyek
						LEFT JOIN (
							SELECT a.id_proyek, CONCAT('Rp ',FORMAT(SUM(pos_legal+pos_fasumfasos+pos_operasional+pos_promosi+pos_bangunan+pos_pajak),0)) as budget
							FROM proyek a
							LEFT JOIN budget b ON a.id_proyek = b.id_proyek
							GROUP BY a.id_proyek
						)c
						ON a.id_proyek = c.id_proyek
						LEFT JOIN (
							SELECT a.id_proyek, b.username as admin
							FROM trans_admin_proyek a
							LEFT JOIN admin b ON a.id_admin = b.id_user
						)d
						ON a.id_proyek = d.id_proyek
						LEFT JOIN (
							SELECT DISTINCT a.id_proyek, b.username as mandor
							FROM trans_mandor_proyek a
							LEFT JOIN mandor b ON a.id_mandor = b.id_user
						)e
						ON a.id_proyek = e.id_proyek
						LEFT JOIN trans_mandor_proyek f ON a.id_proyek = f.id_proyek
						WHERE a.is_archive = 0 AND b.id_admin = ".$filter['idUser']." 
						ORDER BY a.date DESC
						";
				$query = $this->db->query($sql)->result_array();
				// $this->db->select('p.*');
				// $this->db->from('proyek p');
				// $this->db->join('trans_admin_proyek tap','p.id_proyek = tap.id_proyek');
				// $this->db->where('p.is_archive',0);
				// $this->db->where('tap.id_admin',$filter['idUser']);
				// $this->db->order_by('p.nama_proyek');
				// $query = $this->db->get();
				return $query;
			}elseif($filter['hakAkses']== 'pimpro'){
				$sql = "SELECT DISTINCT a.*,budget, admin, mandor
						FROM proyek a
						LEFT JOIN trans_admin_proyek b ON a.id_proyek = b.id_proyek
						LEFT JOIN (
							SELECT a.id_proyek, CONCAT('Rp ',FORMAT(SUM(pos_legal+pos_fasumfasos+pos_operasional+pos_promosi+pos_bangunan+pos_pajak),0)) as budget
							FROM proyek a
							LEFT JOIN budget b ON a.id_proyek = b.id_proyek
							GROUP BY a.id_proyek
						)c
						ON a.id_proyek = c.id_proyek
						LEFT JOIN (
							SELECT a.id_proyek, b.username as admin
							FROM trans_admin_proyek a
							LEFT JOIN admin b ON a.id_admin = b.id_user
						)d
						ON a.id_proyek = d.id_proyek
						LEFT JOIN (
							SELECT DISTINCT a.id_proyek, b.username as mandor
							FROM trans_mandor_proyek a
							LEFT JOIN mandor b ON a.id_mandor = b.id_user
						)e
						ON a.id_proyek = e.id_proyek
						LEFT JOIN trans_mandor_proyek f ON a.id_proyek = f.id_proyek
						WHERE a.is_archive = 0 AND f.id_mandor = ".$filter['idUser']." 
						ORDER BY a.date DESC
						";
				$query = $this->db->query($sql)->result_array();

				// $this->db->select('p.*');
				// $this->db->from('proyek p');
				// $this->db->join('trans_mandor_proyek tmp','p.id_proyek = tmp.id_proyek');
				// $this->db->where('p.is_archive',0);
				// $this->db->where('tmp.id_mandor',$filter['idUser']);
				// $this->db->order_by('p.nama_proyek');
				// $query = $this->db->get();
				return $query;
			}
		}else{
			$sql = "SELECT DISTINCT a.*,budget, admin, mandor
						FROM proyek a
						LEFT JOIN trans_admin_proyek b ON a.id_proyek = b.id_proyek
						LEFT JOIN (
							SELECT a.id_proyek, CONCAT('Rp ',FORMAT(SUM(pos_legal+pos_fasumfasos+pos_operasional+pos_promosi+pos_bangunan+pos_pajak),0)) as budget
							FROM proyek a
							LEFT JOIN budget b ON a.id_proyek = b.id_proyek
							GROUP BY a.id_proyek
						)c
						ON a.id_proyek = c.id_proyek
						LEFT JOIN (
							SELECT a.id_proyek, b.username as admin
							FROM trans_admin_proyek a
							LEFT JOIN admin b ON a.id_admin = b.id_user
						)d
						ON a.id_proyek = d.id_proyek
						LEFT JOIN (
							SELECT DISTINCT a.id_proyek, b.username as mandor
							FROM trans_mandor_proyek a
							LEFT JOIN mandor b ON a.id_mandor = b.id_user
						)e
						ON a.id_proyek = e.id_proyek
						LEFT JOIN trans_mandor_proyek f ON a.id_proyek = f.id_proyek
						WHERE a.is_archive = 0
						ORDER BY a.date DESC
						";
				$query = $this->db->query($sql)->result_array();
				// echo "<pre>";
				// print_r($query);exit();
			// $this->db->select('p.*');
			// $this->db->from('proyek p');
			// $this->db->order_by('p.nama_proyek');
			// $this->db->where('p.is_archive',0);
			// $query = $this->db->get();
			return $query;
		}
	}

	function getArchiveProject_model(){
		$sql = "SELECT DISTINCT a.*,budget, admin, mandor
						FROM proyek a
						LEFT JOIN trans_admin_proyek b ON a.id_proyek = b.id_proyek
						LEFT JOIN (
							SELECT a.id_proyek, CONCAT('Rp ',FORMAT(SUM(pos_legal+pos_fasumfasos+pos_operasional+pos_promosi+pos_bangunan+pos_pajak),0)) as budget
							FROM proyek a
							LEFT JOIN budget b ON a.id_proyek = b.id_proyek
							GROUP BY a.id_proyek
						)c
						ON a.id_proyek = c.id_proyek
						LEFT JOIN (
							SELECT a.id_proyek, b.username as admin
							FROM trans_admin_proyek a
							LEFT JOIN admin b ON a.id_admin = b.id_user
						)d
						ON a.id_proyek = d.id_proyek
						LEFT JOIN (
							SELECT DISTINCT a.id_proyek, b.username as mandor
							FROM trans_mandor_proyek a
							LEFT JOIN mandor b ON a.id_mandor = b.id_user
						)e
						ON a.id_proyek = e.id_proyek
						LEFT JOIN trans_mandor_proyek f ON a.id_proyek = f.id_proyek
						WHERE a.is_archive = 1
						ORDER BY a.date DESC
						";
				$query = $this->db->query($sql)->result_array();
				// echo "<pre>";
				// print_r($query);exit();
			// $this->db->select('p.*');
			// $this->db->from('proyek p');
			// $this->db->order_by('p.nama_proyek');
			// $this->db->where('p.is_archive',0);
			// $query = $this->db->get();
		return $query;
	}

	function detailProject_model($id){
		$sql="SELECT proyek.*, DATE_FORMAT(date, '%d %M %Y') AS tanggal, IFNULL(lokasi,'Lokasi') AS lokasi
			FROM proyek
			WHERE id_proyek = ".$id."";
		$query = $this->db->query($sql)->result_array();
		return $query;
	}

	function getAdminByIdProyek_model($id){
		$this->db->select('tap.*');
		$this->db->select('a.*');
		$this->db->from('trans_admin_proyek tap');
		$this->db->join('admin a','tap.id_admin = a.id_user');
		$this->db->where('tap.id_proyek',$id);
		$query = $this->db->get()->result_array();
		return $query;
	}

	function getMandorByIdProyek_model($id){
		$this->db->select('tmp.*');
		$this->db->select('m.*');
		$this->db->from('trans_mandor_proyek tmp');
		$this->db->join('mandor m','tmp.id_mandor = m.id_user');
		$this->db->where('tmp.id_proyek',$id);
		$query = $this->db->get()->result_array();
		return $query;
	}

	function addProject_model($data){
		$query = $this->db->insert('proyek',array('nama_proyek' => $data['nama_proyek'],
												'lokasi' => $data['lokasi'],
												'foto' => $data['foto']
											)
									);
		if ($query) {
			$this->db->select('id_proyek');
			$this->db->from('proyek');
			$id_proyek = $this->db->get()->result_array();
			$max_id = max($id_proyek);
			for ($i=0; $i < count($data['id_admin']); $i++) { 
				
				$this->db->insert('trans_admin_proyek',array('id_admin' => $data['id_admin'][$i],
														'id_proyek' => $max_id['id_proyek']
													));
			}
			for ($i=0; $i < count($data['id_mandor']); $i++) { 
				$this->db->insert('trans_mandor_proyek',array('id_mandor' => $data['id_mandor'][$i],
														'id_proyek' => $max_id['id_proyek']
													));
			}
			return $query;
		}
	}

	function updateProject_model($where,$data){
		if ($data['foto']=='empty') {
			$this->db->where('id_proyek',$where);
			$query = $this->db->update('proyek',array('nama_proyek'=>$data['nama_proyek'],'lokasi'=>$data['lokasi'] ));

		}else{
			$this->db->where('id_proyek',$where);
			$query = $this->db->update('proyek',array('nama_proyek'=>$data['nama_proyek'],
				'lokasi'=>$data['lokasi'],
				'foto'=>$data['foto']
			)
		);
		}
		if ($query) {
			$this->db->where('id_proyek',$where);
			$this->db->delete('trans_admin_proyek');

			for ($i=0; $i < count($data['id_admin']); $i++) { 
				
				$this->db->insert('trans_admin_proyek',array('id_admin' => $data['id_admin'][$i],
					'id_proyek' => $where
				));
			}
			$this->db->where('id_proyek',$where);
			$this->db->delete('trans_mandor_proyek');
			for ($i=0; $i < count($data['id_mandor']); $i++) { 
				$this->db->insert('trans_mandor_proyek',array('id_mandor' => $data['id_mandor'][$i],
					'id_proyek' => $where
				));
			}
		}
	}

	function setPos_model($data){
		$query = $this->db->insert('budget',$data);
		return $query;
	}

	function getPos_model($id){
		$this->db->select('*');
		$this->db->from('budget');
		$this->db->where('id_proyek',$id);
		$query = $this->db->get()->result_array();
		return $query;
	}

	function updatePos_model($data){
		$this->db->where('id_proyek',$data['id_proyek']);
		$query = $this->db->update('budget',$data);
		return $query;
	}

	function Arsip_model($id){
		$this->db->where('id_proyek',$id);
		$query = $this->db->update('proyek',array('is_archive'=> 1));
		return $query;
	}
	//manipulasi rumah
	function getAllRumah_model($id){
		$sql="SELECT a.*,b.id_transaksi,b.nama_pembeli,b.is_lunas
			FROM rumah a
			LEFT JOIN transaksi_rumah b ON a.id_rumah = b.id_rumah
			WHERE a.is_archive = 0 AND a.id_proyek = ".$id."";
		$query = $this->db->query($sql)->result_array();
		return $query;
	}

	// function getAllRumah_model($id){
	// 	$this->db->select('*');
	// 	$this->db->from('rumah');
	// 	$this->db->where('is_archive',0);
	// 	$this->db->where('is_buy',0);
	// 	$this->db->where('id_proyek',$id);
	// 	$query = $this->db->get();
	// 	return $query;
	// }

	function getRumahDibeli_model($id){
		$this->db->select('R.*');
		$this->db->select('TR.*');
		$this->db->from('rumah R');
		$this->db->join('transaksi_rumah TR','R.id_rumah = TR.id_rumah');
		$this->db->where('R.is_archive',0);
		$this->db->where('is_buy',1);
		$this->db->where('R.id_proyek',$id);
		$query = $this->db->get();
		return $query;
	}

	function getRumahById_model($id){
		$this->db->select('*');
		$this->db->from('rumah');
		$this->db->where('id_rumah',$id);
		$query = $this->db->get();
		return $query->result_array();
	}

	function addRumah_model($data){
		$query = $this->db->insert('rumah',$data);
		return $query;
	}

	function updateRumah_model($data){
		$this->db->where('id_rumah',$data['id_rumah']);
		$query = $this->db->update('rumah',$data);
		return $query;
	}
	
	function arsipRumah_model($id){
		$this->db->where('id_rumah',$id);
		$query = $this->db->update('rumah',array('is_archive'=> 1));
		return $query;
	}
	
	function beliRumah_model($data){
		$this->db->where('id_rumah',$data['id_rumah']);
		$query = $this->db->update('rumah',array('is_buy' =>1));
		if ($query) {
			$beli = $this->db->insert('transaksi_rumah',$data);
			return $beli;
		}
	}

	function chartPemasukan_model(){
		$id_proyek = $this->session->userdata('id_proyek');
		$year = date('Y');

		$sql = "SELECT 
                SUM(IF(MONTH = 'Jan', total, 0)) AS 'Jan',
                SUM(IF(MONTH = 'Feb', total, 0)) AS 'Feb',
                SUM(IF(MONTH = 'Mar', total, 0)) AS 'Mar',
                SUM(IF(MONTH = 'Apr', total, 0)) AS 'Apr',
                SUM(IF(MONTH = 'May', total, 0)) AS 'May',
                SUM(IF(MONTH = 'Jun', total, 0)) AS 'Jun',
                SUM(IF(MONTH = 'Jul', total, 0)) AS 'Jul',
                SUM(IF(MONTH = 'Aug', total, 0)) AS 'Aug',
                SUM(IF(MONTH = 'Sep', total, 0)) AS 'Sep',
                SUM(IF(MONTH = 'Oct', total, 0)) AS 'Oct',
                SUM(IF(MONTH = 'Nov', total, 0)) AS 'Nov',
                SUM(IF(MONTH = 'Dec', total, 0)) AS 'Dec'
                FROM (
                    SELECT DATE_FORMAT(tanggal_pemasukan, '%b') AS MONTH,SUM(jumlah) AS total
                    FROM pemasukan
                    WHERE DATE_FORMAT(tanggal_pemasukan, '%Y') = ".$year." AND id_proyek = ".$id_proyek."
                    GROUP BY DATE_FORMAT(tanggal_pemasukan,'%M')
                )
                AS another";
        $query = $this->db->query($sql)->result_array();
        // var_dump($query);exit();
        return !empty($query) ? $query : 0;
	}

	function chartPengeluaran_model() {
		$id_proyek = $this->session->userdata('id_proyek');
		$year = date('Y');
		$sql = "SELECT 
                SUM(IF(MONTH = 'Jan', total, 0)) AS 'Jan',
                SUM(IF(MONTH = 'Feb', total, 0)) AS 'Feb',
                SUM(IF(MONTH = 'Mar', total, 0)) AS 'Mar',
                SUM(IF(MONTH = 'Apr', total, 0)) AS 'Apr',
                SUM(IF(MONTH = 'May', total, 0)) AS 'May',
                SUM(IF(MONTH = 'Jun', total, 0)) AS 'Jun',
                SUM(IF(MONTH = 'Jul', total, 0)) AS 'Jul',
                SUM(IF(MONTH = 'Aug', total, 0)) AS 'Aug',
                SUM(IF(MONTH = 'Sep', total, 0)) AS 'Sep',
                SUM(IF(MONTH = 'Oct', total, 0)) AS 'Oct',
                SUM(IF(MONTH = 'Nov', total, 0)) AS 'Nov',
                SUM(IF(MONTH = 'Dec', total, 0)) AS 'Dec'
                FROM (
                    SELECT DATE_FORMAT(tgl_transaksi, '%b') AS MONTH,SUM(pos_legal+pos_fasumfasos+pos_operasional+pos_promosi+pos_bangunan+pos_pajak) AS total
                    FROM pengeluaran
                    WHERE DATE_FORMAT(tgl_transaksi, '%Y') = ".$year." AND id_proyek = ".$id_proyek."
                    GROUP BY DATE_FORMAT(tgl_transaksi,'%M')
                )
                AS another";
        $query = $this->db->query($sql)->result_array();
        // var_dump($query);exit();
        return !empty($query) ? $query : 0;
	}
}

 ?>