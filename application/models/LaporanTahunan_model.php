<?php
/**
*Progammer : Mashudi Rohmat & Bambang Wijayanto
*Projek : Manajemen Keuangan Proyek 2020
*Desc : Model untuk manipulasi laporan tahunan
*/
class LaporanTahunan_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_pemasukan_tahunan($id,$tahun){
		$sql = 'SELECT SUM(jumlah) as jumlah, DATE_FORMAT(tanggal_pemasukan,"%M") as bulan
				FROM pemasukan
				WHERE id_proyek = ?
				AND DATE_FORMAT(tanggal_pemasukan,"%Y") = ?
				GROUP BY DATE_FORMAT(tanggal_pemasukan,"%m")
				ORDER BY tanggal_pemasukan';
		$query = $this->db->query($sql, array($id,$tahun));
		return $query->result_array();
	}

	function get_pengeluaran_tahunan($id,$tahun){
		$sql = 'SELECT
				SUM(pos_legal) as pos_legal,
				SUM(pos_fasumfasos) as pos_fasumfasos,
				SUM(pos_operasional) as pos_operasional,
				SUM(pos_promosi) as pos_promosi,
				SUM(pos_bangunan) as pos_bangunan,
				SUM(pos_pajak) as pos_pajak,
				DATE_FORMAT(tgl_transaksi,"%M") as bulan
				FROM pengeluaran
				WHERE id_proyek = ?
				AND DATE_FORMAT(tgl_transaksi,"%Y") = ?
				GROUP BY DATE_FORMAT(tgl_transaksi,"%m")
				ORDER BY tgl_transaksi';
		$query = $this->db->query($sql, array($id,$tahun));
		return $query->result_array();
	}
}
  ?>