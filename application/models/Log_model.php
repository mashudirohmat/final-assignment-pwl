<?php 
/**
*Progammer : Mashudi Rohmat & Bambang Wijayanto
*Projek : Manajemen Keuangan Proyek 2020
*Desc : Model untuk manipulasi log
*/
class Log_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function addLog_model($data){
		$this->db->insert('log',$data);
	}
	
	function getLog_model($id){
		$this->db->select('L.*');
		$this->db->select('PR.nama_proyek');
		// $this->db->select('PN.jenis_pengeluaran');
		$this->db->from('log L');
		$this->db->join('proyek PR','L.id_proyek = PR.id_proyek');
		// $this->db->join('pengeluaran PN','L.id_pengeluaran = PN.id_pengeluaran');
		$this->db->where('L.id_proyek',$id);
		$this->db->order_by('L.tanggal','DESC');
		$sql = $this->db->get();
		return $sql->result_array();
	}

	function get_logGlobal_model(){
		$this->db->select('*');
		$this->db->from('log');
		$this->db->where('id_proyek',NULL);
		$this->db->order_by('tanggal','DESC');
		$sql = $this->db->get();
		return $sql->result_array();
	}
}
 ?>