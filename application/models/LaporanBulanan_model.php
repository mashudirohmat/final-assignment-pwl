<?php
/**
*Progammer	: Mashudi Rohmat & Bambang Wijayanto
*Projek 	: Manajemen Keuangan Proyek 2020
*Desc 		: Model untuk manipulasi laporan bulanan
*/
class LaporanBulanan_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_amount_pengeluaran($params) {
		$this->db->select('SUM(pos_legal) as pos_legal');
		$this->db->select('SUM(pos_fasumfasos) as pos_fasumfasos');
		$this->db->select('SUM(pos_operasional) as pos_operasional');
		$this->db->select('SUM(pos_promosi) as pos_promosi');
		$this->db->select('SUM(pos_bangunan) as pos_bangunan');
		$this->db->select('SUM(pos_pajak) as pos_pajak');
		$this->db->from('pengeluaran');
		$this->db->where('id_proyek',$params);
		$this->db->where('is_archive',0);
		$sql = $this->db->get()->result_array();
		return $sql;
	}

	function get_pemasukan_bulanan($id,$bulan){
		$this->db->select('*');
		$this->db->from('pemasukan');
		$this->db->where('id_proyek',$id);
		$this->db->where('DATE_FORMAT(tanggal_pemasukan,"%Y-%m")',$bulan);
		$this->db->order_by('tanggal_pemasukan');
		$query = $this->db->get();
		return $query->result_array();
	}

	function get_pengeluaran_bulanan($id,$bulan){
		$this->db->select('*');
		$this->db->from('pengeluaran');
		$this->db->where('id_proyek',$id);
		$this->db->where('DATE_FORMAT(tgl_transaksi,"%Y-%m")',$bulan);
		$this->db->order_by('tgl_transaksi');
		$query = $this->db->get();
		return $query->result_array();
	}
}
  ?>