<?php
/**
*Progammer	: Mashudi Rohmat & Bambang Wijayanto
*Projek 	: Manajemen Keuangan Proyek 2020
*Desc 		: Model untuk manipulasi laporan pajak
*/
class LaporanPajak_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_pemasukan_bypajak($id_proyek,$pajak=null,$filter=null) {
		$this->db->select('DATE_FORMAT(tanggal_pemasukan,"%d %M %Y") AS tanggal', FALSE);
		$this->db->select('keterangan');
		$this->db->select('('.$pajak.'*jumlah) AS jumlah');
		$this->db->from('pemasukan');
		$this->db->where('id_proyek',$id_proyek);
		$this->db->where('is_archive',0);
		$this->db->where('DATE_FORMAT(tanggal_pemasukan,"%Y-%m")',$filter);
		$sql = $this->db->get()->result_array();

		return $sql;
	}
}
  ?>