<?php 
/**
*Progammer : Mashudi Rohmat & Bambang Wijayanto
*Projek : Manajemen Keuangan Proyek 2020
*Desc : Model untuk manipulasi users
*/
class Users_model extends CI_Model
{

	function __construct(){
		parent::__construct();
		$this->load->database();
	}
	//manajemen admin
	function getAllAdmins_model(){
		$this->db->select('*');
		$this->db->from('admin');
		$query = $this->db->get();
		return $query;
	}

	function getAdminById_model($id){
		$this->db->select('*');
		$this->db->from('admin');
		$this->db->where('id_user',$id);
		$query = $this->db->get();
		return $query->result_array();
	}

	function addAdmin_model($data){
		$query = $this->db->insert('admin',$data);
		return $query;
	}

	function updateAdmin_model($data){
		$this->db->where('id_user',$data['id_user']);
		$query = $this->db->update('admin',$data);
		return $query;
	}

	function deleteAdmin_model($delete){
		$this->db->where('id_user', $delete);
		$delete = $this->db->delete('admin');
	}
	//manajemen pimpro / mandor
	function getAllMandors_model(){
		$this->db->select('*');
		$this->db->from('mandor');
		$query = $this->db->get();
		return $query;
	}
	
	function getMandorById_model($id){
		$this->db->select('*');
		$this->db->from('mandor');
		$this->db->where('id_user',$id);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function addMandor_model($data){
		$query = $this->db->insert('mandor',$data);
		return $query;
	}
	
	function updateMandor_model($data){
		$this->db->where('id_user',$data['id_user']);
		$query = $this->db->update('mandor',$data);
		return $query;
	}
	
	function deleteMandor_model($delete){
		$this->db->where('id_user', $delete);
		$delete = $this->db->delete('mandor');
	}
	//manajemen owner / superadmin
	function getAllSuperadmin_model(){
		$this->db->select('*');
		$this->db->from('direksi');
		$query = $this->db->get();
		return $query;
	}
	
	function getSuperadminById_model($id){
		$this->db->select('*');
		$this->db->from('direksi');
		$this->db->where('id_user',$id);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function addSuperadmin_model($data){
		$query = $this->db->insert('direksi',$data);
		return $query;
	}
	
	function updateSuperadmin_model($data){
		$this->db->where('id_user',$data['id_user']);
		$query = $this->db->update('direksi',$data);
		return $query;
	}
	
	function deleteSuperadmin_model($delete){
		$this->db->where('id_user', $delete);
		$delete = $this->db->delete('direksi');
	}
}

 ?>