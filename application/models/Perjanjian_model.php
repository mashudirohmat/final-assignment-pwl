<?php
/**
*Progammer : Mashudi Rohmat & Bambang Wijayanto
*Projek : Manajemen Keuangan Proyek 2020
*Desc : Model untuk manipulasi pemasukan
*/
class Perjanjian_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function detailRumah_model($id){
		$this->db->select('R.*');
		$this->db->select('TR.*');
		$this->db->from('rumah R');
		$this->db->join('transaksi_rumah TR','R.id_rumah = TR.id_rumah');
		$this->db->where('TR.id_transaksi',$id);
		$query = $this->db->get();
		return $query->result_array();
	}
	function getAllTermin_model($id){
		$this->db->select('*');
		$this->db->from('perjanjian');
		$this->db->where('id_transaksi',$id);
		$this->db->where('is_archive',0);
		$query = $this->db->get();
		return $query;
	}
	function getTerminById_model($id){
		$this->db->select('*');
		$this->db->from('perjanjian');
		$this->db->where('id_perjanjian',$id);
		$query = $this->db->get();
		return $query;
	}
	function addTermin_model($data){
		$query = $this->db->insert('perjanjian',$data);
		return $query;
	}
	function update_termin_model($data){
		$this->db->where('id_perjanjian',$data['id_perjanjian']);
		$query = $this->db->update('perjanjian',$data);
		return $query;
	}
	function verivikasi_lunas($id,$data){
		$this->db->where('id_transaksi',$id);
		$query = $this->db->update('transaksi_rumah',$data);
		return $query;
	}
	
	function get_max_pemasukan_kode(){
		$this->db->select('MAX(id_pemasukan) AS id_pemasukan_max');
		$this->db->from('pemasukan');
		$request = $this->db->get();
		return $request;
	}

	function arsip_termin_model($id){
		$this->db->where('id_perjanjian',$id);
		$query = $this->db->update('perjanjian',array('is_archive'=> 1));
		return $query;
	}
}
  ?>