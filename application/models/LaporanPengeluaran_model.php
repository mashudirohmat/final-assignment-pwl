<?php
/**
*Progammer	: Mashudi Rohmat & Bambang Wijayanto
*Projek 	: Manajemen Keuangan Proyek 2020
*Desc 		: Model untuk manipulasi pengeluaran
*/
class LaporanPengeluaran_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function getAllPengeluaran_model($filter){
		$this->db->select('*');
		$this->db->from('pengeluaran');
		$this->db->where('id_proyek',$filter);
		$this->db->where('is_archive',0);
		$query = $this->db->get()->result_array();

		// var_dump($query);exit();

		return !empty($query) ? $query : FALSE;
	}

	function getPengeluaranById_model($id){
		$this->db->select('*');
		$this->db->select('(pos_legal+pos_fasumfasos+pos_operasional+pos_promosi+pos_bangunan+pos_pajak) AS jumlah');
		$this->db->from('pengeluaran');
		$this->db->where('id_pengeluaran',$id);
		$query = $this->db->get();
		return $query->result_array();
	}

	function addPengeluaran_model($data){
		$query = $this->db->insert('pengeluaran',$data);
		return $query;
	}

	function updatePengeluaran_model($data){
		$this->db->where('id_pengeluaran',$data['id_pengeluaran']);
		$netralize_pos = $this->db->update('pengeluaran',array('pos_legal' => 0,
														'pos_promosi' => 0,
														'pos_operasional' => 0,
														'pos_fasumfasos' => 0,
														'pos_bangunan' => 0,
														'pos_pajak' => 0,
													));
		if ($netralize_pos) {
			$this->db->where('id_pengeluaran',$data['id_pengeluaran']);
			$sql = $this->db->update('pengeluaran',$data);

			return $sql; 
		}
	}
	
	function archivePengeluaran_model($id){
		$this->db->where('id_pengeluaran',$id);
		$query = $this->db->update('pengeluaran',array('is_archive'=> 1));
		return $query;
	}

	function getAllPemasukan_model($id){
		$this->db->select('*');
		$this->db->from('pemasukan');
		$this->db->where('id_proyek',$id);
		$query= $this->db->get();
		return $query;
	}
}

?>