<?php 
/**
*Progammer	: Mashudi Rohmat & Bambang Wijayanto
*Projek 	: Manajemen Keuangan Proyek 2020
*Desc 		: Model untuk manipulasi prject, pos, rumah
*/
class LaporanProyeksi_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_laporan_proyeksi($id_proyek,$tanggal) {
		$this->db->select('b.nama_rumah as nama_rumah, b.harga_rumah as harga_rumah, c.nama_pembeli, c.trx_date, a.tgl_jatuh_tempo, a.jumlah as kontrak_termin');
		$this->db->from('perjanjian a');
		$this->db->join('rumah b','a.id_rumah = b.id_rumah');
		$this->db->join('transaksi_rumah c','a.id_rumah = c.id_rumah');
		$this->db->where('a.id_proyek',$id_proyek);
		$this->db->where('c.is_lunas',0);
		$this->db->where('b.is_buy',1);
		$this->db->where('b.is_archive',0);
		$this->db->where('DATE_FORMAT(a.tgl_jatuh_tempo,"%Y-%m") =',$tanggal);
		$this->db->order_by('a.tgl_jatuh_tempo','DESC');
		$sql = $this->db->get()->result_array();
		
		return $sql;
	}
}
 ?>