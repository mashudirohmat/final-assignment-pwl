<?php 
/**
*Progammer	: Mashudi Rohmat & Bambang Wijayanto
*Projek 	: Manajemen Keuangan Proyek 2020
*Desc 		: Controller untuk olah data user
*/
class Users extends CI_Controller
{

	function __construct(){
		parent::__construct();
		$this->load->model('Users_model','um');
		$this->load->model('Log_model','m_log');
	}
	//view untuk admin
	function index(){
		if ($this->session->userdata('id_user')== null) {
			redirect('');
		}else{
			$data = array (	'title'		=>'Data Pegawai',
				'isi'		=>'superadmin/adminmanagement',
				'js'		=>'superadmin/js',
				'modal'		=>'superadmin/modal'
			);
			$this->data['admin'] = $this->getAllAdmins();
			$this->load->view('layout/wrapper',$data, $this->data, FALSE);
		}
	}

	function getAllAdmins(){
		$request = $this->um->getAllAdmins_model();
		return $request->result_array();
	}

	function getAdminById(){
    	$id 	= $this->input->post('id');
    	$request= $this->um->getAdminById_model($id);
    	echo json_encode($request);
	}

	function deleteAdmin($id){
		if(empty($id)){
			redirect('index.php/users');
		}else{
			try
			{
				$request = $this->um->deleteAdmin_model($id);
				$this->log('Delete data admin');
			}catch( Exception $e){			
			}
			redirect('index.php/users');
		}
	}
	//view untuk pimpro
	function mandor(){
		if ($this->session->userdata('id_user')== null) {
			redirect('');
		}else{
			$data = array (	'title'		=>'Data Pegawai',
				'isi'		=>'superadmin/mandormanagement',
				'js'		=>'superadmin/js',
				'modal'		=>'superadmin/modal'
			);
			$this->data['mandor'] = $this->getAllMandors();
			$this->load->view('layout/wrapper',$data, $this->data, FALSE);
		}
	}

	function getAllMandors(){
		$request = $this->um->getAllMandors_model();
		return $request->result_array();
	}

	function getMandorById(){
    	$id 	= $this->input->post('id');
    	$request= $this->um->getMandorById_model($id);
    	echo json_encode($request);
	}

	function deleteMandor($id){
		if(empty($id)){
			redirect('index.php/users/mandor');
		}else{
			try
			{
				$request = $this->um->deleteMandor_model($id);
				$this->log('Delete data pimpro');
			}catch( Exception $e){			
			}
			redirect('index.php/users/mandor');
		}
	}
	//view untuk owner
	function superadmin(){
		if ($this->session->userdata('id_user')== null) {
			redirect('');
		}else{
			$data = array (	'title'		=>'Data Pegawai',
				'isi'		=>'superadmin/superadminmanagement',
				'js'		=>'superadmin/js',
				'modal'		=>'superadmin/modal'
			);
			$this->data['superadmin'] = $this->getAllSuperadmin();
			$this->load->view('layout/wrapper',$data, $this->data, FALSE);
		}
	}
	
	function getAllSuperadmin(){
		$request = $this->um->getAllSuperadmin_model();
		return $request->result_array();
	}
	
	function getSuperadminById(){
    	$id 	= $this->input->post('id');
    	$request= $this->um->getSuperadminById_model($id);
    	echo json_encode($request);
	}

	function deleteSuperadmin($id){
		if(empty($id)){
			redirect('index.php/users/superadmin');
		}else{
			try
			{
				$request = $this->um->deleteSuperadmin_model($id);
				$this->log('Delete data owner');
			}catch( Exception $e){			
			}
			redirect('index.php/users/superadmin');
		}
	}
	//crud all users
	function addUser(){
		$data = array('username' =>$_POST['username'],
					'password' =>md5($_POST['password']),
					'hak_akses' =>$_POST['hakakses'],
					'no_hp' =>$_POST['nohp'],
					'email' =>$_POST['email']);
		if ($data['hak_akses'] == 'admin') {
			$request = $this->um->addAdmin_model($data);

			if ($request) {
				$this->log('Add data admin '.$data['username']);

				$result['status'] = 'success';
				$result['message'] = 'Data berhasil diinputkan';
				$result['redirect_url'] = site_url('index.php/users/');
				$this->output->set_content_type('application/json');
				$this->output->set_output(json_encode($result));
				$string = $this->output->get_output();
				echo json_encode($result);
				exit();
			}
		}elseif($data['hak_akses'] == 'pimpro'){
			$request = $this->um->addMandor_model($data);

			if ($request) {
				$this->log('Add data pimpro '.$data['username']);

				$result['status'] = 'success';
				$result['message'] = 'Data berhasil diinputkan';
				$result['redirect_url'] = site_url('index.php/users/mandor');
				$this->output->set_content_type('application/json');
				$this->output->set_output(json_encode($result));
				$string = $this->output->get_output();
				echo json_encode($result);
				exit();
			}
		}else{
			$request = $this->um->addSuperadmin_model($data);

			if ($request) {
				$this->log('Add data owner '.$data['username']);

				$result['status'] = 'success';
				$result['message'] = 'Data berhasil diinputkan';
				$result['redirect_url'] = site_url('index.php/users/superadmin');
				$this->output->set_content_type('application/json');
				$this->output->set_output(json_encode($result));
				$string = $this->output->get_output();
				echo json_encode($result);
				exit();
			}
		}	
	}

	function updateUser(){
		if ($_POST['password'] == $_POST['password_lama']) {
			$password = $_POST['password'];
		}else{
			$password = md5($_POST['password']);
		}

		$data = array('id_user' => $_POST['id'],
					'username' =>$_POST['username'],
					'password' =>$password,
					'hak_akses' =>$_POST['hakakses'],
					'no_hp' =>$_POST['nohp'],
					'email' =>$_POST['email']);
		if ($data['hak_akses'] == 'admin') {
			$request = $this->um->updateAdmin_model($data);

			if ($request) {
				$this->log('Update data admin '.$data['username']);

				$result['status'] = 'success';
				$result['message'] = 'Data berhasil diubah';
				$result['redirect_url'] = site_url('index.php/users/');
				$this->output->set_content_type('application/json');
				$this->output->set_output(json_encode($result));
				$string = $this->output->get_output();
				echo json_encode($result);
				exit();
			}
		}elseif($data['hak_akses'] == 'pimpro'){
			$request = $this->um->updateMandor_model($data);

			if ($request) {
				$this->log('Update data pimpro '.$data['username']);

				$result['status'] = 'success';
				$result['message'] = 'Data berhasil diubah';
				$result['redirect_url'] = site_url('index.php/users/mandor');
				$this->output->set_content_type('application/json');
				$this->output->set_output(json_encode($result));
				$string = $this->output->get_output();
				echo json_encode($result);
				exit();
			}
		}else{
			$request = $this->um->updateSuperadmin_model($data);

			if ($request) {
				$this->log('Update data owner '.$data['username']);

				$result['status'] = 'success';
				$result['message'] = 'Data berhasil diubah';
				$result['redirect_url'] = site_url('index.php/users/superadmin');
				$this->output->set_content_type('application/json');
				$this->output->set_output(json_encode($result));
				$string = $this->output->get_output();
				echo json_encode($result);
				exit();
			}
		}
	}
	//log
	function log($params){
		$log = array('id_pegawai' => $this->session->userdata('id_user'),
					'nama_pegawai' => $this->session->userdata('username'),
					'hak_akses' => $this->session->userdata('hak_akses'),
					'jenis_log' => $params);

		$this->m_log->addLog_model($log);
	}
	
}

 ?>