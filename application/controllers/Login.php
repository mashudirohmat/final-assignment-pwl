<?php
/**
*Progammer	: Mashudi Rohmat & Bambang Wijayanto
*Projek 	: Manajemen Keuangan Proyek 2020
*Desc 		: Controller untuk mengolah authentication
*/
class Login extends CI_Controller {

    public function __construct()
       {
            parent::__construct();
            $this->load->model('Login_model','lm');
            $this->load->model('Log_model','m_log');
            // Your own constructor code
    }

	public function index()
	{
		$this->load->view('login');
	}

	public function cek_login(){
		$data= array('username'=> $this->input->post('username'),
					'password'=> md5($this->input->post('password'))
				);
		$superAdmin = $this->lm->cekUser_model($data, $tabel='direksi');
		$admin = $this->lm->cekUser_model($data, $tabel='admin');
		$mandor = $this->lm->cekUser_model($data, $tabel='mandor');
		// var_dump($superAdmin[0]['username']);exit();

		if ($superAdmin != null) {
			$this->session->set_userdata('username',$superAdmin[0]['username']);
			$this->session->set_userdata('hak_akses',$superAdmin[0]['hak_akses']);
			$this->session->set_userdata('id_user',$superAdmin[0]['id_user']);

			$this->log('Login');
			redirect('index.php/project');
		}elseif($admin != null){
			$this->session->set_userdata('username',$admin[0]['username']);
			$this->session->set_userdata('hak_akses',$admin[0]['hak_akses']);
			$this->session->set_userdata('id_user',$admin[0]['id_user']);

			$this->log('Login');
			redirect('index.php/project');
		}elseif($mandor != null){
			$this->session->set_userdata('username',$mandor[0]['username']);
			$this->session->set_userdata('hak_akses',$mandor[0]['hak_akses']);
			$this->session->set_userdata('id_user',$mandor[0]['id_user']);

			$this->log('Login');
			redirect('index.php/project');
		}else{
			$this->session->set_flashdata('confirm','Username atau Password Salah');
			redirect('');
		}
	}

	public function logout(){
		$this->log('Logout');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('hak_akses');
		$this->session->unset_userdata('id_user');
		$this->session->set_flashdata('confirm','Anda telah logout.');
		redirect('');
	}

	function log($params){
		$log = array('id_pegawai' => $this->session->userdata('id_user'),
					'nama_pegawai' => $this->session->userdata('username'),
					'hak_akses' => $this->session->userdata('hak_akses'),
					'jenis_log' => $params);

		$this->m_log->addLog_model($log);
	}
}

?>