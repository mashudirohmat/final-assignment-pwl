<?php 
/**
*Progammer	: Mashudi Rohmat & Bambang Wijayanto
*Projek 	: Manajemen Keuangan Proyek 2020
*Desc 		: Controller untuk manipulasi prject, pos, rumah
*/
class Project extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('project_model','project');
		$this->load->model('Approvement_model','approvement');
		$this->load->model('Log_model','m_log');
	}
	//view list proyek
	function index(){
		if ($this->session->userdata('id_user')== null) {
			redirect('');
		}else{
			$data = array (	'title'		=>'Proyek',
				'isi'		=>'project/list',
				'js'		=>'project/js',
				'modal'		=>'project/modal'
			);
			$this->data['admin'] = $this->getAdmin();
			$this->data['mandor'] = $this->getMandor();
			$this->data['project'] = $this->getAllProject();
			$this->load->view('layout/wrapper',$data,$this->data, FALSE);
		}
	}
	// view detail proyek
	function detailProject($id=null){
		if ($this->session->userdata('id_user')== null) {
			redirect('');
		}else{
			$data = array (	'title'		=>'Proyek',
				'isi'		=>'project/detail',
				'js'		=>'project/js',
				'modal'		=>'project/modal'
			);
			$this->data['detail'] = $this->project->detailProject_model($id);
			if ($this->data['detail']) {
				$this->session->set_userdata('id_proyek',$this->data['detail'][0]['id_proyek']);
				$this->session->set_userdata('lokasi',$this->data['detail'][0]['lokasi']);
				$this->session->set_userdata('tanggal',$this->data['detail'][0]['tanggal']);
				$this->session->set_userdata('nama_proyek',$this->data['detail'][0]['nama_proyek']);
				$this->session->set_userdata('foto',$this->data['detail'][0]['foto']);
			}
			$this->data['request'] = $this->approvement->get_request($id);
			$this->data['adminproject'] = $this->project->getAdminByIdProyek_model($id);
			$this->data['mandorproject'] = $this->project->getMandorByIdProyek_model($id);
			$this->data['pos'] = $this->getPos();
			$this->data['admin'] = $this->getAdmin();
			$this->data['mandor'] = $this->getMandor();
			$this->data['rumah'] = $this->getAllRumah();
			$this->data['rumahdibeli'] = $this->getRumahDibeli();
			$this->data['pemasukan'] = $this->chartPemasukan();
			$this->data['pengeluaran'] = $this->chartPengeluaran();

				//menampilkan detailproject berdasarkan id
			$this->data['project'] = $this->project->detailProject_model($id);

			$this->load->view('layout/wrapper',$data,$this->data, FALSE);
		}
	}

	function getProjectById(){
		$id = $_POST['id'];
		$proyek = $this->project->detailProject_model($id);
		$admin = $this->project->getAdminByIdProyek_model($id);
		$mandor = $this->project->getMandorByIdProyek_model($id);
		$result = array($proyek,$admin,$mandor);
		echo json_encode($result);
	}

	function getAdmin(){
		$request = $this->project->getAdmin_model();
		return $request->result_array();
	}

	function getMandor(){
		$request = $this->project->getMandor_model();
		return $request->result_array();
	}
	// manipulasi proyek
	function getAllProject(){
		$filter= array('hakAkses' => $this->session->userdata('hak_akses'),
			'idUser' => $this->session->userdata('id_user')
		);
		if ($this->session->userdata('hak_akses')== 'owner') {
			$result = $this->project->getAllProject_model($filter='');
		}else{
			$result = $this->project->getAllProject_model($filter);
		}

		$temp_arr = array();
		$temp_admin = array();
		$temp_mandor = array();
		if(!empty($result)){

			$id_proyek = NULL;
			foreach($result as $k => $v){
				if($id_proyek != $v['id_proyek']){
					$temp = $result[$k];
					foreach(['admin','mandor'] as $t){
						unset($temp[$t]);
					}
					$temp_arr[] = $temp;
				}

				$id_proyek = $v['id_proyek'];
					// buat cari array admin
				if($id_proyek == $v['id_proyek']){
					$temp = $v;
					foreach(['id_proyek','nama_proyek','foto','lokasi','date','budget','is_archive','mandor'] as $t){
						unset($temp[$t]);
					}
					$temp_admin[$v['id_proyek']][] = $temp;
				}
					// buat cari array mandor
				if($id_proyek == $v['id_proyek']){
					$temp = $v;
					foreach(['id_proyek','nama_proyek','foto','lokasi','date','budget','is_archive','admin'] as $t){
						unset($temp[$t]);
					}
					$temp_mandor[$v['id_proyek']][] = $temp;
				}
			}
			// haisl masih dobel dijadikan uniq
			$uniq = [];

			foreach ($temp_arr as $k => $v) {
				$uniq[$v['id_proyek']] = array_unique($temp_arr[$k]);
			}
			//memasukkan admin dan mandor ke dalam array return an
			foreach($uniq as $k => $v){
				// $temp_arr[$k]['id_proyek'] = $v['id_proyek'];
				$uniq[$k]['admin'] = array_unique(array_column($temp_admin[$v['id_proyek']], 'admin'));
				$uniq[$k]['mandor'] = array_unique(array_column($temp_mandor[$v['id_proyek']],'mandor'));
			}
		}
		// echo "<pre>";
		// print_r(array_values($uniq));exit();
		return array_values($uniq);
	}

	function getArchiveProject(){
		if ($this->session->userdata('id_user')== null) {
			redirect('');
		}else{
			$result = $this->project->getArchiveProject_model();
			$temp_arr = array();
			$temp_admin = array();
			$temp_mandor = array();
			if(!empty($result)){

				$id_proyek = NULL;
				foreach($result as $k => $v){
					if($id_proyek != $v['id_proyek']){
						$temp = $result[$k];
						foreach(['admin','mandor'] as $t){
							unset($temp[$t]);
						}
						$temp_arr[] = $temp;
					}

					$id_proyek = $v['id_proyek'];
					// buat cari array admin
					if($id_proyek == $v['id_proyek']){
						$temp = $v;
						foreach(['id_proyek','nama_proyek','foto','lokasi','date','budget','is_archive','mandor'] as $t){
							unset($temp[$t]);
						}
						$temp_admin[$v['id_proyek']][] = $temp;
					}
					// buat cari array mandor
					if($id_proyek == $v['id_proyek']){
						$temp = $v;
						foreach(['id_proyek','nama_proyek','foto','lokasi','date','budget','is_archive','admin'] as $t){
							unset($temp[$t]);
						}
						$temp_mandor[$v['id_proyek']][] = $temp;
					}
				}
			// haisl masih dobel dijadikan uniq
				$uniq = [];

				foreach ($temp_arr as $k => $v) {
					$uniq[$v['id_proyek']] = array_unique($temp_arr[$k]);
				}
			//memasukkan admin dan mandor ke dalam array return an
				foreach($uniq as $k => $v){
				// $temp_arr[$k]['id_proyek'] = $v['id_proyek'];
					$uniq[$k]['admin'] = array_unique(array_column($temp_admin[$v['id_proyek']], 'admin'));
					$uniq[$k]['mandor'] = array_unique(array_column($temp_mandor[$v['id_proyek']],'mandor'));
				}
			}

			$data = array (	'title'		=>'Proyek',
				'isi'		=>'project/archive',
				'js'		=>'project/js',
				'modal'		=>'project/modal'
			);

			$this->data['admin'] = $this->getAdmin();
			$this->data['mandor'] = $this->getMandor();
			$this->data['project'] = $uniq;
			$this->load->view('layout/wrapper',$data,$this->data, FALSE);
		}
	}
	//manipulasi pos
	function setPos(){
		$posLegal = str_replace(array('Rp. ','.'),array('',''), $_POST['posLegal']);
		$posFasumFasos = str_replace(array('Rp. ','.'),array('',''), $_POST['posFasumFasos']);
		$posOperasional = str_replace(array('Rp. ','.'),array('',''), $_POST['posOperasional']);
		$posPromosi = str_replace(array('Rp. ','.'),array('',''), $_POST['posPromosi']);
		$posBangunan = str_replace(array('Rp. ','.'),array('',''), $_POST['posBangunan']);
		$posBiayaPajak = str_replace(array('Rp. ','.'),array('',''), $_POST['posBiayaPajak']);
		
		$data = array('id_proyek' => intval($_POST['id']),
			'pos_legal' =>intval($posLegal),
			'pos_fasumfasos' =>intval($posFasumFasos),
			'pos_operasional' =>intval($posOperasional),
			'pos_promosi' =>intval($posPromosi),
			'pos_bangunan' =>intval($posBangunan),
			'pos_pajak' =>intval($posBiayaPajak),
		);

		if ($this->project->setPos_model($data)) {
			$this->log('Set POS');

			$result['status'] = 'success';
			$result['message'] = 'Data berhasil diinputkan';
			$result['redirect_url'] = site_url('index.php/project/detailProject/'.$this->session->userdata('id_proyek'));
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($result));
			$string = $this->output->get_output();
			echo json_encode($result);
			exit();
		}
	}

	function getPos(){
		$id = $this->session->userdata('id_proyek');
		$request = $this->project->getPos_model($id);
		return $request;
	}

	function getPosById(){
		$id = $_POST['id'];
		$request = $this->project->getPos_model($id);
		echo json_encode($request);
	}

	function updatePos(){
		$posLegal = str_replace(array('Rp. ','.'),array('',''), $_POST['posLegal']);
		$posFasumFasos = str_replace(array('Rp. ','.'),array('',''), $_POST['posFasumFasos']);
		$posOperasional = str_replace(array('Rp. ','.'),array('',''), $_POST['posOperasional']);
		$posPromosi = str_replace(array('Rp. ','.'),array('',''), $_POST['posPromosi']);
		$posBangunan = str_replace(array('Rp. ','.'),array('',''), $_POST['posBangunan']);
		$posBiayaPajak = str_replace(array('Rp. ','.'),array('',''), $_POST['posBiayaPajak']);

		$data = array('id_proyek' => intval($_POST['id']),
			'pos_legal' =>intval($posLegal),
			'pos_fasumfasos' =>intval($posFasumFasos),
			'pos_operasional' =>intval($posOperasional),
			'pos_promosi' =>intval($posPromosi),
			'pos_bangunan' =>intval($posBangunan),
			'pos_pajak' =>intval($posBiayaPajak),
		);

		if ($this->project->updatePos_model($data)) {
			$this->log('Update POS');

			$result['status'] = 'success';
			$result['message'] = 'Data berhasil diubah';
			$result['redirect_url'] = site_url('index.php/project/detailProject/'.$this->session->userdata('id_proyek'));
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($result));
			$string = $this->output->get_output();
			echo json_encode($result);
			exit();
		}
	}
	//manipulasi project
	function addProject(){
		$data = array('nama_proyek' =>$_POST['namaproyek'],
			'lokasi' =>$_POST['lokasiproyek'],
			'id_admin' =>$_POST['admin'],
			'id_mandor' =>$_POST['mandor']
			);

		if(!empty($_FILES['foto']['name']))
		{
			$upload = $this->_do_upload();
			$data['foto'] = $upload['file_name'];
		}else{
			$data['foto'] = 'default';
		}
		$request = $this->project->addProject_model($data);
		if ($request) {
			$this->log_global('Add Project');

			$result['status'] = 'success';
			$result['message'] = 'Data berhasil diinputkan';
			$result['redirect_url'] = site_url('index.php/project/');
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($result));
			$string = $this->output->get_output();
			echo json_encode($result);
			exit();
		}
	}

	function Arsip($id){
		if(empty($id)){
			redirect('index.php/project');
		}else{
			try
			{
				$request = $this->project->Arsip_model($id);
				$this->log('Arsip project');
			}catch( Exception $e){			
			}
			redirect('index.php/project');
		}
	}

	private function _do_upload()
	{
		$config['upload_path']          = './assets/img/';
		$config['allowed_types']        = 'jpg|png|jpeg';
		$config['max_size']             = 100; 
		$config['max_width']            = 2000; 
		$config['max_height']           = 2000; 
		$config['file_name']            = round(microtime(true) * 1000);
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload('foto')) 
		{
			$data['inputerror'][] = 'foto';
			$data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); 
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		return $this->upload->data('file_name');
	}

	function updateProject() {
		$data = array('nama_proyek' =>$_POST['namaproyek'],
			'lokasi' =>$_POST['lokasiproyek'],
			'id_admin' =>$_POST['admin'],
			'id_mandor' =>$_POST['mandor'],
			'id_proyek' => $_POST['id_proyek']);
		if($this->input->post('hapusfoto'))
		{
			if(file_exists('./assets/img/'.$this->input->post('hapusfoto')) && $this->input->post('hapusfoto'))
				unlink('./assets/img/'.$this->input->post('hapusfoto'));
			$data['foto']='empty';
		}
		if(!empty($_FILES['foto']['name']))
		{
			$upload = $this->_do_upload();
			$unit 	= $this->session->userdata('MSU_KODE');
			$project = $this->project->detailProject_model($data['id_proyek']);
			if(file_exists('./assets/img/'.$project[0]['foto']) && $project[0]['foto'])
				unlink('./assets/img/'.$project[0]['foto']);
			$data['foto']=$upload['file_name'];
		}else{
			$data['foto']='empty';
		}
		$this->project->updateProject_model($data['id_proyek'], $data);
		$this->log('Update Project');

		$result['status'] = 'success';
		$result['message'] = 'Data berhasil diinputkan';
		$result['redirect_url'] = site_url('index.php/project/detailproject/'.$this->session->userdata('id_proyek'));
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($result));
		$string = $this->output->get_output();
		echo json_encode($result);
		exit();
	}
	//manipulasi rumah
	function getAllRumah(){
		$id = $this->session->userdata('id_proyek');
		$request = $this->project->getAllRumah_model($id);
		// echo "<pre>";
		// print_r($request);exit();
		return $request;
	}

	function getRumahById(){
		$id = $_POST['id'];
		$request= $this->project->getRumahById_model($id);
		// var_dump($id);exit();
		echo json_encode($request);
	}

	function addRumah(){
		$harga_rumah = str_replace(array('Rp. ','.'),array('',''), $_POST['harga_rumah']);

		$id_proyek = $this->session->userdata('id_proyek');
		$data= array(	'id_proyek' => intval($id_proyek),
			'nama_rumah' => $_POST['nama_rumah'],
			'tipe_rumah' => $_POST['tipe_rumah'],
			'harga_rumah' => intval($harga_rumah)
		);
		$request = $this->project->addRumah_model($data);
		if ($request) {
			$this->log('Add Rumah');

			$result['status'] = 'success';
			$result['message'] = 'Data berhasil diinputkan';
			$result['redirect_url'] = site_url('index.php/project/detailProject/'.$id_proyek);
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($result));
			$string = $this->output->get_output();
			echo json_encode($result);
			exit();
		}
	}

	function updateRumah(){
		$harga_rumah = str_replace(array('Rp. ','.'),array('',''), $_POST['harga_rumah']);

		$id_proyek = $this->session->userdata('id_proyek');
		$data= array(	'id_rumah' => intval($_POST['id_rumah']),
			'nama_rumah' => $_POST['nama_rumah'],
			'tipe_rumah' => $_POST['tipe_rumah'],
			'harga_rumah' => intval($harga_rumah)
		);
		$request = $this->project->updateRumah_model($data);
		if ($request) {
			$this->log('Update Rumah');

			$result['status'] = 'success';
			$result['message'] = 'Data berhasil diinputkan';
			$result['redirect_url'] = site_url('index.php/project/detailProject/'.$id_proyek);
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($result));
			$string = $this->output->get_output();
			echo json_encode($result);
			exit();
		}
	}

	function arsipRumah($id){
		$kode = $this->session->userdata('id_proyek');
		if(empty($id)){
			redirect('index.php/project/detailProject/'.$kode);
		}else{
			try
			{
				$request = $this->project->arsipRumah_model($id);
				$this->log('Arsip Rumah');
			}catch( Exception $e){			
			}
			redirect('index.php/project/detailProject/'.$kode);
		}
	}

	function beliRumah(){
		$id_proyek = $this->session->userdata('id_proyek');
		$data= array(	'id_rumah' 			=> intval($_POST['id_rumah2']),
						'nama_pembeli'		=> $_POST['nama_pembeli'],
						'alamat_pembeli'	=> $_POST['alamat_pembeli'],
						'no_ktp' 			=> $_POST['noktp_pembeli'],
						'no_kk' 			=> $_POST['nokk_pembeli'],
						'no_telp' 			=> $_POST['nohp_pembeli']
		);
		$request = $this->project->beliRumah_model($data);
		if ($request) {
			$this->log('Transaksi pembelian rumah');

			$result['status'] 		= 'success';
			$result['message'] 		= 'Data berhasil diinputkan';
			$result['redirect_url'] = site_url('index.php/project/detailProject/'.$id_proyek);
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($result));
			$string = $this->output->get_output();
			echo json_encode($result);
			exit();
		}
	}

	function getRumahDibeli(){
		$id 		= $this->session->userdata('id_proyek');
		$request 	= $this->project->getRumahDibeli_model($id);
		return $request->result_array();
	}

	function log($params){
		$log = array('id_pegawai' => $this->session->userdata('id_user'),
					'nama_pegawai' => $this->session->userdata('username'),
					'hak_akses' => $this->session->userdata('hak_akses'),
					'id_proyek' => $this->session->userdata('id_proyek'),
					'jenis_log' => $params);

		$this->m_log->addLog_model($log);
	}

	function log_global($params){
		$log = array('id_pegawai' => $this->session->userdata('id_user'),
					'nama_pegawai' => $this->session->userdata('username'),
					'hak_akses' => $this->session->userdata('hak_akses'),
					'jenis_log' => $params);

		$this->m_log->addLog_model($log);
	}

	function chartPemasukan() {
		$data = $this->project->chartPemasukan_model();
        $summary = array('a' => intval($data[0]['Jan']),
                        'b' => intval($data[0]['Feb']),
                        'c' => intval($data[0]['Mar']),
                        'd' => intval($data[0]['Apr']),
                        'e' => intval($data[0]['May']),
                        'f' => intval($data[0]['Jun']),
                        'g' => intval($data[0]['Jul']),
                        'h' => intval($data[0]['Aug']),
                        'i' => intval($data[0]['Sep']),
                        'j' => intval($data[0]['Oct']),
                        'k' => intval($data[0]['Nov']),
                        'l' => intval($data[0]['Dec'])
                    );
        // echo "<pre>";
        // print_r($summary);exit();
        return $summary;
	}

	function chartPengeluaran() {
		$data = $this->project->chartPengeluaran_model();
        $summary = array('a' => intval($data[0]['Jan']),
                        'b' => intval($data[0]['Feb']),
                        'c' => intval($data[0]['Mar']),
                        'd' => intval($data[0]['Apr']),
                        'e' => intval($data[0]['May']),
                        'f' => intval($data[0]['Jun']),
                        'g' => intval($data[0]['Jul']),
                        'h' => intval($data[0]['Aug']),
                        'i' => intval($data[0]['Sep']),
                        'j' => intval($data[0]['Oct']),
                        'k' => intval($data[0]['Nov']),
                        'l' => intval($data[0]['Dec'])
                    );
        // echo "<pre>";
        // print_r($summary);exit();
        return $summary;
	}
}
?>