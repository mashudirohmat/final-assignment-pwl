<?php 
/**
*Progammer	: Mashudi Rohmat & Bambang Wijayanto
*Projek 	: Manajemen Keuangan Proyek 2020
*Desc 		: Controller untuk manipulasi laporan pengeluaran
*/
class Laporan_pengeluaran extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		//load model
		$this->load->model('LaporanPengeluaran_model','pengeluaran');
		$this->load->model('LaporanBulanan_model','bulanan');
		$this->load->model('Project_model','project');
		$this->load->model('Approvement_model','approvement');
		$this->load->model('Log_model','m_log');
	}

	function index(){
		// cek login session
		if ($this->session->userdata('id_user')== null) {
			redirect('');
		}else{
			//data page dan layout
			$data = array (	'title'	=>'Proyek',
							'isi'	=>'laporan_pengeluaran/list',
							'js'	=>'laporan_pengeluaran/js',
							'modal'	=>'laporan_pengeluaran/modal'
			);
			//id proyek
			$id = $this->session->userdata('id_proyek');
			$this->data['detail'] = $this->project->detailProject_model($id);
			if ($this->data['detail']) {
				$this->session->set_userdata('id_proyek',$this->data['detail'][0]['id_proyek']);
				$this->session->set_userdata('nama_proyek',$this->data['detail'][0]['nama_proyek']);
				$this->session->set_userdata('foto',$this->data['detail'][0]['foto']);
			}
			//load data
			$this->data['sisa_pos'] = $this->sisa_pos();
			$this->data['pos'] = $this->project->getPos_model($id);
			$this->data['pengeluaran'] = $this->getAllPengeluaran();
			$this->data['pemasukan'] = $this->getAllPemasukan();
			$this->data['project'] = $this->project->detailProject_model($id);
			//load view
			$this->load->view('layout/wrapper',$data,$this->data, FALSE);
		}
	}

	function getAllPengeluaran(){
		$filter= $this->session->userdata('id_proyek');
		$request= $this->pengeluaran->getAllPengeluaran_model($filter);
		// var_dump($request);exit();
		return $request;
	}

	function getAllPemasukan(){
		$id = $this->session->userdata('id_proyek');
		$data= $this->pengeluaran->getAllPemasukan_model($id);
		return $data->result_array();
	}

	function getPengeluaranById(){
		$id = $_POST['id'];
		$request = $this->pengeluaran->getPengeluaranById_model($id);

		if ($request[0]['pos_legal'] != '0') {
			$nama_pos = 'pos_legal';
		}elseif($request[0]['pos_fasumfasos'] != '0'){
			$nama_pos = 'pos_fasumfasos';
		}elseif($request[0]['pos_operasional'] != '0'){
			$nama_pos = 'pos_operasional';
		}elseif($request[0]['pos_promosi'] != '0'){
			$nama_pos = 'pos_promosi';
		}elseif($request[0]['pos_bangunan'] != '0'){
			$nama_pos = 'pos_bangunan';
		}elseif($request[0]['pos_pajak'] != '0'){
			$nama_pos = 'pos_pajak';
		}
		$data_edit = array('id_pengeluaran' => $request[0]['id_pengeluaran'],
							'id_proyek' => $request[0]['id_proyek'],
							'no_nota' => $request[0]['no_nota'],
							'tgl_transaksi' => $request[0]['tgl_transaksi'],
							'desc_pengeluaran' => $request[0]['jenis_pengeluaran'],
							'pos_legal' => $request[0]['pos_legal'],
							'pos_fasumfasos' => $request[0]['pos_fasumfasos'],
							'pos_operasional' => $request[0]['pos_operasional'],
							'pos_promosi' => $request[0]['pos_promosi'],
							'pos_bangunan' => $request[0]['pos_bangunan'],
							'pos_pajak' => $request[0]['pos_pajak'],
							'pilih_pos' => $nama_pos,
							'jumlah' => $request[0]['jumlah']
						);
		echo json_encode($data_edit);
	}

	function addPengeluaran(){
		$id_proyek = $this->session->userdata('id_proyek');

		$no_nota = $this->no_notaGenerator($_POST['tgl_transaksi'],$id_proyek);

		$index_pos = $_POST['pilih_pos'];

		$jumlah_pengeluaran = str_replace(array('Rp. ','.'),array('',''), $_POST['jumlah_pengeluaran']);

		$data= array(	'id_proyek' => intval($id_proyek),
			'no_nota' => $no_nota,
			'tgl_transaksi' => $_POST['tgl_transaksi'],
			'jenis_pengeluaran' => $_POST['desc_pengeluaran'],
			$index_pos => intval($jumlah_pengeluaran)
		);

		$cekMaximalPos = $this->cekMaximalPos($data[$index_pos],$index_pos);

		// var_dump($cekMaximalPos);exit();

		if ($cekMaximalPos) {
			$request = $this->pengeluaran->addPengeluaran_model($data);
			if ($request) {
				$this->log('Add pengeluaran');

				$result['status'] = 'success';
				$result['message'] = 'Data berhasil diinputkan';
				$result['redirect_url'] = site_url('index.php/laporan_pengeluaran');
				$this->output->set_content_type('application/json');
				$this->output->set_output(json_encode($result));
				$string = $this->output->get_output();
				echo json_encode($result);
				exit();
			}
		}else{
			$result['status'] = 'failed';
			$result['message'] = 'Pengeluaran melebihi budget';
			$result['redirect_url'] = site_url('index.php/laporan_pengeluaran');
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($result));
			$string = $this->output->get_output();
			echo json_encode($result);
			exit();
		}
	}

	function updatePengeluaran(){
		$id_proyek = $this->session->userdata('id_proyek');

		$index_pos = $_POST['pilih_pos'];

		$jumlah_pengeluaran = str_replace(array('Rp. ','.'),array('',''), $_POST['jumlah_pengeluaran']);

		$data= array(	'id_pengeluaran' => intval($_POST['id_pengeluaran']),
			'tgl_transaksi' => $_POST['tgl_transaksi'],
			'jenis_pengeluaran' => $_POST['desc_pengeluaran'],
			$index_pos => intval($jumlah_pengeluaran));

		$datalama= array( 'jumlah_pengeluaran_lama' => $_POST['jumlah_pengeluaran_lama'],
							'pos_lama' => $_POST['pos_lama']);

		if ($index_pos == $datalama['pos_lama']) {
			$amount_pos_lama = intval($datalama['jumlah_pengeluaran_lama']);
		}else{
			$amount_pos_lama = 0;
		}

		$data_requester = array('id_admin' => $this->session->userdata('id_user'),
								'aksi' => 'edit',
								'id_proyek' => $id_proyek);

		if ($this->cekMaximalPosForUpdate($data[$index_pos],$index_pos,$amount_pos_lama)) {
			if ($this->session->userdata('hak_akses') == 'admin') {
				$this->request_aksi($data_requester,$data);

				$result['status'] = 'success';
				$result['message'] = 'Request berhasil dikirim';
				$result['redirect_url'] = site_url('index.php/laporan_pengeluaran');
				$this->output->set_content_type('application/json');
				$this->output->set_output(json_encode($result));
				$string = $this->output->get_output();
				echo json_encode($result);
				exit();
			}else{
				if ($this->pengeluaran->updatePengeluaran_model($data)) {
					$this->log('Update data pengeluaran');

					$result['status'] = 'success';
					$result['message'] = 'Data berhasil diinputkan';
					$result['redirect_url'] = site_url('index.php/laporan_pengeluaran');
					$this->output->set_content_type('application/json');
					$this->output->set_output(json_encode($result));
					$string = $this->output->get_output();
					echo json_encode($result);
					exit();
				}
			}
				
		}else{
			$result['status'] = 'failed';
			$result['message'] = 'Pengeluaran melebihi budget';
			$result['redirect_url'] = site_url('index.php/laporan_pengeluaran');
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($result));
			$string = $this->output->get_output();
			echo json_encode($result);
			exit();
		}
	}
	// archive pengganti delete
	function archivePengeluaran($id){
		$kode = $this->session->userdata('id_proyek');

		$data_requester = array('id_admin' => $this->session->userdata('id_user'),
								'aksi' => 'hapus',
								'id_proyek' => $kode);
		$data = array('id_pengeluaran' => $id);
		if(empty($id)){
			redirect('index.php/laporan_pengeluaran/');
		}else{
			try
			{
				if ($this->session->userdata('hak_akses') == 'admin') {
					$this->request_aksi($data_requester,$data);
				}else{
					$request = $this->pengeluaran->archivePengeluaran_model($id);
					$this->log('Archive data pengeluaran');
				}
			}catch( Exception $e){			
			}
			redirect('index.php/laporan_pengeluaran/');
		}
	}
	//export excel
	function export() {
		$data = array( 'title' => 'Laporan Excel Per Pos');

		$this->data['pengeluaran'] = $this->getAllPengeluaran();
		$this->data['pemasukan'] = $this->getAllPemasukan();
		$this->data['sisa_pos'] = $this->sisa_pos();
		$this->log('Export');
		$this->load->view('laporan_pengeluaran/export', $data, $this->data, FALSE);
	}
	//nota generate by sistem
	function no_notaGenerator($tanggal,$kode){
		$words = preg_split("/\s+/",$this->session->userdata('nama_proyek'));
		$acronym = "";

		foreach ($words as $key) {
			$acronym .= $key[0];
		}

		$kode = $this->getAllPengeluaran();
		$max_kode=0;
		
		if($kode){
			$max_kode = count($kode);
		}
		
		$date = date('m/y', strtotime($tanggal));
		if (empty($kode)) {
			$no_nota = strtoupper($acronym).'/1/'.$date;
		}else{
			$no_nota = strtoupper($acronym).'/'.($max_kode+1).'/'.$date;
		}

		return $no_nota;
	}
	//cek apakah pengeluaran masing masing pos sudah melebihi batas maksimal
	//pos yang sudah di set sebelumnya
	function cekMaximalPos($data,$index_pos){
		$budget = $this->project->getPos_model($this->session->userdata('id_proyek'));

		$pengeluaran = $this->getAllPengeluaran();
		// var_dump($pengeluaran);exit();

		for ($i=0; $i < count($pengeluaran) ; $i++) { 
			$pos_legal[] = $pengeluaran[$i]['pos_legal'];
			$pos_fasumfasos[] = $pengeluaran[$i]['pos_fasumfasos'];
			$pos_operasional[] = $pengeluaran[$i]['pos_operasional'];
			$pos_promosi[] = $pengeluaran[$i]['pos_promosi'];
			$pos_bangunan[] = $pengeluaran[$i]['pos_bangunan'];
			$pos_pajak[] = $pengeluaran[$i]['pos_pajak'];
		}

		if ($budget[0][$index_pos] < array_sum($$index_pos) + intval($data)) {
			return FALSE;
		}else{
			return TRUE;
		}
	}
	//cek apakah pengeluaran masing masing pos sudah melebihi batas maksimal
	//pos yang sudah di set sebelumnya, untuk proses update
	function cekMaximalPosForUpdate($data,$index_pos,$amount_pos_lama){
		$budget = $this->project->getPos_model($this->session->userdata('id_proyek'));

		$pengeluaran = $this->getAllPengeluaran();

		for ($i=0; $i < count($pengeluaran) ; $i++) { 
			$pos_legal[] = $pengeluaran[$i]['pos_legal'];
			$pos_fasumfasos[] = $pengeluaran[$i]['pos_fasumfasos'];
			$pos_operasional[] = $pengeluaran[$i]['pos_operasional'];
			$pos_promosi[] = $pengeluaran[$i]['pos_promosi'];
			$pos_bangunan[] = $pengeluaran[$i]['pos_bangunan'];
			$pos_pajak[] = $pengeluaran[$i]['pos_pajak'];
		}

		if ($budget[0][$index_pos] < array_sum($$index_pos) - $amount_pos_lama  + intval($data)) {
			return FALSE;
		}else{
			return TRUE;
		}
	}

	function sisa_pos() {
		$pos = $this->project->getPos_model($this->session->userdata('id_proyek'));
		$pengeluaran = $this->bulanan->get_amount_pengeluaran($this->session->userdata('id_proyek'));
		@$sisa_pos = array('legal' => $pos[0]['pos_legal'] - $pengeluaran[0]['pos_legal'],
							'fasumfasos' => $pos[0]['pos_fasumfasos'] - $pengeluaran[0]['pos_fasumfasos'],
							'operasional' => $pos[0]['pos_operasional'] - $pengeluaran[0]['pos_operasional'],
							'promosi' => $pos[0]['pos_promosi'] - $pengeluaran[0]['pos_promosi'],
							'bangunan' => $pos[0]['pos_bangunan'] - $pengeluaran[0]['pos_bangunan'],
							'pajak' => $pos[0]['pos_pajak'] - $pengeluaran[0]['pos_pajak']
						);
		return $sisa_pos;
	}

	function log($params){
		$log = array('id_pegawai' => $this->session->userdata('id_user'),
					'nama_pegawai' => $this->session->userdata('username'),
					'hak_akses' => $this->session->userdata('hak_akses'),
					'id_proyek' => $this->session->userdata('id_proyek'),
					'jenis_log' => $params);

		$this->m_log->addLog_model($log);
	}

	function request_aksi($data_requester,$data) {
		$value = serialize($data);
		$datas = array('id_admin' => $data_requester['id_admin'],
						'aksi' => $data_requester['aksi'],
						'id_proyek' => $data_requester['id_proyek'],
						'value' => $value);

		$request = $this->approvement->insert($datas);
	}

	function process_request($params) {
		$request = $this->approvement->get_request_byid($params);

		if ($request[0]['aksi'] == 'hapus') {
			$data_hapus = unserialize($request[0]['value']);
			
			if ($this->pengeluaran->archivePengeluaran_model($data_hapus['id_pengeluaran'])) {
				$this->approvement->delete($params);
				$this->log('Archive data pengeluaran');
			}
		}else{
			$data_update = unserialize($request[0]['value']);
			if ($this->pengeluaran->updatePengeluaran_model($data_update)) {
				$this->approvement->delete($params);
				$this->log('Update data pengeluaran');
			}
		}
		redirect('index.php/project/detailProject/'.$this->session->userdata('id_proyek'));
	}
}
?>