<?php 
/**
*Progammer	: Mashudi Rohmat & Bambang Wijayanto
*Projek 	: Manajemen Keuangan Proyek 2020
*Desc 		: Controller untuk manipulasi laporan pajak
*/
class Laporan_pajak extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		//load model
		$this->load->model('LaporanPajak_model','pajak');
		$this->load->model('Project_model','project');
		$this->load->model('Log_model','m_log');
	}

	function index(){
		if ($this->session->userdata('id_user')== null) {
			redirect('');
		}else{
			$data = array (	'title'		=>'Proyek',
				'isi'		=>'laporan_pajak/list',
				'js'		=>'laporan_pajak/js',
				'modal'		=>'laporan_pajak/modal'
			);
			$id = $this->session->userdata('id_proyek');
			$this->data['detail'] = $this->project->detailProject_model($id);
			if ($this->data['detail']) {
				$this->session->set_userdata('id_proyek',$this->data['detail'][0]['id_proyek']);
				$this->session->set_userdata('nama_proyek',$this->data['detail'][0]['nama_proyek']);
				$this->session->set_userdata('foto',$this->data['detail'][0]['foto']);
			}

			// $this->data['sisa_pos'] = $this->sisa_pos();
			$this->data['pos'] = $this->project->getPos_model($id);

			$this->data['laporan_pajak'] = $this->get_pemasukan_bypajak();

			//menampilkan detailproject berdasarkan id
			$this->data['project'] = $this->project->detailProject_model($id);

			$this->load->view('layout/wrapper',$data,$this->data, FALSE);
		}
	}

	function filter(){
		if ($this->session->userdata('id_user')== null) {
			redirect('');
		}else{
			$data = array (	'title'		=>'Proyek',
				'isi'		=>'laporan_pajak/list',
				'js'		=>'laporan_pajak/js',
				'modal'		=>'laporan_pajak/modal'
			);
			$id = $this->session->userdata('id_proyek');
			$this->data['detail'] = $this->project->detailProject_model($id);
			if ($this->data['detail']) {
				$this->session->set_userdata('id_proyek',$this->data['detail'][0]['id_proyek']);
				$this->session->set_userdata('nama_proyek',$this->data['detail'][0]['nama_proyek']);
				$this->session->set_userdata('foto',$this->data['detail'][0]['foto']);
			}
			$filter = array('pajak' => $this->input->post('filter_pajak'),
							'tanggal' => $this->input->post('filter_date'));
			// $this->data['sisa_pos'] = $this->sisa_pos();
			$this->data['pos'] = $this->project->getPos_model($id);

			$this->data['laporan_pajak'] = $this->get_pemasukan_bypajak($filter['pajak'],$filter['tanggal']);

			//menampilkan detailproject berdasarkan id
			$this->data['project'] = $this->project->detailProject_model($id);

			$this->load->view('layout/wrapper',$data,$this->data, FALSE);
		}
	}


	function get_pemasukan_bypajak($pajak = null,$tanggal = null) {
		$id_proyek = $this->session->userdata('id_proyek');
		if ($pajak == null) {
			$tax = 0;
		}else{
			$tax = $pajak/100;
		}

		if ($tanggal == null) {
			date_default_timezone_set('Asia/Jakarta');
			$filter = date("Y-m");
		}else{
			$filter = $tanggal;
		}
		$date = strtotime($filter);
		$this->session->set_userdata('filter_date',date('F Y',$date));
		$this->session->set_userdata('filter_pajak',$tax);
		$request = $this->pajak->get_pemasukan_bypajak($id_proyek,$tax,$filter);
		// var_dump($request);exit();
		return $request;
	}

	function export() {
		$data = array( 'title' => 'Laporan Pajak');
		$tanggal = strtotime($this->session->userdata('filter_date'));
		$filter_tanggal = date('Y-m',$tanggal);
		
		$pajak = $this->session->userdata('filter_pajak')*100;

		$this->data['laporan_pajak'] = $this->get_pemasukan_bypajak($pajak,$filter_tanggal);

		$this->log('Export laporan pajak');
		$this->load->view('laporan_pajak/export', $data, $this->data, FALSE);
	}

	function log($params){
		$log = array('id_pegawai' => $this->session->userdata('id_user'),
					'nama_pegawai' => $this->session->userdata('username'),
					'hak_akses' => $this->session->userdata('hak_akses'),
					'id_proyek' => $this->session->userdata('id_proyek'),
					'jenis_log' => $params);

		$this->m_log->addLog_model($log);
	}
}
 ?>