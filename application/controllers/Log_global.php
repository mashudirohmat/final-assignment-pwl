<?php 
/**
*Progammer	: Mashudi Rohmat & Bambang Wijayanto
*Projek 	: Manajemen Keuangan Proyek 2020
*Desc 		: Controller untuk manipulasi log global / di luar project
*/
class Log_global extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Log_model','m_log');
	}
	 function index() {
	 	if ($this->session->userdata('id_user')== null) {
	 		redirect('');
	 	}else{
	 		$data = array (	'title'		=>'Proyek',
	 			'isi'		=>'log_global/list',
	 			'js'		=>'log_global/js',
	 			'modal'		=>'log_global/modal'
	 		);
	 		$this->data['log_global'] = $this->m_log->get_logGlobal_model();
	 		$this->load->view('layout/wrapper',$data,$this->data, FALSE);
	 	}
	 }
}
 ?>