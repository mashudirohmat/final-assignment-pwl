<?php 
/**
*Progammer	: Mashudi Rohmat & Bambang Wijayanto
*Projek 	: Manajemen Keuangan Proyek 2020
*Desc 		: Controller untuk manipulasi pemasukan
*/
class Perjanjian extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		// load model
		$this->load->model('Perjanjian_model','perjanjian');
		$this->load->model('project_model','project');
		$this->load->model('Log_model','m_log');
	}
	//menampilkan view termin
	function index(){
		if ($this->session->userdata('id_user')== null) {
			redirect('');
		}else{
			$data = array (	'title'		=>'Perjanjian',
				'isi'		=>'perjanjian_termin/list',
				'js'		=>'perjanjian_termin/js',
				'modal'		=>'perjanjian_termin/modal'
			);
			$id = $this->input->get('id');
			$this->data['detailrumah'] = $this->perjanjian->detailRumah_model($id);
			if ($this->data['detailrumah']) {
				$this->session->set_userdata('id_rumah',$this->data['detailrumah'][0]['id_rumah']);
				$this->session->set_userdata('id_transaksi',$this->data['detailrumah'][0]['id_transaksi']);
				$this->session->set_userdata('nama_rumah',$this->data['detailrumah'][0]['nama_rumah']);
				$this->session->set_userdata('tipe_rumah',$this->data['detailrumah'][0]['tipe_rumah']);
				$this->session->set_userdata('harga_rumah',$this->data['detailrumah'][0]['harga_rumah']);
				$this->session->set_userdata('nama_pembeli',$this->data['detailrumah'][0]['nama_pembeli']);
				$this->session->set_userdata('alamat_pembeli',$this->data['detailrumah'][0]['alamat_pembeli']);
				$this->session->set_userdata('no_ktp',$this->data['detailrumah'][0]['no_ktp']);
				$this->session->set_userdata('no_kk',$this->data['detailrumah'][0]['no_kk']);
				$this->session->set_userdata('no_kk',$this->data['detailrumah'][0]['no_kk']);
				$this->session->set_userdata('no_telp',$this->data['detailrumah'][0]['no_telp']);
				$this->session->set_userdata('harga_rumah',$this->data['detailrumah'][0]['harga_rumah']);
			}
			$this->data['termin'] = $this->getAllTermin($this->session->userdata('id_transaksi'));
			$this->data['is_lunas'] = $this->is_lunas($this->data['detailrumah']);
			$this->load->view('layout/wrapper',$data,$this->data, FALSE);
		}
	}

	/**
	*Function ini untuk manipulasi pemasukan 
	*/
	function getAllTermin($id){
		$request = $this->perjanjian->getAllTermin_model($id);
		return $request->result_array();
	}
	function getTerminById(){
		$id = $_POST['id'];
		$request = $this->perjanjian->getTerminById_model($id);
		return $request->result_array();
	}
	function get_termin_byid_json(){
		$id = $_POST['id'];
		$request= $this->perjanjian->getTerminById_model($id)->result_array();
		echo json_encode($request);
	}
	function getAllPemasukan(){
		$id = $this->session->userdata('id_proyek');
		$data= $this->perjanjian->getAllPemasukan_model($id);
		return $data->result_array();
	}
	function is_lunas($data){
		$termin = $this->getAllTermin($this->session->userdata('id_transaksi'));
		$harga_rumah = $data[0]['harga_rumah'];
		$jumlah = 0;
		for ($i=0; $i < count($termin) ; $i++) { 
			$jumlah += $termin[$i]['jumlah'];
		}
		if ($harga_rumah == $jumlah) {
			return true;
		}else{
			return false;
		}
	}
	function get_max_pemasukan_kode(){
		$request = $this->perjanjian->get_max_pemasukan_kode()->result_array();
		$max_id = $request[0]['id_pemasukan_max'];
		$next_id = intval($max_id)+1;
		return  $next_id;
	}
	function addTermin(){
		$id_proyek = $this->session->userdata('id_proyek');
		$id_transaksi = $this->session->userdata('id_transaksi');
		$id_rumah = $this->session->userdata('id_rumah');
		$jumlah_bayar = str_replace(array('Rp. ','.'),array('',''), $_POST['jumlah_perjanjian']);
			// var_dump($jumlah);exit();
		$datapemasukan= array(	'id_proyek' 		=> intval($id_proyek),
								'id_transaksi'		=> intval($id_transaksi),
								'id_rumah' 			=> intval($id_rumah),
								'keterangan' 		=> $_POST['keterangan_perjanjian'],
								'jumlah' 			=> floatval($jumlah_bayar),
								'tgl_jatuh_tempo' => $_POST['tgl_jatuh_tempo']
							);
		if ($this->perjanjian->addTermin_model($datapemasukan)) {
			//log
			$this->log('Tambah kontrak termin: '.$this->session->userdata('nama_rumah').', pemilik:'.$this->session->userdata('nama_pembeli'));
			//response
			$result['status'] = 'success';
			$result['message'] = 'Data berhasil diinputkan';
		}else{
			$result['status'] = 'failed';
			$result['message'] = 'Data gagal diinputkan';
		}

		$result['status'];
		$result['message'];
		$result['redirect_url'] = site_url('index.php/perjanjian?id='.$id_transaksi);
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($result));
		$string = $this->output->get_output();
		echo json_encode($result);
		exit();
	}

	function update_termin(){
		$id_proyek = $this->session->userdata('id_proyek');
		$id_transaksi = $this->session->userdata('id_transaksi');
		$jumlah_bayar = str_replace(array('Rp. ','.'),array('',''), $_POST['jumlah_perjanjian']);
			// var_dump(intval($jumlah_bayar));exit();
		$datapemasukan= array(
			'id_perjanjian' => intval($_POST['id_perjanjian']),
			'keterangan' => $_POST['keterangan_perjanjian'],
			'jumlah' => intval($jumlah_bayar),
			'tgl_jatuh_tempo' => $_POST['tgl_jatuh_tempo']
		);

		if ($this->perjanjian->update_termin_model($datapemasukan)) {
			$this->log('Ubah kontrak termin: '.$this->session->userdata('nama_rumah').', pemilik: '.$this->session->userdata('nama_pembeli'));

			$result['status'] = 'success';
			$result['message'] = 'Data berhasil diupdate';
		}else{
			$result['status'] = 'failed';
			$result['message'] = 'Data gagal diupdate';
		}

		$result['status'];
		$result['message'];
		$result['redirect_url'] = site_url('index.php/perjanjian?id='.$id_transaksi);
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($result));
		$string = $this->output->get_output();
		echo json_encode($result);
		exit();
	}

	function archive_pemasukan($id){
		$kode = $this->session->userdata('id_transaksi');
		// var_dump($kode);exit();
		if(empty($id)){
			redirect('index.php/perjanjian?id='.$kode);
		}else{
			try
			{
				$request = $this->perjanjian->arsip_pemasukan_model($id);
				$this->log('Archive kontrak termin: '.$this->session->userdata('nama_rumah').', pemilik: '.$this->session->userdata('nama_pembeli'));
			}catch( Exception $e){			
			}
			redirect('index.php/perjanjian?id='.$kode);
		}
	}

	function verivikasi_lunas() {
		$id_transaksi = $this->session->userdata('id_transaksi');
		$data = array('is_lunas' => $_POST['is_lunas']);
		
		if ($this->perjanjian->verivikasi_lunas($id_transaksi,$data)) {
			redirect('index.php/perjanjian?id='.$id_transaksi);
		}
	}

	function log($params){
		$log = array('id_pegawai' => $this->session->userdata('id_user'),
					'nama_pegawai' => $this->session->userdata('username'),
					'hak_akses' => $this->session->userdata('hak_akses'),
					'id_proyek' => $this->session->userdata('id_proyek'),
					'jenis_log' => $params);

		$this->m_log->addLog_model($log);
	}
}
 ?>