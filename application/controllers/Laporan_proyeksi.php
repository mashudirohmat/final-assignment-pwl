<?php 
/**
*Progammer	: Mashudi Rohmat & Bambang Wijayanto
*Projek 	: Manajemen Keuangan Proyek 2020
*Desc 		: Controller untuk manipulasi laporan proyeksi
*/
class Laporan_proyeksi extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('LaporanProyeksi_model','proyeksi');
		$this->load->model('Project_model','project');
		$this->load->model('Log_model','m_log');
	}

	function index() {
		if ($this->session->userdata('id_user')== null) {
			redirect('');
		}else{
			$data = array (	'title'		=>'Proyeksi',
				'isi'		=>'laporan_proyeksi/list',
				'js'		=>'laporan_proyeksi/js',
				'modal'		=>'laporan_proyeksi/modal'
			);
			$id = $this->session->userdata('id_proyek');
			$this->data['detail'] = $this->project->detailProject_model($id);
			if ($this->data['detail']) {
				$this->session->set_userdata('id_proyek',$this->data['detail'][0]['id_proyek']);
				$this->session->set_userdata('nama_proyek',$this->data['detail'][0]['nama_proyek']);
				$this->session->set_userdata('foto',$this->data['detail'][0]['foto']);
			}

			// $this->data['sisa_pos'] = $this->sisa_pos();
			$this->data['pos'] = $this->getPos();

			$this->data['laporan_proyeksi'] = $this->get_laporan_proyeksi();

			//menampilkan detailproject berdasarkan id
			$this->data['project'] = $this->project->detailProject_model($id);

			$this->load->view('layout/wrapper',$data,$this->data, FALSE);
		}
	}

	function filter(){
		if ($this->session->userdata('id_user')== null) {
			redirect('');
		}else{
			$data = array (	'title'		=>'Proyeksi',
				'isi'		=>'laporan_proyeksi/list',
				'js'		=>'laporan_proyeksi/js',
				'modal'		=>'laporan_proyeksi/modal'
			);
			$id = $this->session->userdata('id_proyek');
			$this->data['detail'] = $this->project->detailProject_model($id);
			if ($this->data['detail']) {
				$this->session->set_userdata('id_proyek',$this->data['detail'][0]['id_proyek']);
				$this->session->set_userdata('nama_proyek',$this->data['detail'][0]['nama_proyek']);
				$this->session->set_userdata('foto',$this->data['detail'][0]['foto']);
			}
			$filter = array('pajak' => $this->input->post('filter_pajak'),
							'tanggal' => $this->input->post('filter_date'));
			// $this->data['sisa_pos'] = $this->sisa_pos();
			$this->data['pos'] = $this->project->getPos_model($id);

			$this->data['laporan_proyeksi'] = $this->get_laporan_proyeksi($filter['tanggal']);

			//menampilkan detailproject berdasarkan id
			$this->data['project'] = $this->project->detailProject_model($id);

			$this->load->view('layout/wrapper',$data,$this->data, FALSE);
		}
	}

	function getPos(){
		$id = $this->session->userdata('id_proyek');
		$request = $this->project->getPos_model($id);
		return $request;
	}

	function get_laporan_proyeksi($tanggal = null) {
		$id_proyek = $this->session->userdata('id_proyek');

		if ($tanggal == null) {
			date_default_timezone_set('Asia/Jakarta');
			$filter = date("Y-m");
		}else{
			$filter = $tanggal;
		}
		$date = strtotime($filter);
		$this->session->set_userdata('filter_date',date('F Y',$date));
		$request = $this->proyeksi->get_laporan_proyeksi($id_proyek,$filter);

		return $request;
	}

	function export() {
 		$data = array( 'title' => 'Laporan Excel Proyeksi');
 		$tanggal = strtotime($this->session->userdata('filter_date'));
		$filter_tanggal = date('Y-m',$tanggal);

		$this->data['laporan_proyeksi'] = $this->get_laporan_proyeksi($filter_tanggal);
		
		$this->log('Export laporan proyeksi');
		$this->load->view('laporan_proyeksi/export', $data, $this->data, FALSE);
	}

	function log($params){
		$log = array('id_pegawai' => $this->session->userdata('id_user'),
					'nama_pegawai' => $this->session->userdata('username'),
					'hak_akses' => $this->session->userdata('hak_akses'),
					'id_proyek' => $this->session->userdata('id_proyek'),
					'jenis_log' => $params);

		$this->m_log->addLog_model($log);
	}


}
 ?>