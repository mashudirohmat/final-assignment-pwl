<?php 
/**
*Progammer	: Mashudi Rohmat & Bambang Wijayanto
*Projek 	: Manajemen Keuangan Proyek 2020
*Desc 		: Controller untuk manipulasi log per project
*/
class Log extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Log_model','lm');
		$this->load->model('Project_model','pm');
	}

	function index() {
		if ($this->session->userdata('id_user')== null) {
			redirect('');
		}else{
			$data = array (	'title'		=>'Proyek',
				'isi'		=>'log/list',
				'js'		=> 'log/js',
				'modal'		=>'log/modal'
			);
			$id = $this->session->userdata('id_proyek');
			$this->data['detail'] = $this->pm->detailProject_model($id);
			if ($this->data['detail']) {
				$this->session->set_userdata('id_proyek',$this->data['detail'][0]['id_proyek']);
				$this->session->set_userdata('nama_proyek',$this->data['detail'][0]['nama_proyek']);
				$this->session->set_userdata('foto',$this->data['detail'][0]['foto']);
			}
			$this->data['log'] = $this->getLog();

			//menampilkan detailproject berdasarkan id
			$this->data['project'] = $this->pm->detailProject_model($id);

			$this->load->view('layout/wrapper',$data,$this->data, FALSE);
		}
	}

	function addLog($data){
		$request = $this->lm->addLog_model($data);
	}

	function getLog(){
		$id = $this->session->userdata('id_proyek');
		$request = $this->lm->getLog_model($id);
		return $request;
	}
}
 ?>