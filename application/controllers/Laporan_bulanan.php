<?php 
/**
*Progammer	: Mashudi Rohmat & Bambang Wijayanto
*Projek 	: Manajemen Keuangan Proyek 2020
*Desc 		: Controller untuk manipulasi laporan bulanan
*/
class Laporan_bulanan extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('LaporanBulanan_model','bulanan');
		$this->load->model('Project_model','project');
		$this->load->model('Log_model','m_log');
	}

	function index(){
		if ($this->session->userdata('id_user')== null) {
			redirect('');
		}else{
			$data = array (	'title'		=>'Proyek',
				'isi'		=>'laporan_bulanan/list',
				'js'		=>'laporan_bulanan/js',
				'modal'		=>'laporan_bulanan/modal'
			);
			$id = $this->session->userdata('id_proyek');
			$this->data['detail'] = $this->project->detailProject_model($id);
			if ($this->data['detail']) {
				$this->session->set_userdata('id_proyek',$this->data['detail'][0]['id_proyek']);
				$this->session->set_userdata('nama_proyek',$this->data['detail'][0]['nama_proyek']);
				$this->session->set_userdata('foto',$this->data['detail'][0]['foto']);
			}

			$this->data['sisa_pos'] = $this->sisa_pos();
			$this->data['pos'] = $this->getPos();

			$this->data['laporan_bulanan'] = $this->get_laporan_bulanan();

			//menampilkan detailproject berdasarkan id
			$this->data['project'] = $this->project->detailProject_model($id);

			$this->load->view('layout/wrapper',$data,$this->data, FALSE);
		}
	}

	function filter(){
		if ($this->session->userdata('id_user')== null) {
			redirect('');
		}else{
			$data = array (	'title'		=>'Proyek',
				'isi'		=>'laporan_bulanan/list',
				'js'		=>'laporan_bulanan/js',
				'modal'		=>'laporan_bulanan/modal'
			);
			$id = $this->session->userdata('id_proyek');
			$this->data['detail'] = $this->project->detailProject_model($id);
			if ($this->data['detail']) {
				$this->session->set_userdata('id_proyek',$this->data['detail'][0]['id_proyek']);
				$this->session->set_userdata('nama_proyek',$this->data['detail'][0]['nama_proyek']);
				$this->session->set_userdata('foto',$this->data['detail'][0]['foto']);
			}
			$filter = array('tanggal' => $this->input->post('filter_date'));
			
			$this->data['sisa_pos'] = $this->sisa_pos();
			$this->data['pos'] = $this->getPos();

			$this->data['laporan_bulanan'] = $this->get_laporan_bulanan($filter['tanggal']);

			//menampilkan detailproject berdasarkan id
			$this->data['project'] = $this->project->detailProject_model($id);

			$this->load->view('layout/wrapper',$data,$this->data, FALSE);
		}
	}

	function getPos(){
		$id = $this->session->userdata('id_proyek');
		$request = $this->project->getPos_model($id);
		return $request;
	}

	function get_pengeluaran_bulanan($bulan){
		$id = $this->session->userdata('id_proyek');
		$request = $this->bulanan->get_pengeluaran_bulanan($id,$bulan);

		$data['legal'] = 0;
		$data['fasumfasos'] = 0;
		$data['operasional'] = 0;
		$data['promosi'] = 0;
		$data['bangunan'] = 0;
		$data['pajak'] = 0;

		for ($i=0; $i < count($request); $i++) { 
			$data['legal'] += $request[$i]['pos_legal'];
			$data['fasumfasos'] += $request[$i]['pos_fasumfasos'];
			$data['operasional'] += $request[$i]['pos_operasional'];
			$data['promosi'] += $request[$i]['pos_promosi'];
			$data['bangunan'] += $request[$i]['pos_bangunan'];
			$data['pajak'] += $request[$i]['pos_pajak'];
		}
		$data['origin'] = $request;
	// var_dump($data);exit();
		return $data;
	}

	function get_pemasukan_bulanan($bulan){
		$id = $this->session->userdata('id_proyek');
		$request = $this->bulanan->get_pemasukan_bulanan($id,$bulan);

		$data['origin'] = $request;
		$data['jumlah'] = 0;
		for ($i=0; $i < count($request) ; $i++) { 
			$data['jumlah'] += $request[$i]['jumlah'];
		}
	// var_dump($data);exit();
		return $data;
	}

	function get_laporan_bulanan($bulan = null){
		if ($bulan == null) {
			date_default_timezone_set('Asia/Jakarta');
			$filter = date("Y-m");
		}else{
			$filter = $bulan;
		}

		$date = strtotime($filter);
		$this->session->set_userdata('filter_date',date('F Y',$date));

		$data['pemasukan'] = $this->get_pemasukan_bulanan($filter);
		$data['pengeluaran'] = $this->get_pengeluaran_bulanan($filter);

		return $data;
	}

	function sisa_pos() {
		$pos = $this->project->getPos_model($this->session->userdata('id_proyek'));
		$pengeluaran = $this->bulanan->get_amount_pengeluaran($this->session->userdata('id_proyek'));
		@$sisa_pos = array('legal' => $pos[0]['pos_legal'] - $pengeluaran[0]['pos_legal'],
							'fasumfasos' => $pos[0]['pos_fasumfasos'] - $pengeluaran[0]['pos_fasumfasos'],
							'operasional' => $pos[0]['pos_operasional'] - $pengeluaran[0]['pos_operasional'],
							'promosi' => $pos[0]['pos_promosi'] - $pengeluaran[0]['pos_promosi'],
							'bangunan' => $pos[0]['pos_bangunan'] - $pengeluaran[0]['pos_bangunan'],
							'pajak' => $pos[0]['pos_pajak'] - $pengeluaran[0]['pos_pajak']
						);
		return $sisa_pos;
	}

	function export() {
 		$data = array( 'title' => 'Laporan Excel Bulanan');
 		$tanggal = strtotime($this->session->userdata('filter_date'));
		$filter_tanggal = date('Y-m',$tanggal);

		$this->data['laporan_bulanan'] = $this->get_laporan_bulanan($filter_tanggal);
		$this->data['sisa_pos'] = $this->sisa_pos();
		
		$this->log('Export laporan bulanan');
		$this->load->view('laporan_bulanan/export', $data, $this->data, FALSE);
	}

	function log($params){
		$log = array('id_pegawai' => $this->session->userdata('id_user'),
					'nama_pegawai' => $this->session->userdata('username'),
					'hak_akses' => $this->session->userdata('hak_akses'),
					'id_proyek' => $this->session->userdata('id_proyek'),
					'jenis_log' => $params);

		$this->m_log->addLog_model($log);
	}
}
 ?>