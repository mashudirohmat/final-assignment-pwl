<?php 

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=$title.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<table border="1" id="tabel_pajak" width="100%">
  <thead>
    <tr>
      <th colspan="11">
        <div class="row">
          <div class="col-md-2">
            <img src="<?php echo base_url('assets/img/'.$this->session->userdata('foto')) ?>" width="50">
          </div>
          <div class="col-md-8">
            <div class="text-center">
              <p><?php echo strtoupper($this->session->userdata('nama_proyek')); ?><hr>
                REKAP LAPORAN PAJAK PER BULAN<br>
                BULAN <?php echo strtoupper($this->session->userdata('filter_date')) ?></p>
              </div>
            </div>
            <div class="col-md-2">
              <br>
            </div>
          </div>
        </th>
      </tr>
      <tr>
        <th class="text-center">NO</th>
        <th colspan="2" class="text-center">TANGGAL</th>
        <th colspan="4" class="text-center">KETERANGAN</th>
        <th colspan="4" class="text-center">JUMLAH</th>
      </tr>
  </thead>
    <tbody>
      <?php
      $no = 1;
      foreach ($this->data['laporan_pajak'] as $key): ?>
        <tr>
          <td class="text-center" width="10"><?php echo $no ?></td>
          <td colspan="2" class="text-center" width="20"><?php echo $key['tanggal'] ?></td>
          <td colspan="4" width="100"><?php echo $key['keterangan'] ?></td>
          <td colspan="4" class="text-center"  width="30"><?php echo 'Rp. '.number_format($key['jumlah'],0,',','.') ?></td>
        </tr>
        <?php $no++;endforeach; ?>
        <tr>
          <?php
          $jumlah = 0; 
          for ($i=0; $i < count($this->data['laporan_pajak']) ; $i++) { 
            $jumlah += $this->data['laporan_pajak'][$i]['jumlah'];
          } ?>
          <td colspan="7" class="text-center"><b>TOTAL</td>
            <td colspan="4" class="text-center"><b><?php echo 'Rp. '.number_format($jumlah,0,',','.') ?></b></td>
          </tr>
  </tbody>
</table>
<table border="0" width="50%">
  <thead>
    <tr>
      <th></th>
      <th rowspan="6" colspan="11"><br>Yogyakarta, <?php echo date("l jS \of F Y") ?><br> Dibuat Oleh <br><br><br> <?php echo strtoupper($this->session->userdata('username')); ?> </th>
    </tr>
  </thead>
  <tbody></tbody>
  <tr></tr>
  <tr></tr>
</table>