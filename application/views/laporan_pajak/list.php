<?php foreach ($this->data['project'] as $key):?>
<?php require_once(APPPATH.'views/layout/nav_proyek.php'); ?>
<?php endforeach ?>
<!-- breadcrumb menu -->
<div class="container">
        <!-- laporan tahunan -->
        <?php if ($this->data['pos'] == null) {?>
        <div class="card">
          <div class="card-header">
            <div class="text-left">
              <p>Belum ada data</p>
            </div>
          </div>
        </div><br>
        <?php }else{?>
        <div class="card">
          <div class="card-header">
            <div class="row">
              <div class="col-md-2">
                <div class="text-left">
                  <p>Laporan Pajak</p>
                  <hr style="height:2px;border-width:0;color:blue;background-color:blue;width:50px;text-align:left;margin-left:0">
                </div>
              </div>
              <div class="col-md-1"></div>
              <div class="col-md-6">
                <div class="text-right">
                  <form class="form-inline" method="post" action="<?php echo base_url('laporan_pajak/filter') ?>">
                    <div class="form-group mb-2">
                      <input type="number" name="filter_pajak" class="form-control form-control-sm" placeholder="Pajak (<?php echo $this->session->userdata('filter_pajak')*100 ?> %)">
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                      <div class='input-group date' id='datetimepicker6'>
                        <input type='text' name="filter_date" id="filter_date" class="form-control form-control-sm" data-date-format="YYYY-MM" value="<?php date_default_timezone_set('Asia/Jakarta'); echo date("y/m") ?>" />
                        <div class="input-group-addon">
                        <div class="input-group-append">
                          <span class="input-group-text">
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                          </span>
                        </div>
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                    <button type="submit" class="btn btn-primary mb-2 form-control form-control-sm">
                      <i class="fa fa-search"></i>
                    </button>
                    </div>
                  </form>
                </div>
              </div>
              <div class="col-md-1">
                <a href="<?php echo base_url('laporan_pajak') ?>" class="btn btn-info btn-sm">
                  <i class="fa fa-retweet"></i>
                </a>
              </div>
              <div class="col-md-2" >
                <div class="text-right">
                  <a  href="<?php echo base_url('laporan_pajak/export') ?>" class="btn btn-sm btn-success" id="export_pajak" target="_blank"> <i class="fas fa-print fa-sm text-right"></i> &nbsp; Export Excel</a>
                </div>
              </div>
            </div>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
              <div class="text-xs">
                <?php if ($this->data['laporan_pajak'] == null) {?>
                <p>Data kosong</p>
                <?php }else{ ?>
                <table border="1" id="tabel_pajak" width="100%">
                  <thead>
                    <tr>
                      <th colspan="10">
                        <div class="row">
                          <div class="col-md-2">
                            <!-- <img src="<?php //echo base_url('assets/img/'.$this->session->userdata('foto')) ?>" width="50"> -->
                          </div>
                          <div class="col-md-8">
                            <div class="text-center">
                              <p><?php echo strtoupper($this->session->userdata('nama_proyek')); ?><hr>
                                REKAP LAPORAN PAJAK PER BULAN<br>
                                BULAN <?php echo strtoupper($this->session->userdata('filter_date')) ?></p>
                              </div>
                            </div>
                            <div class="col-md-2">
                              <br>
                            </div>
                          </div>
                        </th>
                      </tr>
                      <tr>
                        <th class="text-center">NO</th>
                        <th class="text-center">TANGGAL</th>
                        <th class="text-center">KETERANGAN</th>
                        <th class="text-center">JUMLAH</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $no = 1;
                      foreach ($this->data['laporan_pajak'] as $key): ?>
                      <tr>
                        <td class="text-center" width="10"><?php echo $no ?></td>
                        <td class="text-center" width="20"><?php echo $key['tanggal'] ?></td>
                        <td width="100"><?php echo $key['keterangan'] ?></td>
                        <td class="text-center"  width="30"><?php echo 'Rp. '.number_format($key['jumlah'],0,',','.') ?></td>
                      </tr>
                      <?php $no++;endforeach; ?>
                      <tr>
                        <?php
                        $jumlah = 0; 
                        for ($i=0; $i < count($this->data['laporan_pajak']) ; $i++) { 
                          $jumlah += $this->data['laporan_pajak'][$i]['jumlah'];
                        } ?>
                        <td colspan="3" class="text-center"><b>TOTAL</td>
                        <td class="text-center"><b><?php echo 'Rp. '.number_format($jumlah,0,',','.') ?></b></td>
                      </tr>
                    </tbody>
                  </table>
                  <?php } ?>
                </div>
              </div>
              </div>
            </div>
          </div>
          <br>
          <!-- end of laporan tahunan -->

          <?php } ?>
          <br><br>

        </div>