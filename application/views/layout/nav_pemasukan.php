<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
	<!-- subheader -->
	<!--begin::Subheader-->
	<div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
		<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
			<!--begin::Info-->
			<div class="d-flex align-items-center flex-wrap mr-2">
				<!--begin::Page Title-->
				<div class="text-dark font-weight-bold mt-2 mb-2 mr-5">
				<h1 ><?php echo $this->session->userdata('nama_proyek') ?></h1>
				<br>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href=""><?php echo $this->session->userdata('nama_proyek') ?></a></li>
					<li class="breadcrumb-item active" aria-current="page">
						<?php echo $this->session->userdata('nama_rumah') ?>
					</li>
					<li class="breadcrumb-item active" aria-current="page">
						<?php echo $this->uri->segment(1) ?>
					</li>
				</ol>
				</div>
				<!--end::Page Title-->
			</div>
			<!--end::Info-->
			<!--begin::Toolbar-->
			<div class="d-flex align-items-center flex-wrap">
				<!--begin::Header Menu-->
				<div id="kt_header_menu" class="header-menu header-menu-left header-menu-mobile header-menu-layout-default">
					<!--begin::Header Nav-->
					<ul class="menu-nav">
						<li class="menu-item menu-item" aria-haspopup="true">
							<a href="<?php echo base_url('index.php/project/detailProject/'.$this->session->userdata('id_proyek')) ?>" class="menu-link">
								<span class="menu-text">Detail</span>
							</a>
						</li>
						<li class="menu-item menu-item-submenu menu-item-rel">
							<a href="<?php echo base_url('index.php/log/') ?>" class="menu-link">
								<span class="menu-text">Log</span>
								<span class="menu-desc"></span>
								<i class="menu-arrow"></i>
							</a>
						</li>
						<li class="menu-item menu-item-submenu menu-item-rel">
							<a href="<?php echo base_url('index.php/laporan_pajak/') ?>" class="menu-link">
								<span class="menu-text">Pajak</span>
								<span class="menu-desc"></span>
								<i class="menu-arrow"></i>
							</a>
						</li>
						<li class="menu-item menu-item-submenu menu-item-rel" aria-haspopup="true">
							<a href="<?php echo base_url('index.php/laporan_proyeksi/') ?>" class="menu-link">
								<span class="menu-text">Proyeksi</span>
								<span class="menu-desc"></span>
								<i class="menu-arrow"></i>
							</a>
						</li>
						<li class="menu-item menu-item-submenu" aria-haspopup="true">
							<a href="<?php echo base_url('index.php/laporan_pengeluaran/') ?>" class="menu-link">
								<span class="menu-text">Pengeluaran</span>
								<span class="menu-desc"></span>
								<i class="menu-arrow"></i>
							</a>
						</li>
						<li class="menu-item menu-item-submenu" aria-haspopup="true">
							<a href="<?php echo base_url('index.php/laporan_bulanan/') ?>" class="menu-link">
								<span class="menu-text">Lap Bulanan</span>
								<span class="menu-desc"></span>
								<i class="menu-arrow"></i>
							</a>
						</li>
						<li class="menu-item menu-item-submenu" aria-haspopup="true">
							<a href="<?php echo base_url('index.php/laporan_tahunan/') ?>" class="menu-link">
								<span class="menu-text">Lap Tahunan</span>
								<span class="menu-desc"></span>
								<i class="menu-arrow"></i>
							</a>
						</li>
					</ul>
					<!--end::Header Nav-->
				</div>
				<!--end::Header Menu-->
			</div>
			<!--end::Toolbar-->
		</div>
	</div>
  <!-- end of subheader -->