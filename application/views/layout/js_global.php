<script>var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";</script>
<!--begin::Global Config(global config for global JS scripts)-->
<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#0BB783", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#D7F9EF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
<!--end::Global Config-->
<!--begin::Global Theme Bundle(used by all pages)-->
<script src="<?php echo base_url('assets_new/plugins/global/plugins.bundle.js')?>"></script>
<script src="<?php echo base_url('assets_new/plugins/custom/prismjs/prismjs.bundle.js')?>"></script>
<script src="<?php echo base_url('assets_new/js/scripts.bundle.js')?>"></script>
<!--end::Global Theme Bundle-->
<!--begin::Page Vendors(used by this page)-->
<script src="<?php echo base_url('assets_new/plugins/custom/fullcalendar/fullcalendar.bundle.js')?>"></script>
<!--end::Page Vendors-->
<!--begin::Page Scripts(used by this page)-->
<script src="<?php echo base_url('assets_new/js/pages/widgets.js')?>"></script>
<script src="<?php echo base_url('assets_new/js/pages/crud/ktdatatable/base/data-local.js')?>"></script>


  <script src="<?php echo base_url('startbootstrap-sb-admin-2-gh-pages/vendor/jquery/jquery.min.js') ?>"></script>
  <script src="<?php echo base_url('startbootstrap-sb-admin-2-gh-pages/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
  <script src="<?php echo base_url('startbootstrap-sb-admin-2-gh-pages/vendor/jquery-easing/jquery.easing.min.js') ?>"></script>
  <script src="<?php echo base_url('startbootstrap-sb-admin-2-gh-pages/js/sb-admin-2.min.js') ?>"></script>
  <script src="<?php echo base_url('startbootstrap-sb-admin-2-gh-pages/vendor/chart.js/Chart.min.js') ?>"></script>
  <script src="<?php echo base_url('startbootstrap-sb-admin-2-gh-pages/js/demo/chart-area-demo.js') ?>"></script>
  <script src="<?php echo base_url('startbootstrap-sb-admin-2-gh-pages/js/demo/chart-pie-demo.js') ?>"></script>
  <script src="<?php echo base_url('startbootstrap-sb-admin-2-gh-pages/vendor/datatables/jquery.dataTables.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/Moment.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrap-datetimepicker.min.js') ?>"></script>
  <script src="<?php echo base_url('startbootstrap-sb-admin-2-gh-pages/vendor/datatables/dataTables.bootstrap4.min.js') ?>"></script>
  <script src="<?php echo base_url('startbootstrap-sb-admin-2-gh-pages/js/demo/datatables-demo.js') ?>"></script>
  <script type="text/javascript" src="<?= base_url('assets/selectize/standalone/selectize.min.js')?>"></script>

<script type="text/javascript">
    $(function() {
    $('#datetimepicker3').datetimepicker({locale:'id',format : "yyyy/mm/dd"});
  });
    $(function() {
    $('#datetimepicker4').datetimepicker({locale:'id',format : "yyyy/mm/dd"});
  });
    $(function() {
    $('#datetimepicker5').datetimepicker({locale:'id',format : "dd"});
  });
    $(function() {
    $('#datetimepicker6').datetimepicker({locale:'id',format : "mm/yyyy"});
  });
  </script>

  <script type="text/javascript">
    $(function () {
      $('#tabel1').DataTable()
    });
    $(function () {
      $('#tabel2').DataTable()
    });
    $(function () {
      $('#tabel3').DataTable()
    });
   

   function tambahproyek(){
      if ($("[name='namaproyek']").val()=='') {
        $('#namaproyek_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='lokasiproyek']").val()==''){
        $('#lokasiproyek_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='admin[]']").val()==''){
        $('#admin_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='mandor[]']").val()=='Pilih hak akses . . .'){
        $('#mandor_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else{
        $('#btn-tambahpr').html("<i class='fa fa-spinner fa-spin'></i> Saving...");
        $('#btn-tambahpr').attr('disabled',true);
        var formData = new FormData($('#formproject')[0]);

        $.ajax({
          type:'POST',
          data: formData,
          contentType: false,
          processData: false,
          url:'<?php echo base_url('index.php/project/addProject') ?>',
          dataType : 'JSON',
          success : function(hasil){
            console.log(hasil.error_string);
            if (hasil.status=='success') {
              $("#formadd").modal('hide');

              $("[name='namaproyek']").val('');
              $("[name='lokasiproyek']").val('');
              $("[name='admin']").val('');
              $("[name='mandor']").val('');
              $("[name='foto']").val('');

              window.location.href = hasil.redirect_url;
            }else{
              $('#error_message').text(hasil.error_string).fadeIn().delay(3000).fadeOut();
            }
          }
        });
      }
    }
    
    function ubahproyek(){
      if ($("[name='namaproyek']").val()=='') {
        $('#namaproyek_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='lokasiproyek']").val()==''){
        $('#lokasiproyek_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='admin[]']").val()==''){
        $('#admin_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='mandor[]']").val()=='Pilih hak akses . . .'){
        $('#mandor_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else{
        $('#btn-ubahpr').html("<i class='fa fa-spinner fa-spin'></i> Updating...");
        $('#btn-ubahpr').attr('disabled',true);
        var formData = new FormData($('#formproject')[0]);

        $.ajax({
          type:'POST',
          data: formData,
          contentType: false,
          processData: false,
          url:'<?php echo base_url('index.php/project/updateProject') ?>',
          dataType : 'JSON',
          success : function(hasil){
            console.log(hasil.error_string);
            if (hasil.status=='success') {
              $("#formadd").modal('hide');

              $("[name='namaproyek']").val('');
              $("[name='lokasiproyek']").val('');
              $("[name='admin']").val('');
              $("[name='mandor']").val('');
              $("[name='foto']").val('');
              $("[name='id']").val('');

              window.location.href = hasil.redirect_url;
            }else{
              $('#error_message').text(hasil.error_string).fadeIn().delay(3000).fadeOut();
            }
          }
        });
      }
    }

    function submitpr(x){
      if (x=='tambah') {
        $('#btn-tambahpr').show();
        $('#btn-ubahpr').hide();
        $('#photo-preview').hide();
        $('#modal-titlepr').html('<b>Form Tambah Proyek</b>');

        $("[name='namaproyek']").val('');
        $("[name='lokasiproyek']").val('');
        $("[name='admin']").val('');
        $("[name='mandor']").val('');
        $("[name='foto']").val('');

      }else{
        $('#btn-tambahpr').hide();
        $('#btn-ubahpr').show();
        $('#btn-ubahpr').attr('disabled',false);
        $('#btn-ubahpr').html('Ubah <i class="fa fa-paper-plane-o ml-1"></i>');
        $('#modal-titlepr').html('<b>Form Ubah Proyek</b>');

        $.ajax({
          type:"POST",
          data:'id='+x,
          url:'<?php echo base_url('index.php/project/getProjectById') ?>',
          dataType:'JSON',
          success: function(data){
            var base_url = '<?php echo base_url();?>';
            // console.log(data[1].length);
            $("[name='namaproyek']").val(data[0][0].nama_proyek);
            $("[name='lokasiproyek']").val(data[0][0].lokasi);
            $("[name='admin[]']").data('selectize').setValue(data[1][0].id_admin);
            $('#mandor').data('selectize').setValue(data[2][0].id_mandor);
            $("[name='id_proyek']").val(data[0][0].id_proyek);
            $('#photo-preview').show();

            if(data[0][0].foto)
            {
                $('#label-photo').text('Change Photo'); // label photo upload
                $('#photo-preview div').html('<img src="'+base_url+'./assets/img/'+data[0][0].foto+'" width="100" class="img-responsive"><br>'); // show photo
                $('#photo-preview div').append('<input type="checkbox" name="hapusfoto" value="'+data[0][0].foto+'"/> Hapus foto ketika menyimpan ?'); // remove photo
              }
              else
              {
                // $('#label-photo').text('Upload Photo'); // label photo upload
                $('#photo-preview div').text('(No photo)');
              }
            }
          })
      }
    }

    function konfirmasiHapus(url){
      $('#btn-delete').attr('href', url);
      $('#deletemodal').modal();
    }
    function konfirmasiArsip(url){
      $('#btn-arsip').attr('href', url);
      $('#arsipmodal').modal();
    }
  </script>

  <script type="text/javascript">
    $(document).ready(function(){
      $('#export_pengeluaran').click(function(e){
        e.preventDefault();
        var data_type = 'data:application/vnd.ms-excel';
        var table_div = document.getElementById('tabel_pengeluaran');
        var table_html = table_div.outerHTML.replace(/ /g, '%20');

        var a = document.createElement('a');
        a.href = data_type + ', ' + table_html;
        a.download = 'Laporan POS dan Pemsukan' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
        a.click();
      })
    })
  </script>

  <script type="text/javascript">
      //all in set pos form
      var posLegal = document.getElementById('posLegal');
      var posBiayaPajak = document.getElementById('posBiayaPajak');
      var posFasumFasos = document.getElementById('posFasumFasos');
      var posOperasional = document.getElementById('posOperasional');
      var posPromosi = document.getElementById('posPromosi');
      var posBangunan = document.getElementById('posBangunan');
      //all in pengeluaran form
      var jumlah_pengeluaran = document.getElementById('jumlah_pengeluaran');
      //all in add rumah form
      var harga_rumah = document.getElementById('harga_rumah');
      //all in perjanjian form
      var jumlah_perjanjian = document.getElementById('jumlah_perjanjian');
      //all in add termin form
      var jumlah_bayar = document.getElementById('jumlah_bayar');

      if (posLegal !== null) {
        posLegal.addEventListener('keyup', function(e){
          posLegal.value = formatRupiah(this.value, 'Rp. ');
        });
      }

      if (posBiayaPajak !== null) {
        posBiayaPajak.addEventListener('keyup', function(e){
          posBiayaPajak.value = formatRupiah(this.value, 'Rp. ');
        });
      }

      if (posFasumFasos !== null) {
        posFasumFasos.addEventListener('keyup', function(e){
          posFasumFasos.value = formatRupiah(this.value, 'Rp. ');
        });
      }

      if (posOperasional !== null) {
        posOperasional.addEventListener('keyup', function(e){
          posOperasional.value = formatRupiah(this.value, 'Rp. ');
        });
      }

      if (posPromosi !== null) {
        posPromosi.addEventListener('keyup', function(e){
          posPromosi.value = formatRupiah(this.value, 'Rp. ');
        });
      }

      if (posBangunan !== null) {
        posBangunan.addEventListener('keyup', function(e){
          posBangunan.value = formatRupiah(this.value, 'Rp. ');
        });
      }

      if (jumlah_pengeluaran !== null) {
        jumlah_pengeluaran.addEventListener('keyup', function(e){
          jumlah_pengeluaran.value = formatRupiah(this.value, 'Rp. ');
        });
      }

      if (harga_rumah !== null) {
        harga_rumah.addEventListener('keyup', function(e){
          harga_rumah.value = formatRupiah(this.value, 'Rp. ');
        });
      }

      if (jumlah_perjanjian !== null) {
        jumlah_perjanjian.addEventListener('keyup', function(e){
          jumlah_perjanjian.value = formatRupiah(this.value, 'Rp. ');
        });
      }

      if (jumlah_bayar !== null) {
        jumlah_bayar.addEventListener('keyup', function(e){
          jumlah_bayar.value = formatRupiah(this.value, 'Rp. ');
        });
      }
      
    //fungsi format rupiah
    function formatRupiah(angka, prefix){
      console.log(angka);
      var number_string = angka.replace(/[^,\d]/g, '').toString(),
      split       = number_string.split(','),
      sisa        = split[0].length % 3,
      rupiah        = split[0].substr(0, sisa),
      ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
 
      if(ribuan){
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }
 
      rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
      return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
</script>