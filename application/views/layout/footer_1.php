<div style="position: absolute; bottom: 0; width: 83%; height: 60px; ">
      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Manajemen Keuangan Proyek | Copyright &copy; <?php echo date("Y") ?></span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->
</div>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>