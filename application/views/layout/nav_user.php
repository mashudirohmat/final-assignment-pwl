<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
	<!-- subheader -->
	<!--begin::Subheader-->
	<div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
		<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
			<!--begin::Info-->
			<div class="d-flex align-items-center flex-wrap mr-2">
				<div class="text-dark font-weight-bold mt-2 mb-2 mr-5">
				<h1 ><?php echo strtoupper($this->uri->segment(1)) ?></h1>
				
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url('index.php/users') ?>"><?php echo $this->uri->segment(1) ?></a></li>
					<?php if ($this->uri->segment(2) === 'mandor'): ?>
						<li class="breadcrumb-item active" aria-current="page">pimpro</li>
					<?php elseif ($this->uri->segment(2) === 'superadmin'): ?>
						<li class="breadcrumb-item active" aria-current="page">owner</li>
					<?php elseif ($this->uri->segment(2) === FALSE): ?>
						<li class="breadcrumb-item active" aria-current="page">admin</li>
					<?php endif ?>
				</ol>
				</div>
			</div>
			<!--end::Info-->
			<!--begin::Toolbar-->
			<div class="d-flex align-items-center flex-wrap">
				<!--begin::Header Menu-->
				<div id="kt_header_menu" class="header-menu header-menu-left header-menu-mobile header-menu-layout-default">
					<!--begin::Header Nav-->
					<ul class="menu-nav">
						<li class="menu-item menu-item" aria-haspopup="true">
							<a href="<?php echo base_url('index.php/users/') ?>" class="menu-link">
								<span class="menu-text">Admin</span>
							</a>
						</li>
						<li class="menu-item menu-item-submenu menu-item-rel" data-menu-toggle="click" aria-haspopup="true">
							<a href="<?php echo base_url('index.php/users/mandor') ?>" class="menu-link">
								<span class="menu-text">Pimpro</span>
								<span class="menu-desc"></span>
								<i class="menu-arrow"></i>
							</a>
						</li>
						<li class="menu-item menu-item-submenu menu-item-rel" data-menu-toggle="click" aria-haspopup="true">
							<a href="<?php echo base_url('index.php/users/superadmin') ?>" class="menu-link">
								<span class="menu-text">Owner</span>
								<span class="menu-desc"></span>
								<i class="menu-arrow"></i>
							</a>
						</li>
					</ul>
					<!--end::Header Nav-->
				</div>
				<!--end::Header Menu-->
			</div>
			<!--end::Toolbar-->
		</div>
	</div>
  <!-- end of subheader -->