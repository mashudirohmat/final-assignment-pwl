<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head><base href="">
		<meta charset="utf-8" />
		<title><?php echo $title ?></title>
		<meta name="description" content="Updates and statistics" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<link rel="canonical" href="https://keenthemes.com/metronic" />
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<link href="<?php echo base_url('assets_new/plugins/custom/fullcalendar/fullcalendar.bundle.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url('assets_new/plugins/global/plugins.bundle.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url('assets_new/plugins/custom/prismjs/prismjs.bundle.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url('assets_new/css/style.bundle.css')?>" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="<?php echo base_url('assets_new/assets/media/logos/favicon.ico')?>" />
		<!-- ===================== -->
		<link href="<?php echo base_url('startbootstrap-sb-admin-2-gh-pages/vendor/fontawesome-free/css/all.min.css') ?>" rel="stylesheet" type="text/css">
		<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
		<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap-datetimepicker.min.css')?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/selectize/selectize.bootstrap4.css')?>"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('startbootstrap-sb-admin-2-gh-pages/vendor/datatables/dataTables.bootstrap4.min.css')?>"/>
		<link rel="icon" href="<?= base_url() ?>assets/img/download.png" type="image/png">
	</head>