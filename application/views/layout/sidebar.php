    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3"><?php echo strtoupper($this->session->userdata('hak_akses')) ?></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Menu
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <?php if ($this->session->userdata('hak_akses') == 'owner') {?>
        <li class="nav-item">
          <a class="nav-link collapsed" href="<?php echo base_url('index.php/users') ?>" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-users"></i>
            <span>Users</span>
          </a>
        </li>
        <!-- <hr class="sidebar-divider"> -->
        <li class="nav-item">
          <a class="nav-link collapsed" href="<?php echo base_url('index.php/project') ?>" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-tasks"></i>
            <span>Projects</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link collapsed" href="<?php echo base_url('index.php/log_global') ?>" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-tag"></i>
            <span>Log Global</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link collapsed" href="<?php echo base_url('index.php/project/getArchiveProject') ?>" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-book"></i>
            <span>Archive</span>
          </a>
        </li>
      <?php }elseif ($this->session->userdata('hak_akses') == 'admin' or $this->session->userdata('hak_akses') == 'pimpro') {?>
        <li class="nav-item">
          <a class="nav-link collapsed" href="<?php echo base_url('index.php/project') ?>" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-tasks"></i>
            <span>Projects</span>
          </a>
        </li>
      <?php } ?>
      

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->