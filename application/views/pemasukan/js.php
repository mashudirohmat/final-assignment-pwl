<script type="text/javascript">

	function submittermin(x){
		if (x=='tambah') {
			$('#btn-tambahtermin').show();
			$('#btn-tambahtermin').text('Simpan');
			$('#btn-tambahtermin').attr('disabled', false);
			$('#btn-ubahtermin').hide();
			$('#modal-titletermin').html('<b>Form Tambah Termin</b>');

        // $("[name='tgl_termin']").val('');
        	$("[name='keterangan']").val('');
        	$("[name='jumlah_bayar']").val('');

    	}else{
    		$('#btn-tambahtermin').hide();
    		$('#btn-ubahtermin').show();
    		$('#btn-ubahtermin').attr('disabled',false);
    		$('#btn-ubahtermin').html('Ubah <i class="fa fa-paper-plane-o ml-1"></i>');
    		$('#modal-titletermin').html('<b>Form Ubah Termin</b>');

	    	$.ajax({
	    		type:"POST",
	    		data:'id='+x,
	    		url:'<?php echo base_url('index.php/pemasukan/get_termin_byid_json') ?>',
	    		dataType:'JSON',
	    		success: function(data){
	    			$("[name='tgl_termin']").val(data[0].tanggal_pemasukan);
	    			$("[name='keterangan']").val(data[0].keterangan);
	    			$("[name='jumlah_bayar']").val(data[0].jumlah);
	    			$("[name='id_pemasukan']").val(data[0].id_pemasukan);
	    		}
	    	})
	    }
	}

	function tambahtermin(){
		if ($("[name='keterangan']").val()=='') {
			$('#keterangan_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
		}else if($("[name='jumlah_bayar']").val()==''){
			$('#jumlah_bayar_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
		}else{
			$('#btn-tambahtermin').html("<i class='fa fa-spinner fa-spin'></i> Saving...");
			$('#btn-tambahtermin').attr('disabled',true);
			var formData = new FormData($('#form_add_pemasukan')[0]);

			$.ajax({
				type:'POST',
				data: formData,
				contentType: false,
				processData: false,
				url:'<?php echo base_url('index.php/pemasukan/addTermin') ?>',
				dataType : 'JSON',
				success : function(hasil){
					console.log(hasil);
					if (hasil.status=='success') {
						$("#form_add_termin").modal('hide');

						$("[name='tgl_termin']").val('');
						$("[name='keterangan']").val('');
						$("[name='jumlah_bayar']").val('');
						$("[name='id_pemasukan']").val('');

						window.location.href = hasil.redirect_url;
					}else{
						$('#termin_error_message').text(hasil.message).fadeIn().delay(3000).fadeOut();
						$('#btn-tambahtermin').html("<i></i> Simpan");
						$('#btn-tambahtermin').attr('disabled',false);
					}
				}
			});
		}
	}
	
	function ubah_termin(){
		if ($("[name='keterangan']").val()=='') {
			$('#keterangan_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
		}else if($("[name='jumlah_bayar']").val()==''){
			$('#jumlah_bayar_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
		}else{
			$('#btn-ubahtermin').html("<i class='fa fa-spinner fa-spin'></i> Updating...");
			$('#btn-ubahtermin').attr('disabled',true);
			var formData = new FormData($('#form_add_pemasukan')[0]);

			$.ajax({
				type:'POST',
				data: formData,
				contentType: false,
				processData: false,
				url:'<?php echo base_url('index.php/pemasukan/update_termin') ?>',
				dataType : 'JSON',
				success : function(hasil){
					console.log(hasil);
					if (hasil.status=='success') {
						$("#form_add_termin").modal('hide');

						$("[name='tgl_termin']").val('');
						$("[name='keterangan']").val('');
						$("[name='jumlah_bayar']").val('');
						$("[name='id_pemasukan']").val('');

						window.location.href = hasil.redirect_url;
					}else{
						$('#termin_error_message').text(hasil.message).fadeIn().delay(3000).fadeOut();
						$('#btn-ubahtermin').html("<i></i> Ubah");
						$('#btn-ubahtermin').attr('disabled',false);
					}
				}
			});
		}
	}
</script>

<!--end::Page Scripts-->
</body>
<!--end::Body-->
</html>