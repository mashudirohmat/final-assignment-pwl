<!-- FORM ADD TERMIN -->
<div class="modal fade" id="form_add_termin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title text-center" id="modal-titletermin"></h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body text-xs">
          <form actionrole="form" id="form_add_pemasukan" class="form-xs" onSubmit="return false" enctype="multipart/form-data" id="form">
            <input type="hidden" name="id_pemasukan" value="">
            <label>Tanggal</label>
            <div class='input-group date' id='datetimepicker4'>
              <input type='text' name="tgl_termin" id="tgl_termin" class="form-control" data-date-format="YYYY-MM-DD" value="<?php date_default_timezone_set('Asia/Jakarta'); echo date("y/m/d") ?>" />
              <span class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </span>
            </div>
            <div class="form-group">
              <label>Keterangan</label>
              <input type="text" name="keterangan" id="keterangan" placeholder="Keterangan ..." required="" class="form-control form-sm">
              <p id="keterangan_alert" style="color: red"></p>
            </div>
            <div class="form-group">
              <label>Jumlah</label>
              <input type="text" name="jumlah_bayar" id="jumlah_bayar" placeholder="Jumlah Bayar ..." required="" class="form-control form-sm">
              <p id="jumlah_bayar_alert" style="color: red"></p>
            </div>
        </div>
        <p id="termin_error_message" style="color:red"></p>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
          <a class="btn btn-primary" id="btn-ubahtermin" onclick="ubah_termin()" href="#!">Ubah</a>
          <a class="btn btn-primary" id="btn-tambahtermin" onclick="tambahtermin()" href="#!">Simpan</a>
        </div>
      </form>
      </div>
    </div>
  </div>
<!-- END OF FORM ADD TERMIN -->