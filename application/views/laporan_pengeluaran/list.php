<?php foreach ($this->data['project'] as $key):?>
  <?php require_once(APPPATH.'views/layout/nav_proyek.php'); ?>
<?php endforeach ?>
<!-- breadcrumb menu -->
<div class="container">
  <!-- pengeluaran -->
  <?php if ($this->data['pos'] == null) {?>
    <div class="card">
      <div class="card-header">
        <div class="text-left">
          <p>Belum ada data</p>
        </div>
      </div>
    </div><br>
  <?php }else{?>
    <div class="card">
      <div class="card-header">
        <div class="row">
          <div class="col-md-2">
            <div class="text-left">
              <p>Pengeluaran</p>
              <hr style="height:2px;border-width:0;color:blue;background-color:blue;width:50px;text-align:left;margin-left:0">
            </div>
          </div>
          <div class="col-md-2"></div>
          <div class="col-md-4">
          </div>
          <div class="col-md-1">
          </div>
          <div class="col-md-3">
            <div class="text-right">
              <a  href="<?php echo base_url('laporan_pengeluaran/export') ?>" class="btn btn-success btn-sm text-xs" id="" target="_blank"> <i class="fas fa-print fa-sm text-right"></i> &nbsp; Export Excel</a>
              <?php if ($this->session->userdata('hak_akses')== 'admin' || $this->session->userdata('hak_akses')== 'owner') {?>
                <a class="btn btn-sm btn-primary btn-sm text-xs" href="#form_add_pengeluaran" onclick="submitpengeluaran('tambah')" data-toggle="modal"><i class="fa fa-plus"></i> Pengeluaran</a>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="text-xs">
            <?php if ($this->data['pengeluaran'] == null) {?>
              <p>Data pengeluaran kosong</p>
            <?php }else{ ?>
              <table border="1" id="tabel_pengeluaran">
                <thead>
                  <tr>
                    <th colspan="13">
                      <div class="row">
                        <div class="col-md-2">
                          <!-- <img src="<?php /*echo base_url('assets/img/'.$this->session->userdata('foto'))*/ ?>" width="50"> -->
                        </div>
                        <div class="col-md-8">
                          <div class="text-center">
                            <p><?php echo strtoupper($this->session->userdata('nama_proyek')); ?><hr>
                              LAPORAN PENGELUARAN PER POS DAN PEMASUKAN <br>
                            KESELURUHAN</p>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <br>
                        </div>
                      </div>
                    </th>
                  </tr>
                  <tr>
                    <th rowspan="2" class="text-center">NO</th>
                    <th rowspan="2" class="text-center">TANGGAL</th>
                    <th rowspan="2" class="text-center">NO NOTA</th>
                    <th rowspan="2" class="text-center">JENIS PENGELUARAN</th>
                    <th colspan="6" class="text-center">POS</th>
                    <th rowspan="2" class="text-center">JUMLAH NOTA</th>
                    <th rowspan="2" class="text-center">PEMASUKAN</th>
                    <th rowspan="2" class="text-center">AKSI</th>
                  </tr>
                  <tr>
                    <th class="text-center">POS 1 (LEGAL)</th>
                    <th class="text-center">POS 2 (FASUMFASOS)</th>
                    <th class="text-center">POS 3 (OPERASIONAL)</th>
                    <th class="text-center">POS 4 (PROMOSI)</th>
                    <th class="text-center">POS 5 (BANGUNAN)</th>
                    <th class="text-center">POS 6 (PAJAK)</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 1;
                  foreach ($this->data['pengeluaran'] as $key): ?>
                    <tr>
                      <td class="text-center"><?php echo $no ?></td>
                      <td class="text-center"><?php echo $key['tgl_transaksi'] ?></td>
                      <td class="text-center"><?php echo $key['no_nota'] ?></td>
                      <td class="text-center"><?php echo $key['jenis_pengeluaran'] ?></td>
                      <td class="text-center"><?php echo 'Rp. '.number_format($key['pos_legal'],0,',','.') ?></td>
                      <td class="text-center"><?php echo 'Rp. '.number_format($key['pos_fasumfasos'],0,',','.') ?></td>
                      <td class="text-center"><?php echo 'Rp. '.number_format($key['pos_operasional'],0,',','.') ?></td>
                      <td class="text-center"><?php echo 'Rp. '.number_format($key['pos_promosi'],0,',','.') ?></td>
                      <td class="text-center"><?php echo 'Rp. '.number_format($key['pos_bangunan'],0,',','.') ?></td>
                      <td class="text-center"><?php echo 'Rp. '.number_format($key['pos_pajak'],0,',','.') ?></td>
                      <td class="text-center"><?php       $jumlah_nota = ($key['pos_legal']
                        +$key['pos_fasumfasos']
                        +$key['pos_operasional']
                        +$key['pos_promosi']
                        +$key['pos_bangunan']
                        +$key['pos_pajak']);
                      echo 'Rp. '.number_format($jumlah_nota,0,',','.');
                      ?>
                    </td>
                    <td class="text-center"></td>
                    <td class="text-center">
                      <?php if ($this->session->userdata('hak_akses') == 'pimpro'):?>
                        <p>no action</p>
                        <?php else: ?>
                          <a class="text-warning" href="#form_add_pengeluaran" onclick="submitpengeluaran(<?php echo $key['id_pengeluaran'] ?>)" data-toggle="modal">Edit
                          </a>
                          <a onClick="konfirmasiArsip('<?php echo site_url('index.php/laporan_pengeluaran/archivePengeluaran/'.$key['id_pengeluaran']) ?>')" href="#!" class="text-danger">Archive
                          </a>
                        <?php endif ?>
                      </td>
                    </tr>
                    <?php $no++; endforeach ?>
                    <?php 
                    for ($i=0; $i < count($this->data['pemasukan']) ; $i++) { 
                      $jumlah_pemasukan_bersih[] = $this->data['pemasukan'][$i]['jumlah'];
                    }
                    $nomer = $no;
                    foreach ($this->data['pemasukan'] as $key):
                      ?>
                      <tr>
                        <td class="text-center"><?php echo $nomer ?></td>
                        <td class="text-center"><?php echo $key['tanggal_pemasukan'] ?></td>
                        <td class="text-center"></td>
                        <td class="text-center"><?php echo $key['keterangan'] ?></td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-center"><?php echo 'Rp. '.number_format($key['jumlah'],0,',','.') ?></td>
                        <td class="text-center"></td>
                      </tr>
                      <?php $nomer++; endforeach ?>
                      <tr>
                        <?php 
                        for ($i=0; $i < count($this->data['pengeluaran']) ; $i++) { 
                          $jumlahnota[] = $jumlah_nota;
                          $jumlahpos1[] = $this->data['pengeluaran'][$i]['pos_legal'];
                          $jumlahpos2[] = $this->data['pengeluaran'][$i]['pos_fasumfasos'];
                          $jumlahpos3[] = $this->data['pengeluaran'][$i]['pos_operasional'];
                          $jumlahpos4[] = $this->data['pengeluaran'][$i]['pos_promosi'];
                          $jumlahpos5[] = $this->data['pengeluaran'][$i]['pos_bangunan'];
                          $jumlahpos6[] = $this->data['pengeluaran'][$i]['pos_pajak'];
                        }
                        ?>
                        <td class="text-center" colspan="4"><b>Jumlah</b></td>
                        <td class="text-center"><b><?php echo 'Rp. '.number_format(array_sum($jumlahpos1),0,',','.') ?></b></td>
                        <td class="text-center"><b><?php echo 'Rp. '.number_format(array_sum($jumlahpos2),0,',','.') ?></b></td>
                        <td class="text-center"><b><?php echo 'Rp. '.number_format(array_sum($jumlahpos3),0,',','.') ?></b></td>
                        <td class="text-center"><b><?php echo 'Rp. '.number_format(array_sum($jumlahpos4),0,',','.') ?></b></td>
                        <td class="text-center"><b><?php echo 'Rp. '.number_format(array_sum($jumlahpos5),0,',','.') ?></b></td>
                        <td class="text-center"><b><?php echo 'Rp. '.number_format(array_sum($jumlahpos6),0,',','.') ?></b></td>
                        <td class="text-center"><b><?php $total =
                        (array_sum($jumlahpos1)+
                          array_sum($jumlahpos2)+
                          array_sum($jumlahpos3)+
                          array_sum($jumlahpos4)+
                          array_sum($jumlahpos5)+
                          array_sum($jumlahpos6));
                          echo 'Rp. '.number_format($total,0,',','.') ?></b></td>
                          <td class="text-center"><b><?php echo 'Rp. '.@number_format(array_sum($jumlah_pemasukan_bersih),0,',','.');?></b></td>
                          <td class="text-center"></td>
                        </tr>
                      </tbody>
                      <tfoot class="text-center">
                        <?php @$rekapitulasi = array_sum(@$jumlah_pemasukan_bersih) - $total; ?>
                        <tr>
                          <td colspan="4"><b>Sisa budget</b></td>
                          <td><b><?php echo 'Rp. '.number_format($this->data['sisa_pos']['legal'],0,',','.'); ?></b></td>
                          <td><b><?php echo 'Rp. '.number_format($this->data['sisa_pos']['fasumfasos'],0,',','.'); ?></b></td>
                          <td><b><?php echo 'Rp. '.number_format($this->data['sisa_pos']['operasional'],0,',','.'); ?></b></td>
                          <td><b><?php echo 'Rp. '.number_format($this->data['sisa_pos']['promosi'],0,',','.'); ?></b></td>
                          <td><b><?php echo 'Rp. '.number_format($this->data['sisa_pos']['bangunan'],0,',','.'); ?></b></td>
                          <td><b><?php echo 'Rp. '.number_format($this->data['sisa_pos']['pajak'],0,',','.'); ?></b></td>
                          <td colspan="3"></td>
                        </tr>
                      </tfoot>
                    </table>
                  <?php } ?>
                </div>
              </div>
              <br><br>
              <div class="row">
                <div class="col-md-5">
                  <div <?php if (@$rekapitulasi < 0) {?>
                    class="alert alert-warning"
                  <?php }else{?>
                    class="alert alert-primary"
                    <?php } ?> role="alert">
                    <b>Rekapitulasi: <?php echo 'Rp. '.number_format(@$rekapitulasi,0,',','.'); ?></b>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <br>
          <!-- end of pengeluaran -->



        <?php } ?>
        <br><br>

      </div>