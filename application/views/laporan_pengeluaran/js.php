<script type="text/javascript">
	function submitpengeluaran(x){
      if (x=='tambah') {
        $('#btn-tambahpengeluaran').show();
        $('#btn-tambahpengeluaran').attr('disabled',false);
        $('#btn-tambahpengeluaran').text('Tambah');
        $('#btn-ubahpengeluaran').hide();
        $('#pengeluaran_error_message').text('');
        $('#modal-titlepengeluaran').html('<b>Form Tambah Pengeluaran</b>');

        $("[name='desc_pengeluaran']").val('');
        $("[name='jumlah_pengeluaran']").val('');
        $("[name='id_pengeluaran']").val('');

      }else{
        $('#btn-tambahpengeluaran').hide();
        $('#btn-ubahpengeluaran').show();
        $('#btn-ubahpengeluaran').attr('disabled',false);
        $('#pengeluaran_error_message').text('');
        $('#btn-ubahpengeluaran').html('Ubah <i class="fa fa-paper-plane-o ml-1"></i>');
        $('#modal-titlepengeluaran').html('<b>Form Ubah Pengeluaran</b>');

        $.ajax({
          type:"POST",
          data:'id='+x,
          url:'<?php echo base_url('index.php/laporan_pengeluaran/getPengeluaranById') ?>',
          dataType:'JSON',
          success: function(data){
            $("[name='tgl_transaksi']").val(data.tgl_transaksi);
            $("[name='desc_pengeluaran']").val(data.desc_pengeluaran);
            $("[name='jumlah_pengeluaran']").val(data.jumlah);
            $("[name='pilih_pos']").val(data.pilih_pos);
            $("[name='id_pengeluaran']").val(data.id_pengeluaran);

            $("[name='jumlah_pengeluaran_lama']").val(data.jumlah);
            $("[name='pos_lama']").val(data.pilih_pos);
          }
        })
      }
    }

  function tambahpengeluaran(){
      if ($("[name='desc_pengeluaran']").val()=='') {
        $('#desc_pengeluaran_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='pilih_pos']").val()=='placeholder'){
        $('#pos_pengeluaran_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='jumlah_pengeluaran']").val()==''){
        $('#jumlah_pengeluaran_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else{
        $('#btn-tambahpengeluaran').html("<i class='fa fa-spinner fa-spin'></i> Saving...");
        $('#btn-tambahpengeluaran').attr('disabled',true);
        var formData = new FormData($('#form_pengeluaran')[0]);

        $.ajax({
          type:'POST',
          data: formData,
          contentType: false,
          processData: false,
          url:'<?php echo base_url('index.php/laporan_pengeluaran/addPengeluaran') ?>',
          dataType : 'JSON',
          success : function(hasil){
            console.log(hasil);
            if (hasil.status=='success') {
              $("#form_add_pengeluaran").modal('hide');

              $("[name='desc_pengeluaran']").val('');
              $("[name='jumlah_pengeluaran']").val('');
              $("[name='id_pengeluaran']").val('');

              window.location.href = hasil.redirect_url;
            }else{
              $('#pengeluaran_error_message').text(hasil.message).fadeIn().delay(3000).fadeOut();
              $('#btn-tambahpengeluaran').html("<i></i> Simpan");
              $('#btn-tambahpengeluaran').attr('disabled',false);
            }
          }
        });
      }
    }

  function ubahpengeluaran(){
      if ($("[name='desc_pengeluaran']").val()=='') {
        $('#desc_pengeluaran_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='pilih_pos']").val()=='placeholder'){
        $('#pos_pengeluaran_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='jumlah_pengeluaran']").val()==''){
        $('#jumlah_pengeluaran_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else{
        $('#btn-ubahpengeluaran').html("<i class='fa fa-spinner fa-spin'></i> Updating...");
        $('#btn-ubahpengeluaran').attr('disabled',true);
        var formData = new FormData($('#form_pengeluaran')[0]);

        $.ajax({
          type:'POST',
          data: formData,
          contentType: false,
          processData: false,
          url:'<?php echo base_url('index.php/laporan_pengeluaran/updatePengeluaran') ?>',
          dataType : 'JSON',
          success : function(hasil){
            console.log(hasil.error_string);
            if (hasil.status=='success') {
              $("#form_add_pengeluaran").modal('hide');

              $("[name='desc_pengeluaran']").val('');
              $("[name='jumlah_pengeluaran']").val('');
              $("[name='id_pengeluaran']").val('');
              $("[name='id_pengeluaran']").val('');

              window.location.href = hasil.redirect_url;
            }else{
              $('#pengeluaran_error_message').text(hasil.message).fadeIn().delay(3000).fadeOut();
              $('#btn-ubahpengeluaran').html("<i></i> Ubah");
              $('#btn-ubahpengeluaran').attr('disabled',false);
            }
          }
        });
      }
    }
</script>

<!--end::Page Scripts-->
</body>
<!--end::Body-->
</html>