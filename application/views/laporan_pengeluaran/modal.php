<!-- MODAL ADD PENGELUARAN -->
<div class="modal fade" id="form_add_pengeluaran" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="modal-titlepengeluaran"></h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body text-xs">
        <form actionrole="form" id="form_pengeluaran" class="form-xs" onSubmit="return false" enctype="multipart/form-data">
          <input type="hidden" name="id_pengeluaran" value="">
          <label>TANGGAL</label>
          <div class='input-group date' id='datetimepicker3'>
            <input type='text' name="tgl_transaksi" id="tgl_transaksi" class="form-control" data-date-format="YYYY-MM-DD" value="<?php date_default_timezone_set('Asia/Jakarta'); echo date("y/m/d") ?>" />
            <span class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </span>
          </div>
          <div class="form-group">
            <label>KETERANGAN</label>
            <textarea name="desc_pengeluaran" id="desc_pengeluaran" placeholder="Keterangan ..." required="" class="form-control"></textarea>
            <p id="desc_pengeluaran_alert" style="color: red"></p>
          </div>
          <div class="form-group">
            <label>PILIH POS</label>
            <select name="pilih_pos" class="form-control" id="pilih_pos" required="">
              <option selected="" value="placeholder">Pilih pos ...</option>
              <option value="pos_legal">Pos Legal</option>
              <option value="pos_fasumfasos">Pos Fasum dan Fasos</option>
              <option value="pos_operasional">Pos Operasional</option>
              <option value="pos_promosi">Pos Promosi</option>
              <option value="pos_bangunan">Pos Bangunan</option>
              <option value="pos_pajak">Pos Pajak</option>
            </select>
            <p id="pos_pengeluaran_alert" style="color: red"></p>
          </div>
          <div class="form-group">
            <label>JUMLAH</label>
            <input type="text" name="jumlah_pengeluaran" class="form-control" placeholder="Jumlah ..." id="jumlah_pengeluaran" required="">
            <p id="jumlah_pengeluaran_alert" style="color: red"></p>
          </div>
          <input type="hidden" name="jumlah_pengeluaran_lama">
          <input type="hidden" name="pos_lama">
        </div>
        <p id="pengeluaran_error_message" style="color:red"></p>
        
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
          <a class="btn btn-primary" id="btn-ubahpengeluaran" onclick="ubahpengeluaran()" href="#!">Ubah</a>
          <a class="btn btn-primary" id="btn-tambahpengeluaran" onclick="tambahpengeluaran()" href="#!">Simpan</a>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- END OF MODAL ADD PENGELUARAN -->