<?php 

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=$title.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<table border="1" id="tabel_pengeluaran">
              <thead>
                <tr>
                  <th colspan="12">
                    <div class="row">
                      <div class="col-md-2">
                        <img src="<?php echo base_url('assets/img/'.$this->session->userdata('foto')) ?>" width="50">
                      </div>
                      <div class="col-md-8">
                        <div class="text-center">
                          <p><?php echo strtoupper($this->session->userdata('nama_proyek')); ?><hr>
                            LAPORAN PENGELUARAN PER POS DAN PEMASUKAN <br>
                            KESELURUHAN</p>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <br>
                        </div>
                      </div>
                    </th>
                  </tr>
                  <tr>
                    <th rowspan="2" class="text-center">NO</th>
                    <th rowspan="2" class="text-center">TANGGAL</th>
                    <th rowspan="2" class="text-center">NO NOTA</th>
                    <th rowspan="2" class="text-center">JENIS PENGELUARAN</th>
                    <th colspan="6" class="text-center">POS</th>
                    <th rowspan="2" class="text-center">JUMLAH NOTA</th>
                    <th rowspan="2" class="text-center">PEMASUKAN</th>
                  </tr>
                  <tr>
                    <th class="text-center">POS 1 (LEGAL)</th>
                    <th class="text-center">POS 2 (FASUMFASOS)</th>
                    <th class="text-center">POS 3 (OPERASIONAL)</th>
                    <th class="text-center">POS 4 (PROMOSI)</th>
                    <th class="text-center">POS 5 (BANGUNAN)</th>
                    <th class="text-center">POS 6 (PAJAK)</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 1;
                  foreach ($this->data['pengeluaran'] as $key): ?>
                  <tr>
                    <td class="text-center"><?php echo $no ?></td>
                    <td class="text-center"><?php echo $key['tgl_transaksi'] ?></td>
                    <td class="text-center"><?php echo $key['no_nota'] ?></td>
                    <td class="text-center"><?php echo $key['jenis_pengeluaran'] ?></td>
                    <td class="text-center"><?php echo 'Rp. '.number_format($key['pos_legal'],0,',','.') ?></td>
                    <td class="text-center"><?php echo 'Rp. '.number_format($key['pos_fasumfasos'],0,',','.') ?></td>
                    <td class="text-center"><?php echo 'Rp. '.number_format($key['pos_operasional'],0,',','.') ?></td>
                    <td class="text-center"><?php echo 'Rp. '.number_format($key['pos_promosi'],0,',','.') ?></td>
                    <td class="text-center"><?php echo 'Rp. '.number_format($key['pos_bangunan'],0,',','.') ?></td>
                    <td class="text-center"><?php echo 'Rp. '.number_format($key['pos_pajak'],0,',','.') ?></td>
                    <td class="text-center"><?php       $jumlah_nota = ($key['pos_legal']
                      +$key['pos_fasumfasos']
                      +$key['pos_operasional']
                      +$key['pos_promosi']
                      +$key['pos_bangunan']
                      +$key['pos_pajak']);
                    echo 'Rp. '.number_format($jumlah_nota,0,',','.');
                    ?>
                  </td>
                </tr>
                <?php $no++; endforeach ?>
                <?php 
                for ($i=0; $i < count($this->data['pemasukan']) ; $i++) { 
                  $jumlah_pemasukan_bersih[] = $this->data['pemasukan'][$i]['jumlah'];
                }
                $nomer = $no;
                foreach ($this->data['pemasukan'] as $key):
                  ?>
                  <tr>
                    <td class="text-center"><?php echo $nomer ?></td>
                    <td class="text-center"><?php echo $key['tanggal_pemasukan'] ?></td>
                    <td class="text-center"></td>
                    <td class="text-center"><?php echo $key['keterangan'] ?></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"><?php echo 'Rp. '.number_format($key['jumlah'],0,',','.') ?></td>
                  </tr>
                <?php $nomer++; endforeach ?>
                <tr>
                  <?php 
                  for ($i=0; $i < count($this->data['pengeluaran']) ; $i++) { 
                    $jumlahnota[] = $jumlah_nota;
                    $jumlahpos1[] = $this->data['pengeluaran'][$i]['pos_legal'];
                    $jumlahpos2[] = $this->data['pengeluaran'][$i]['pos_fasumfasos'];
                    $jumlahpos3[] = $this->data['pengeluaran'][$i]['pos_operasional'];
                    $jumlahpos4[] = $this->data['pengeluaran'][$i]['pos_promosi'];
                    $jumlahpos5[] = $this->data['pengeluaran'][$i]['pos_bangunan'];
                    $jumlahpos6[] = $this->data['pengeluaran'][$i]['pos_pajak'];
                  }
                  ?>
                  <td class="text-center" colspan="4"><b>Jumlah</b></td>
                  <td class="text-center"><b><?php echo 'Rp. '.number_format(array_sum($jumlahpos1),0,',','.') ?></b></td>
                  <td class="text-center"><b><?php echo 'Rp. '.number_format(array_sum($jumlahpos2),0,',','.') ?></b></td>
                  <td class="text-center"><b><?php echo 'Rp. '.number_format(array_sum($jumlahpos3),0,',','.') ?></b></td>
                  <td class="text-center"><b><?php echo 'Rp. '.number_format(array_sum($jumlahpos4),0,',','.') ?></b></td>
                  <td class="text-center"><b><?php echo 'Rp. '.number_format(array_sum($jumlahpos5),0,',','.') ?></b></td>
                  <td class="text-center"><b><?php echo 'Rp. '.number_format(array_sum($jumlahpos6),0,',','.') ?></b></td>
                  <td class="text-center"><b><?php $total =
                  (array_sum($jumlahpos1)+
                    array_sum($jumlahpos2)+
                    array_sum($jumlahpos3)+
                    array_sum($jumlahpos4)+
                    array_sum($jumlahpos5)+
                    array_sum($jumlahpos6));
                    echo 'Rp. '.number_format($total,0,',','.') ?></b></td>
                    <td class="text-center"><b><?php echo 'Rp. '.@number_format(array_sum($jumlah_pemasukan_bersih),0,',','.');?></b></td>
                  </tr>
                </tbody>
                <tfoot class="text-center">
                    <?php @$rekapitulasi = array_sum($jumlah_pemasukan_bersih) - $total; ?>
                    <tr>
                      <td colspan="4"><b>Sisa budget</b></td>
                      <td><b><?php echo 'Rp. '.number_format($this->data['sisa_pos']['legal'],0,',','.'); ?></b></td>
                      <td><b><?php echo 'Rp. '.number_format($this->data['sisa_pos']['fasumfasos'],0,',','.'); ?></b></td>
                      <td><b><?php echo 'Rp. '.number_format($this->data['sisa_pos']['operasional'],0,',','.'); ?></b></td>
                      <td><b><?php echo 'Rp. '.number_format($this->data['sisa_pos']['promosi'],0,',','.'); ?></b></td>
                      <td><b><?php echo 'Rp. '.number_format($this->data['sisa_pos']['bangunan'],0,',','.'); ?></b></td>
                      <td><b><?php echo 'Rp. '.number_format($this->data['sisa_pos']['pajak'],0,',','.'); ?></b></td>
                      <td colspan="2"></td>
                    </tr>
                </tfoot>
              </table>
              <br><br>
          <div class="row">
            <div class="col-md-5">
            <div <?php if (@$rekapitulasi < 0) {?>
                        class="alert alert-warning"
                      <?php }else{?>
                        class="alert alert-primary"
                      <?php } ?> role="alert">
              <b>Rekapitulasi: <?php echo 'Rp. '.number_format(@$rekapitulasi,0,',','.'); ?></b>
            </div>
            </div>
          </div>

<table border="0" width="50%">
  <thead>
    <tr>
      <th></th>
      <th rowspan="6" colspan="11"><br>Yogyakarta, <?php echo date("l jS \of F Y") ?><br> Dibuat Oleh <br><br><br> <?php echo strtoupper($this->session->userdata('username')); ?> </th>
    </tr>
  </thead>
  <tbody></tbody>
  <tr></tr>
  <tr></tr>
</table>