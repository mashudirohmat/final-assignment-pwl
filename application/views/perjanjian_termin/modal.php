<!-- FORM ADD PERJANJIAN -->
<div class="modal fade" id="modal_add_perjanjian" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title text-center" id="modal-titleperjanjian"></h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body text-xs">
          <form actionrole="form" id="form_add_perjanjian" class="form-xs" onSubmit="return false" enctype="multipart/form-data">
            <input type="hidden" name="id_perjanjian" value="">
            <label>Tanggal Jatuh Tempo</label>
            <div class='input-group date' id='datetimepicker5'>
              <input type='text' name="tgl_jatuh_tempo" id="tgl_jatuh_tempo" class="form-control" data-date-format="YYYY-MM-DD" value="<?php date_default_timezone_set('Asia/Jakarta'); echo date("y/m/d") ?>" />
              <span class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </span>
            </div>
            <div class="form-group">
              <label>Keterangan</label>
              <input type="text" name="keterangan_perjanjian" id="keterangan_perjanjian" placeholder="Keterangan ..." required="" class="form-control form-sm">
              <p id="keterangan_perjanjian_alert" style="color: red"></p>
            </div>
            <div class="form-group">
              <label>Jumlah</label>
              <input type="text" name="jumlah_perjanjian" id="jumlah_perjanjian" placeholder="Jumlah Bayar ..." required="" class="form-control form-sm">
              <p id="jumlah_perjanjian_alert" style="color: red"></p>
            </div>
        </div>
        <p id="termin_perjanjian_error_message" style="color:red"></p>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
          <a class="btn btn-primary" id="btn-ubahperjanjian" onclick="ubah_perjanjian()" href="#!">Ubah</a>
          <a class="btn btn-primary" id="btn-tambahperjanjian" onclick="tambahperjanjian()" href="#!">Simpan</a>
        </div>
      </form>
      </div>
    </div>
  </div>
<!-- END OF FORM ADD PERJANJIAN -->