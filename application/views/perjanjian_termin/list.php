<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
        <h2>Perjanjian</h2>
        <h5 class="text-right">
          <select onchange="location = this.value;">
            <option value="<?php echo base_url('index.php/perjanjian?id='.$this->session->userdata('id_transaksi')) ?>">Perjanjian</option>
            <option value="<?php echo base_url('index.php/pemasukan/termin/'.$this->session->userdata('id_transaksi')) ?>">Termin</option>
          </select>
        </h5>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <!-- budget -->
  <div class="card">
    <div class="card-header">
      <div class="row">
        <div class="col-md-3">
          <h5>Informsi Pembeli</h5>
          <hr style="height:2px;border-width:0;color:blue;background-color:blue;width:50px;text-align:left;margin-left:0">
          <table>
            <tr>
              <td><small>Nama</small></td>
              <td>:</td>
              <td><small><?php echo $this->session->userdata('nama_pembeli') ?></small></td>
            </tr>
            <tr>
              <td><small>Alamat</small></td>
              <td>:</td>
              <td><small><?php echo $this->session->userdata('alamat_pembeli') ?></small></td>
            </tr>
            <tr>
              <td><small>No.KTP</small></td>
              <td>:</td>
              <td><small><?php echo $this->session->userdata('no_ktp') ?></small></td>
            </tr>
            <tr>
              <td><small>No.KK</small></td>
              <td>:</td>
              <td><small><?php echo $this->session->userdata('no_kk') ?></small></td>
            </tr>
            <tr>
              <td><small>No.HP</small></td>
              <td>:</td>
              <td><small><?php echo $this->session->userdata('no_telp') ?></small></td>
            </tr>
          </table>
        </div>
        <div class="col-md-5">
          <h5>Informasi Rumah</h5>
          <hr style="height:2px;border-width:0;color:blue;background-color:blue;width:50px;text-align:left;margin-left:0">
          <table>
            <tr>
              <td><small>Nama Rumah</small></td>
              <td>:</td>
              <td><small><?php echo $this->session->userdata('nama_rumah') ?></small></td>
            </tr>
            <tr>
              <td><small>Tipe</small></td>
              <td>:</td>
              <td><small><?php echo $this->session->userdata('tipe_rumah') ?></small></td>
            </tr>
            <tr>
              <td><small>Harga</small></td>
              <td>:</td>
              <td><small><?php echo 'Rp. '.number_format($this->session->userdata('harga_rumah'),2,',','.') ?></small></td>
            </tr>
          </table>
        </div>
        <div class="col-md-4">
          <div class="text-right">
            <a href="<?php echo base_url('index.php/project/detailProject/'.$this->session->userdata('id_proyek')) ?>">
              <i class="fa fa-arrow-left"> Kembali</i>
            </a>
            &nbsp;&nbsp;&nbsp;
            <a class="btn btn-sm btn-primary text-xs" href="#modal_add_perjanjian" onclick="submitperjanjian('tambah')" data-toggle="modal">Tambah Perjanjian</a>
          </div>
          <br>
          </div>
          </div>
        </div>
    <div class="card-body text-xs">
      <div class="row">
        <div class="table-responsive">
                <table class="table table-bordered" id="tabel1">
                  <thead>
                    <tr class="text-center">
                      <th>No.</th>
                      <th>Tanggal</th>
                      <th>Keterangan</th>
                      <th>Jumlah</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $i=1; 
                    foreach ($this->data['termin'] as $key): ?>
                    <tr>
                      <td><?php echo $i ?></td>
                      <td><?php echo $key['tgl_jatuh_tempo'] ?></td>
                      <td><?php echo $key['keterangan'] ?></td>
                      <td><?php echo 'Rp. '.number_format($key['jumlah'],2,',','.') ?></td>
                      <td class="text-center">
                        <a class="btn btn-warning text-xs btn-sm" href="#modal_add_perjanjian" onclick="submitperjanjian(<?php echo $key['id_perjanjian'] ?>)" data-toggle="modal">Edit
                        </a>
                        <a onClick="konfirmasiArsip('<?php echo site_url('index.php/perjanjian/archive_pemasukan/'.$key['id_perjanjian']) ?>')" href="#!" class="btn btn-danger text-xs btn-sm">Archive
                        </a>
                      </td>
                    </tr>
                    <?php $i++; endforeach ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <?php
                      $jumlah = 0;
                      for ($i=0; $i < count($this->data['termin']); $i++) { 
                        $jumlah += $this->data['termin'][$i]['jumlah'];
                      } ?>
                      <td colspan="3" class="text-center"><b>Jumlah</b></td>
                      <td><b><?php echo 'Rp. '.number_format($jumlah,2,',','.') ?></b></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
      </div>
    </div>
  </div>
</div>

<!-- 


<td>
                        <a class="btn text-sm text-warning" href="#form_add_termin" onclick="submittermin(<?php /*echo $key['id_pemasukan'] ?>)" data-toggle="modal">
                          <i class="fas fa-edit"></i>Edit
                        </a>
                        <a onClick="konfirmasiHapus('<?php echo site_url('index.php/project/deleteTermin/'.$key['id_pemasukan'])*/ ?>')" href="#!" class="btn text-sm text-danger">
                          <i class="fas fa-trash"></i>Hapus
                        </a>
                      </td>
 -->