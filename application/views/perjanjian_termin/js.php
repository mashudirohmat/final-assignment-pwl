<script type="text/javascript">

	function submitperjanjian(x){
		if (x=='tambah') {
			$('#btn-tambahperjanjian').show();
			$('#btn-tambahperjanjian').text('Simpan');
			$('#btn-tambahperjanjian').attr('disabled', false);
			$('#btn-ubahperjanjian').hide();
			$('#modal-titleperjanjian').html('<b>Form Tambah Kontrak Termin</b>');

        // $("[name='tgl_termin']").val('');
        	$("[name='keterangan_perjanjian']").val('');
        	$("[name='jumlah_perjanjian']").val('');

    	}else{
    		$('#btn-tambahperjanjian').hide();
    		$('#btn-ubahperjanjian').show();
    		$('#btn-ubahperjanjian').attr('disabled',false);
    		$('#btn-ubahperjanjian').html('Ubah <i class="fa fa-paper-plane-o ml-1"></i>');
    		$('#modal-titleperjanjian').html('<b>Form Ubah Kontrak Termin</b>');

	    	$.ajax({
	    		type:"POST",
	    		data:'id='+x,
	    		url:'<?php echo base_url('index.php/perjanjian/get_termin_byid_json') ?>',
	    		dataType:'JSON',
	    		success: function(data){
	    			console.log(data[0].tgl_jatuh_tempo);
	    			$("[name='tgl_jatuh_tempo']").val(data[0].tgl_jatuh_tempo);
	    			$("[name='keterangan_perjanjian']").val(data[0].keterangan);
	    			$("[name='jumlah_perjanjian']").val(data[0].jumlah);
	    			$("[name='id_perjanjian']").val(data[0].id_perjanjian);
	    		}
	    	})
	    }
	}

	function tambahperjanjian(){
		if ($("[name='keterangan_perjanjian']").val()=='') {
			$('#keterangan_perjanjian_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
		}else if($("[name='jumlah_perjanjian']").val()==''){
			$('#jumlah_perjanjian_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
		}else{
			$('#btn-tambahperjanjian').html("<i class='fa fa-spinner fa-spin'></i> Saving...");
			$('#btn-tambahperjanjian').attr('disabled',true);
			var formData = new FormData($('#form_add_perjanjian')[0]);

			$.ajax({
				type:'POST',
				data: formData,
				contentType: false,
				processData: false,
				url:'<?php echo base_url('index.php/perjanjian/addTermin') ?>',
				dataType : 'JSON',
				success : function(hasil){
					console.log(hasil);
					if (hasil.status=='success') {
						$("#form_add_perjanjian").modal('hide');

						$("[name='tgl_jatuh_tempo']").val('');
						$("[name='keterangan_perjanjian']").val('');
						$("[name='jumlah_perjanjian']").val('');
						$("[name='id_perjanjian']").val('');

						window.location.href = hasil.redirect_url;
					}else{
						$('#termin_perjanjian_error_message').text(hasil.message).fadeIn().delay(3000).fadeOut();
						$('#btn-tambahperjanjian').html("<i></i> Simpan");
						$('#btn-tambahperjanjian').attr('disabled',false);
					}
				}
			});
		}
	}
	
	function ubah_perjanjian(){
		if ($("[name='keterangan_perjanjian']").val()=='') {
			$('#keterangan_perjanjian_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
		}else if($("[name='jumlah_perjanjian']").val()==''){
			$('#jumlah_perjanjian_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
		}else{
			$('#btn-ubahperjanjian').html("<i class='fa fa-spinner fa-spin'></i> Updating...");
			$('#btn-ubahperjanjian').attr('disabled',true);
			var formData = new FormData($('#form_add_perjanjian')[0]);

			$.ajax({
				type:'POST',
				data: formData,
				contentType: false,
				processData: false,
				url:'<?php echo base_url('index.php/perjanjian/update_termin') ?>',
				dataType : 'JSON',
				success : function(hasil){
					console.log(hasil);
					if (hasil.status=='success') {
						$("#form_add_perjanjian").modal('hide');

						$("[name='tgl_jatuh_tempo']").val('');
						$("[name='keterangan_perjanjian']").val('');
						$("[name='jumlah_perjanjian']").val('');
						$("[name='id_perjanjian']").val('');

						window.location.href = hasil.redirect_url;
					}else{
						$('#termin_perjanjian_error_message').text(hasil.message).fadeIn().delay(3000).fadeOut();
						$('#btn-ubahperjanjian').html("<i></i> Ubah");
						$('#btn-ubahperjanjian').attr('disabled',false);
					}
				}
			});
		}
	}
</script>

<!--end::Page Scripts-->
</body>
<!--end::Body-->
</html>