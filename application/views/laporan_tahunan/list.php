<?php foreach ($this->data['project'] as $key):?>
  <?php require_once(APPPATH.'views/layout/nav_proyek.php'); ?>
<?php endforeach ?>
<!-- breadcrumb menu -->
<div class="container">
  <!-- laporan tahunan -->
  <?php if ($this->data['pos'] == null) {?>
    <div class="card">
      <div class="card-header">
        <div class="text-left">
          <p>Belum ada data</p>
        </div>
      </div>
    </div><br>
  <?php }else{?>
    <div class="card">
      <div class="card-header">
        <div class="row">
          <div class="col-md-2">
            <div class="text-left">
              <p>Laporan Tahunan</p>
              <hr style="height:2px;border-width:0;color:blue;background-color:blue;width:50px;text-align:left;margin-left:0">
            </div>
          </div>
          <div class="col-md-3">
          </div>
          <div class="col-md-4">
            <div class="text-right">
              <form class="form-inline" method="post" action="<?php echo base_url('laporan_tahunan/filter') ?>">
                <div class="form-group mx-sm-3 mb-2">
                  <div class='input-group date' id='datetimepicker6'>
                    <input type='text' name="filter_date" id="filter_date" class="form-control form-control-sm" data-date-format="YYYY" value="<?php date_default_timezone_set('Asia/Jakarta'); echo date("y") ?>" />
                    <div class="input-group-addon">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="fa fa-calendar" aria-hidden="true"></i>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-primary mb-2 form-control form-control-sm">
                    <i class="fa fa-search"></i>
                  </button>
                </div>
              </form>
            </div>
          </div>
          <div class="col-md-1">
            <a href="<?php echo base_url('laporan_tahunan') ?>" class="btn btn-info btn-sm">
              <i class="fa fa-retweet"></i>
            </a>
          </div>
          <div class="col-md-2" >

            <div class="text-right">
              <a  href="<?php echo base_url('laporan_tahunan/export') ?>" class="btn btn-sm btn-success" id="export_tahunan" target="_blank"> <i class="fas fa-print fa-sm text-right"></i> &nbsp; Export Excel</a>
            </div>
          </div>

        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <div class="text-xs">
              <?php if ($this->data['laporan_tahunan'] == null) {?>
                <p>Data laporan tahunan kosong</p>
              <?php }else{ ?>
                <table border="1" id="tabel_pengeluaran">
                  <thead>
                    <tr>
                      <th colspan="10">
                        <div class="row">
                          <div class="col-md-2">
                          </div>
                          <div class="col-md-8">
                            <div class="text-center">
                              <p><?php echo strtoupper($this->session->userdata('nama_proyek')); ?><hr>
                                REKAP LAPORAN TAHUNAN PER POS<br>
                                TAHUN <?php echo $this->session->userdata('filter_date') ?></p>
                              </div>
                            </div>
                            <div class="col-md-2">
                              <br>
                            </div>
                          </div>
                        </th>
                      </tr>
                      <tr>
                        <th rowspan="2" class="text-center">NO</th>
                        <th rowspan="2" class="text-center">BULAN</th>
                        <th colspan="6" class="text-center">JUMLAH</th>
                        <th rowspan="2" class="text-center">SUBTOTAL</th>
                        <th rowspan="2" class="text-center">PEMASUKAN</th>
                      </tr>
                      <tr>
                        <th class="text-center">POS 1 (LEGAL)</th>
                        <th class="text-center">POS 2 (FASUMFASOS)</th>
                        <th class="text-center">POS 3 (OPERASIONAL)</th>
                        <th class="text-center">POS 4 (PROMOSI)</th>
                        <th class="text-center">POS 5 (BANGUNAN)</th>
                        <th class="text-center">POS 6 (PAJAK)</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $no = 1;
                      $peng_tahunan = $this->data['laporan_tahunan']['pengeluaran'];
                      $pem_tahunan = $this->data['laporan_tahunan']['pemasukan']['origin'];
                      // var_dump($pem_tahunan);exit();
                      // var_dump($jumlah['origin']);exit();
                      foreach ($this->data['laporan_tahunan']['pengeluaran']['origin'] as $key): ?>
                        <tr>
                          <td class="text-center" width="100"><?php echo $no ?></td>
                          <td class="text-center" width="100"><?php echo $key['bulan'] ?></td>
                          <td class="text-center"  width="100"><?php echo 'Rp. '.number_format($key['pos_legal'],0,',','.') ?></td>
                          <td class="text-center"  width="100"><?php echo 'Rp. '.number_format($key['pos_fasumfasos'],0,',','.') ?></td>
                          <td class="text-center"  width="100"><?php echo 'Rp. '.number_format($key['pos_operasional'],0,',','.') ?></td>
                          <td class="text-center"  width="100"><?php echo 'Rp. '.number_format($key['pos_promosi'],0,',','.') ?></td>
                          <td class="text-center"  width="100"><?php echo 'Rp. '.number_format($key['pos_bangunan'],0,',','.') ?></td>
                          <td class="text-center"  width="100"><?php echo 'Rp. '.number_format($key['pos_pajak'],0,',','.') ?></td>
                          <td class="text-center"  width="100"><?php       $jumlah_nota = ($key['pos_legal']
                            +$key['pos_fasumfasos']
                            +$key['pos_operasional']
                            +$key['pos_promosi']
                            +$key['pos_bangunan']
                            +$key['pos_pajak']);
                          echo 'Rp. '.number_format($jumlah_nota,0,',','.');
                          ?>
                        </td>
                        <td class="text-center"  width="100"></td>
                      </tr>
                      <?php $no++; endforeach ?>

                      <?php 
                      $index = count($this->data['laporan_tahunan']['pengeluaran']['origin']);
                      foreach ($this->data['laporan_tahunan']['pemasukan']['origin'] as $key): ?>
                        <tr>
                          <td class="text-center" width="100"><?php echo $index+1 ?></td>
                          <td class="text-center" width="100"><?php echo $key['bulan'] ?></td>
                          <td class="text-center" width="100"></td>
                          <td class="text-center" width="100"></td>
                          <td class="text-center" width="100"></td>
                          <td class="text-center" width="100"></td>
                          <td class="text-center" width="100"></td>
                          <td class="text-center" width="100"></td>
                          <td class="text-center" width="100"></td>
                          <td class="text-center" width="100"><?php echo 'Rp. '.number_format($key['jumlah'],0,',','.') ?></td>
                        </tr>
                        <?php $index++; endforeach; ?>

                        <tr>
                          <td colspan="2" class="text-center"><b>TOTAL</td>
                            <td class="text-center"><b><?php echo 'Rp. '.number_format($peng_tahunan['legal'],0,',','.') ?></b></td>
                            <td class="text-center"><b><?php echo 'Rp. '.number_format($peng_tahunan['fasumfasos'],0,',','.') ?></b></td>
                            <td class="text-center"><b><?php echo 'Rp. '.number_format($peng_tahunan['operasional'],0,',','.') ?></b></td>
                            <td class="text-center"><b><?php echo 'Rp. '.number_format($peng_tahunan['promosi'],0,',','.') ?></b></td>
                            <td class="text-center"><b><?php echo 'Rp. '.number_format($peng_tahunan['bangunan'],0,',','.') ?></b></td>
                            <td class="text-center"><b><?php echo 'Rp. '.number_format($peng_tahunan['pajak'],0,',','.') ?></b></td>
                            <td class="text-center" width="100"><b><?php $total = $peng_tahunan['legal']
                            +$peng_tahunan['fasumfasos']
                            +$peng_tahunan['operasional']
                            +$peng_tahunan['promosi']
                            +$peng_tahunan['bangunan']
                            +$peng_tahunan['pajak'];
                            echo 'Rp. '.number_format($total,0,',','.') ?></b></td>
                            <td class="text-center"><b><?php echo 'Rp. '.number_format($this->data['laporan_tahunan']['pemasukan']['jumlah'],0,',','.'); ?></b></td>
                          </tr>
                        </tbody>
                        <tfoot class="text-center">
                          <?php $rekapitulasi = $this->data['laporan_tahunan']['pemasukan']['jumlah'] - $total; ?>
                          <tr>
                            <td colspan="2"><b>Sisa budget</b></td>
                            <td><b><?php echo 'Rp. '.number_format($this->data['sisa_pos']['legal'],0,',','.'); ?></b></td>
                            <td><b><?php echo 'Rp. '.number_format($this->data['sisa_pos']['fasumfasos'],0,',','.'); ?></b></td>
                            <td><b><?php echo 'Rp. '.number_format($this->data['sisa_pos']['operasional'],0,',','.'); ?></b></td>
                            <td><b><?php echo 'Rp. '.number_format($this->data['sisa_pos']['promosi'],0,',','.'); ?></b></td>
                            <td><b><?php echo 'Rp. '.number_format($this->data['sisa_pos']['bangunan'],0,',','.'); ?></b></td>
                            <td><b><?php echo 'Rp. '.number_format($this->data['sisa_pos']['pajak'],0,',','.'); ?></b></td>
                            <td colspan="2">
                            </tr>
                          </tfoot>
                        </table>
                      <?php } ?>
                    </div>
                  </div>
                </div>
                <br><br>
                <div class="row">
                  <div class="col-md-5">
                    <div <?php if ($rekapitulasi < 0) {?>
                      class="alert alert-warning"
                    <?php }else{?>
                      class="alert alert-primary"
                      <?php } ?> role="alert">
                      <b>Rekapitulasi: <?php echo 'Rp. '.number_format($rekapitulasi,0,',','.'); ?></b>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <br>
            <!-- end of laporan tahunan -->

          <?php } ?>
          <br><br>

        </div>