<?php 

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=$title.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<table border="1" id="tabel_pengeluaran">
                  <thead>
                    <tr>
                      <th colspan="10">
                        <div class="row">
                          <div class="col-md-2">
                            <img src="<?php echo base_url('assets/img/'.$this->session->userdata('foto')) ?>" width="50">
                          </div>
                          <div class="col-md-8">
                            <div class="text-center">
                              <p><?php echo strtoupper($this->session->userdata('nama_proyek')); ?><hr>
                                REKAP LAPORAN TAHUNAN PER POS<br>
                                TAHUN <?php echo $this->session->userdata('filter_date') ?></p>
                              </div>
                            </div>
                            <div class="col-md-2">
                              <br>
                            </div>
                          </div>
                        </th>
                      </tr>
                      <tr>
                        <th rowspan="2" class="text-center">NO</th>
                        <th rowspan="2" class="text-center">BULAN</th>
                        <th colspan="6" class="text-center">JUMLAH</th>
                        <th rowspan="2" class="text-center">SUBTOTAL</th>
                        <th rowspan="2" class="text-center">PEMASUKAN</th>
                      </tr>
                      <tr>
                        <th class="text-center">POS 1 (LEGAL)</th>
                        <th class="text-center">POS 2 (FASUMFASOS)</th>
                        <th class="text-center">POS 3 (OPERASIONAL)</th>
                        <th class="text-center">POS 4 (PROMOSI)</th>
                        <th class="text-center">POS 5 (BANGUNAN)</th>
                        <th class="text-center">POS 6 (PAJAK)</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $no = 1;
                      $peng_tahunan = $this->data['laporan_tahunan']['pengeluaran'];
                      $pem_tahunan = $this->data['laporan_tahunan']['pemasukan']['origin'];
                      // var_dump($pem_tahunan);exit();
                      // var_dump($jumlah['origin']);exit();
                      foreach ($this->data['laporan_tahunan']['pengeluaran']['origin'] as $key): ?>
                      <tr>
                        <td class="text-center" width="100"><?php echo $no ?></td>
                        <td class="text-center" width="100"><?php echo $key['bulan'] ?></td>
                        <td class="text-center"  width="100"><?php echo 'Rp. '.number_format($key['pos_legal'],0,',','.') ?></td>
                        <td class="text-center"  width="100"><?php echo 'Rp. '.number_format($key['pos_fasumfasos'],0,',','.') ?></td>
                        <td class="text-center"  width="100"><?php echo 'Rp. '.number_format($key['pos_operasional'],0,',','.') ?></td>
                        <td class="text-center"  width="100"><?php echo 'Rp. '.number_format($key['pos_promosi'],0,',','.') ?></td>
                        <td class="text-center"  width="100"><?php echo 'Rp. '.number_format($key['pos_bangunan'],0,',','.') ?></td>
                        <td class="text-center"  width="100"><?php echo 'Rp. '.number_format($key['pos_pajak'],0,',','.') ?></td>
                        <td class="text-center"  width="100"><?php       $jumlah_nota = ($key['pos_legal']
                          +$key['pos_fasumfasos']
                          +$key['pos_operasional']
                          +$key['pos_promosi']
                          +$key['pos_bangunan']
                          +$key['pos_pajak']);
                        echo 'Rp. '.number_format($jumlah_nota,0,',','.');
                        ?>
                      </td>
                      <td class="text-center"  width="100"></td>
                    </tr>
                    <?php $no++; endforeach ?>

                    <?php 
                    $index = count($this->data['laporan_tahunan']['pengeluaran']['origin']);
                    foreach ($this->data['laporan_tahunan']['pemasukan']['origin'] as $key): ?>
                  <tr>
                    <td class="text-center" width="100"><?php echo $index+1 ?></td>
                    <td class="text-center" width="100"><?php echo $key['bulan'] ?></td>
                    <td class="text-center" width="100"></td>
                    <td class="text-center" width="100"></td>
                    <td class="text-center" width="100"></td>
                    <td class="text-center" width="100"></td>
                    <td class="text-center" width="100"></td>
                    <td class="text-center" width="100"></td>
                    <td class="text-center" width="100"></td>
                    <td class="text-center" width="100"><?php echo 'Rp. '.number_format($key['jumlah'],0,',','.') ?></td>
                  </tr>
                  <?php $index++; endforeach; ?>

                    <tr>
                      <td colspan="2" class="text-center"><b>TOTAL</td>
                      <td class="text-center"><b><?php echo 'Rp. '.number_format($peng_tahunan['legal'],0,',','.') ?></b></td>
                      <td class="text-center"><b><?php echo 'Rp. '.number_format($peng_tahunan['fasumfasos'],0,',','.') ?></b></td>
                      <td class="text-center"><b><?php echo 'Rp. '.number_format($peng_tahunan['operasional'],0,',','.') ?></b></td>
                      <td class="text-center"><b><?php echo 'Rp. '.number_format($peng_tahunan['promosi'],0,',','.') ?></b></td>
                      <td class="text-center"><b><?php echo 'Rp. '.number_format($peng_tahunan['bangunan'],0,',','.') ?></b></td>
                      <td class="text-center"><b><?php echo 'Rp. '.number_format($peng_tahunan['pajak'],0,',','.') ?></b></td>
                      <td class="text-center" width="100"><b><?php $total = $peng_tahunan['legal']
                      +$peng_tahunan['fasumfasos']
                      +$peng_tahunan['operasional']
                      +$peng_tahunan['promosi']
                      +$peng_tahunan['bangunan']
                      +$peng_tahunan['pajak'];
                        echo 'Rp. '.number_format($total,0,',','.') ?></b></td>
                        <td class="text-center"><b><?php echo 'Rp. '.number_format($this->data['laporan_tahunan']['pemasukan']['jumlah'],0,',','.'); ?></b></td>
                      </tr>
                    </tbody>
                    <tfoot class="text-center">
                      <?php $rekapitulasi = $this->data['laporan_tahunan']['pemasukan']['jumlah'] - $total; ?>
                      <tr>
                        <td colspan="2"><b>Sisa budget</b></td>
                        <td><b><?php echo 'Rp. '.number_format($this->data['sisa_pos']['legal'],0,',','.'); ?></b></td>
                        <td><b><?php echo 'Rp. '.number_format($this->data['sisa_pos']['fasumfasos'],0,',','.'); ?></b></td>
                        <td><b><?php echo 'Rp. '.number_format($this->data['sisa_pos']['operasional'],0,',','.'); ?></b></td>
                        <td><b><?php echo 'Rp. '.number_format($this->data['sisa_pos']['promosi'],0,',','.'); ?></b></td>
                        <td><b><?php echo 'Rp. '.number_format($this->data['sisa_pos']['bangunan'],0,',','.'); ?></b></td>
                        <td><b><?php echo 'Rp. '.number_format($this->data['sisa_pos']['pajak'],0,',','.'); ?></b></td>
                        <td colspan="2"></td>
                      </tr>
                    </tfoot>
                  </table>
          <br><br>
          <div class="row">
            <div class="col-md-5">
            <div <?php if ($rekapitulasi < 0) {?>
                        class="alert alert-warning"
                      <?php }else{?>
                        class="alert alert-primary"
                      <?php } ?> role="alert">
              <b>Rekapitulasi: <?php echo 'Rp. '.number_format($rekapitulasi,0,',','.'); ?></b>
            </div>
            </div>
          </div>

<table border="0" width="50%">
  <thead>
    <tr>
      <th></th>
      <th rowspan="6" colspan="11"><br>Yogyakarta, <?php echo date("l jS \of F Y") ?><br> Dibuat Oleh <br><br><br> <?php echo strtoupper($this->session->userdata('username')); ?> </th>
    </tr>
  </thead>
  <tbody></tbody>
  <tr></tr>
  <tr></tr>
</table>