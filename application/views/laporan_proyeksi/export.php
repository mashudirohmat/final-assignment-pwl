<?php 

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=$title.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<table border="1" id="tabel_pajak" width="100%">
                  <thead>
                    <tr>
                      <th colspan="13">
                        <div class="row">
                          <div class="col-md-2">
                            <img src="<?php echo base_url('assets/img/'.$this->session->userdata('foto')) ?>" width="50">
                          </div>
                          <div class="col-md-8">
                            <div class="text-center">
                              <p><?php echo strtoupper($this->session->userdata('nama_proyek')); ?><hr>
                                REKAP LAPORAN PROYEKSI<br>
                                BULAN <?php echo strtoupper($this->session->userdata('filter_date')) ?></p>
                              </div>
                            </div>
                            <div class="col-md-2">
                              <br>
                            </div>
                          </div>
                        </th>
                      </tr>
                      <tr>
                        <th class="text-center">NO</th>
                        <th colspan="2" class="text-center">NAMA RUMAH</th>
                        <th colspan="2" class="text-center">HARGA RUMAH</th>
                        <th colspan="2" class="text-center">NAMA PEMBELI</th>
                        <th colspan="3" class="text-center">TANGGAL BELI</th>
                        <th class="text-center">JADWAL BAYAR</th>
                        <th colspan="2" class="text-center">JUMLAH</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $no = 1;
                      foreach ($this->data['laporan_proyeksi'] as $key): ?>
                      <tr>
                        <td class="text-center" width="10"><?php echo $no ?></td>
                        <td colspan="2" class="text-center" width="20"><?php echo $key['nama_rumah'] ?></td>
                        <td colspan="2" class="text-center" width="20"><?php echo 'Rp. '.number_format($key['harga_rumah'],0,',','.') ?></td>
                        <td colspan="2" class="text-center" width="20"><?php echo $key['nama_pembeli'] ?></td>
                        <td colspan="3" class="text-center" width="20"><?php echo $key['trx_date'] ?></td>
                        <td class="text-center" width="20"><?php echo $key['tgl_jatuh_tempo'] ?></td>
                        <td class="text-center" width="20"><?php echo 'Rp. '.number_format($key['kontrak_termin'],0,',','.') ?></td>
                      </tr>
                      <?php $no++;endforeach; ?>
                      <tr>
                        <?php
                        $jumlah = 0; 
                        for ($i=0; $i < count($this->data['laporan_proyeksi']) ; $i++) { 
                          $jumlah += $this->data['laporan_proyeksi'][$i]['kontrak_termin'];
                        } ?>
                        <td colspan="11" class="text-center"><b>TOTAL</td>
                        <td colspan="2" class="text-center"><b><?php echo 'Rp. '.number_format($jumlah,0,',','.') ?></b></td>
                      </tr>
                    </tbody>
                  </table>

<table border="0" width="50%">
  <thead>
    <tr>
      <th></th>
      <th rowspan="6" colspan="11"><br>Yogyakarta, <?php echo date("l jS \of F Y") ?><br> Dibuat Oleh <br><br><br> <?php echo strtoupper($this->session->userdata('username')); ?> </th>
    </tr>
  </thead>
  <tbody></tbody>
  <tr></tr>
  <tr></tr>
</table>