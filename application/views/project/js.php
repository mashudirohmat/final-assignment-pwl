<script type="text/javascript">
  $('.mandor').selectize({
      maxItems: 3,
      delimiter:',',
      create: true,
      sortField: 'text'
    });
    $('.admin').selectize({
      maxItems:3,
      delimiter:',',
      persist:false,
      create: function(input){
        return{
          value:input,
          text:input
        }
      }
    });
	function submitpos(x){
      if (x=='tambah') {
        $('#btn-tambahpos').show();
        $('#btn-ubahpos').hide();
        $('#modal-titlepos').html('<b>Form Set Pos Budget</b>');

        $("[name='posLegal']").val('');
        $("[name='posFasumFasos']").val('');
        $("[name='posOperasional']").val('');
        $("[name='posPromosi']").val('');
        $("[name='posBangunan']").val('');
        $("[name='posBiayaPajak']").val('');

      }else{
        $('#btn-tambahpos').hide();
        $('#btn-ubahpos').show();
        $('#btn-ubahpos').attr('disabled',false);
        $('#btn-ubahpos').html('Ubah <i class="fa fa-paper-plane-o ml-1"></i>');
        $('#modal-titlepos').html('<b>Form Ubah Pos Budget</b>');

        $.ajax({
          type:"POST",
          data:'id='+x,
          url:'<?php echo base_url('index.php/project/getPosById') ?>',
          dataType:'JSON',
          success: function(data){
            $("[name='posLegal']").val(data[0].pos_legal);
            $("[name='posFasumFasos']").val(data[0].pos_fasumfasos);
            $("[name='posOperasional']").val(data[0].pos_operasional);
            $("[name='posPromosi']").val(data[0].pos_promosi);
            $("[name='posBangunan']").val(data[0].pos_bangunan);
            $("[name='posBiayaPajak']").val(data[0].pos_pajak);
          }
        })
      }
    }

    function submitrumah(x){
      if (x=='tambah') {
        $('#btn-tambahrumah').show();
        $('#btn-ubahrumah').hide();
        $('#modal-titlerumah').html('<b>Form Tambah Rumah</b>');

        $("[name='nama_rumah']").val('');
        $("[name='tipe_rumah']").val('');
        $("[name='harga_rumah']").val('');

      }else{
        $('#btn-tambahrumah').hide();
        $('#btn-ubahrumah').show();
        $('#btn-ubahrumah').attr('disabled',false);
        $('#btn-ubahrumah').html('Ubah <i class="fa fa-paper-plane-o ml-1"></i>');
        $('#modal-titlerumah').html('<b>Form Ubah Rumah</b>');

        $.ajax({
          type:"POST",
          data:'id='+x,
          url:'<?php echo base_url('index.php/project/getRumahById') ?>',
          dataType:'JSON',
          success: function(data){
            $("[name='nama_rumah']").val(data[0].nama_rumah);
            $("[name='tipe_rumah']").val(data[0].tipe_rumah);
            $("[name='harga_rumah']").val(data[0].harga_rumah);
            $("[name='id_rumah']").val(data[0].id_rumah);
          }
        })
      }
    }

    function submitbelirumah(x){
        $('#btn-ubahbelirumah').hide();
        $('#btn-tambahbelirumah').show();
        $('#btn-tambahbelirumah').attr('disabled',false);
        $('#btn-tambahbelirumah').html('Beli <i class="fa fa-paper-plane-o ml-1"></i>');
        $('#modal-titlebelirumah').html('<b>Form Pembelian Rumah</b>');

        $.ajax({
          type:"POST",
          data:'id='+x,
          url:'<?php echo base_url('index.php/project/getRumahById') ?>',
          dataType:'JSON',
          success: function(data){
            $("[name='id_rumah2']").val(data[0].id_rumah);
          }
        })
    }

    function tambahbelirumah(){
      if ($("[name='nama_pembeli']").val()=='') {
        $('#namapembeli_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='alamat_pembeli']").val()==''){
        $('#alamatpembeli_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='noktp_pembeli']").val()==''){
        $('#noktp_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='nokk_pembeli']").val()==''){
        $('#nokk_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='nohp_pembeli']").val()==''){
        $('#nohp_pembeli_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else{
        $('#btn-tambahbelirumah').html("<i class='fa fa-spinner fa-spin'></i> Saving...");
        $('#btn-tambahbelirumah').attr('disabled',true);
        var formData = new FormData($('#form_pembeli_rumah')[0]);

        $.ajax({
          type:'POST',
          data: formData,
          contentType: false,
          processData: false,
          url:'<?php echo base_url('index.php/project/beliRumah') ?>',
          dataType : 'JSON',
          success : function(hasil){
            console.log(hasil);
            if (hasil.status=='success') {
              $("#form_beli_rumah").modal('hide');

              $("[name='id_rumah2']").val('');
              $("[name='nama_pembeli']").val('');
              $("[name='alamat_pembeli']").val('');
              $("[name='noktp_pembeli']").val('');
              $("[name='nokk_pembeli']").val('');

              window.location.href = hasil.redirect_url;
            }
          }
        });
      }
    }

    function tambahpos(){
      if ($("[name='posLegal']").val()=='') {
        $('#posLegal_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='posFasumFasos']").val()==''){
        $('#posFasumFasos_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='posOperasional']").val()==''){
        $('#posOperasional_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='posPromosi']").val()==''){
        $('#posPromosi_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='posBangunan']").val()==''){
        $('#posBangunan_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='posBiayaPajak']").val()==''){
        $('#posBiayaPajak_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else{
        $('#btn-tambahpos').html("<i class='fa fa-spinner fa-spin'></i> Saving...");
        $('#btn-tambahpos').attr('disabled',true);
        var formData = new FormData($('#formpos')[0]);

        $.ajax({
          type:'POST',
          data: formData,
          contentType: false,
          processData: false,
          url:'<?php echo base_url('index.php/project/setPos') ?>',
          dataType : 'JSON',
          success : function(hasil){
            console.log(hasil);
            if (hasil.status=='success') {
              $("#setpos").modal('hide');

              $("[name='posLegal']").val('');
              $("[name='posFasumFasos']").val('');
              $("[name='posOperasional']").val('');
              $("[name='posPromosi']").val('');
              $("[name='posBangunan']").val('');
              $("[name='posBiayaPajak']").val('');

              window.location.href = hasil.redirect_url;
            }
          }
        });
      }
    }

    function tambahrumah(){
      if ($("[name='nama_rumah']").val()=='') {
        $('#namarumah_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='tipe_rumah']").val()==''){
        $('#tiperumah_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='harga_rumah']").val()=='Pilih hak akses . . .'){
        $('#hargarumah_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else{
        $('#btn-tambahrumah').html("<i class='fa fa-spinner fa-spin'></i> Saving...");
        $('#btn-tambahrumah').attr('disabled',true);
        var formData = new FormData($('#form_rumah')[0]);

        $.ajax({
          type:'POST',
          data: formData,
          contentType: false,
          processData: false,
          url:'<?php echo base_url('index.php/project/addRumah') ?>',
          dataType : 'JSON',
          success : function(hasil){
            console.log(hasil);
            if (hasil.status=='success') {
              $("#form_add_rumah").modal('hide');

              $("[name='nama_rumah']").val('');
              $("[name='tipe_rumah']").val('');
              $("[name='harga_rumah']").val('');
              $("[name='id_rumah']").val('');

              window.location.href = hasil.redirect_url;
            }
          }
        });
      }
    }
    
    function ubahpos(){
      if ($("[name='posLegal']").val()=='') {
        $('#posLegal_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='posFasumFasos']").val()==''){
        $('#posFasumFasos_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='posOperasional']").val()==''){
        $('#posOperasional_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='posPromosi']").val()==''){
        $('#posPromosi_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='posBangunan']").val()==''){
        $('#posBangunan_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='posBiayaPajak']").val()==''){
        $('#posBiayaPajak_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else{
        $('#btn-ubahpos').html("<i class='fa fa-spinner fa-spin'></i> Updating...");
        $('#btn-ubahpos').attr('disabled',true);
        var formData = new FormData($('#formpos')[0]);

        $.ajax({
          type:'POST',
          data: formData,
          contentType: false,
          processData: false,
          url:'<?php echo base_url('index.php/project/updatePos') ?>',
          dataType : 'JSON',
          success : function(hasil){
            if (hasil.status=='success') {
              $("#setpos").modal('hide');

              $("[name='posLegal']").val('');
              $("[name='posFasumFasos']").val('');
              $("[name='posOperasional']").val('');
              $("[name='posPromosi']").val('');
              $("[name='posBangunan']").val('');
              $("[name='posBiayaPajak']").val('');

              window.location.href = hasil.redirect_url;
            }
          }
        });
      }
    }

    
    
    function ubahrumah(){
      if ($("[name='nama_rumah']").val()=='') {
        $('#namarumah_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='tipe_rumah']").val()==''){
        $('#tiperumah_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='harga_rumah']").val()=='Pilih hak akses . . .'){
        $('#hargarumah_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else{
        $('#btn-ubahrumah').html("<i class='fa fa-spinner fa-spin'></i> Updating...");
        $('#btn-ubahrumah').attr('disabled',true);
        var formData = new FormData($('#form_rumah')[0]);

        $.ajax({
          type:'POST',
          data: formData,
          contentType: false,
          processData: false,
          url:'<?php echo base_url('index.php/project/updateRumah') ?>',
          dataType : 'JSON',
          success : function(hasil){
            console.log(hasil);
            if (hasil.status=='success') {
              $("#form_add_rumah").modal('hide');

              $("[name='nama_rumah']").val('');
              $("[name='tipe_rumah']").val('');
              $("[name='harga_rumah']").val('');
              $("[name='id_rumah']").val('');

              window.location.href = hasil.redirect_url;
            }
          }
        });
      }
    }


</script>
<script>
var ctx = document.getElementById('myChart');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'],
        datasets: [{
            label: '# pengeluaran',
            data: ["<?php echo $this->data['pengeluaran']['a'] ?>",
                    "<?php echo $this->data['pengeluaran']['b'] ?>",
                    "<?php echo $this->data['pengeluaran']['c'] ?>",
                    "<?php echo $this->data['pengeluaran']['d'] ?>",
                    "<?php echo $this->data['pengeluaran']['e'] ?>",
                    "<?php echo $this->data['pengeluaran']['f'] ?>",
                    "<?php echo $this->data['pengeluaran']['g'] ?>",
                    "<?php echo $this->data['pengeluaran']['h'] ?>",
                    "<?php echo $this->data['pengeluaran']['i'] ?>",
                    "<?php echo $this->data['pengeluaran']['j'] ?>",
                    "<?php echo $this->data['pengeluaran']['k'] ?>",
                    "<?php echo $this->data['pengeluaran']['l'] ?>"],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)'
            ]
        },
        {
            label: '# pemasukan',
            data: ["<?php echo $this->data['pemasukan']['a'] ?>",
                    "<?php echo $this->data['pemasukan']['b'] ?>",
                    "<?php echo $this->data['pemasukan']['c'] ?>",
                    "<?php echo $this->data['pemasukan']['d'] ?>",
                    "<?php echo $this->data['pemasukan']['e'] ?>",
                    "<?php echo $this->data['pemasukan']['f'] ?>",
                    "<?php echo $this->data['pemasukan']['g'] ?>",
                    "<?php echo $this->data['pemasukan']['h'] ?>",
                    "<?php echo $this->data['pemasukan']['i'] ?>",
                    "<?php echo $this->data['pemasukan']['j'] ?>",
                    "<?php echo $this->data['pemasukan']['k'] ?>",
                    "<?php echo $this->data['pemasukan']['l'] ?>"],
            backgroundColor: [
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)'
            ]
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>
</body>
  <!--end::Body-->
</html>