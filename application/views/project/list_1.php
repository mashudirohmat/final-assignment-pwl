<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
              <!--begin::Entry-->
            <div class="d-flex flex-column-fluid">
              <!--begin::Container-->
<div class="container">
  <div class="card">
    <div class="card-header">
      Projects
      <hr style="height:2px;border-width:0;color:blue;background-color:blue;width:50px;text-align:left;margin-left:0">
      <div class="text-right">
        <?php if ($this->session->userdata('hak_akses')== 'owner') {?>
          <a href="#formaddproject" class="btn btn-sm btn-primary text-right" onclick="submitpr('tambah')" data-toggle="modal"><i class="fas fa-plus fa-sm text-right"></i>Tambah Project</a>
        <?php } ?>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <?php foreach ($this->data['project'] as $key):?>
          <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><?php echo $key['nama_proyek'] ?></div>
                    <a class="btn btn-success btn-sm" href="<?php echo base_url('index.php/project/detailProject/'.$key['id_proyek']) ?>">
                      <i class="fas fa-edit"></i>View
                    </a>
                  </div>
                  <div class="col-auto">
                    <!-- <img src="<?php //echo base_url('assets/img/'.$key['foto']) ?>" width="50"> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php endforeach ?>
      </div>
    </div>
  </div>
  </div>
</div>