<!-- MODAL ADD PROJECT -->
<div class="modal fade" id="formaddproject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="modal-titlepr"></h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body text-xs">
        <form actionrole="form" id="formproject" class="foem-xs" onSubmit="return false" enctype="multipart/form-data" id="formpr">
          <input type="hidden" name="id_proyek">
          <div class="form-group">
            <label>Nama Proyek</label>
            <input type="text" name="namaproyek" id="namaproyek" placeholder="Nama Proyek ..." required="" class="form-control form-sm">
            <p id="namaproyek_alert" style="color: red"></p>
          </div>
          <div class="form-group">
            <label>Lokasi</label>
            <input type="text" name="lokasiproyek" id="lokasiproyek" placeholder="Lokasi Proyek ..." required="" class="form-control form-sm">
            <p id="lokasiproyek_alert" style="color: red"></p>
          </div>
          <div class="form-group">
            <label>Admin</label>
            <select class="form-control admin" name="admin[]" id="admin">
              <option selected="" disabled="" value="">Pilih admin . . .</option>
              <?php foreach ($this->data['admin'] as $key):?>
                <option value="<?php echo $key['id_user'] ?>"><?php echo strtoupper($key['username']) ?></option>
              <?php endforeach ?>
            </select>
            <p id="admin_alert" style="color: red"></p>
          </div>
          <div class="form-group">
            <label>Mandor</label>
            <select class="form-control mandor" name="mandor[]" id="mandor">
              <option selected="" disabled="" value="">Pilih mandor . . .</option>
              <?php foreach ($this->data['mandor'] as $key): ?>
                <option value="<?php echo $key['id_user']?>"><?php echo strtoupper($key['username']) ?></option>
              <?php endforeach ?>
            </select>
            <p id="mandor_alert" style="color: red"></p>
          </div>
          <div class="form-group" id="photo-preview">
            <label class="control-label col-md-3">Preview</label>
            <div class="col-md-9">
              (No photo)
              <span class="help-block"></span>
            </div>
          </div>
          <div class="form-group">
            <label>Foto</label>
            <p>* maximal ukuran file 100 kb</p>
            <input type="file" name="foto" id="foto">
            <p id="foto_alert" style="color: red"></p>
          </div>
        </form>
      </div>
      <p id="error_message" style="color:red"></p>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
        <a class="btn btn-primary" id="btn-ubahpr" onclick="ubahproyek()" href="#!">Ubah</a>
        <a class="btn btn-primary" id="btn-tambahpr" onclick="tambahproyek()" href="#!">Simpan</a>
      </div>
    </div>
  </div>
</div>
<!-- END OF MODAL ADD PROJECT -->

<!-- MODAL SET POS -->
<div class="modal fade" id="setpos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="modal-titlepos"></h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body text-xs">
        <form actionrole="form" class="foem-xs" onSubmit="return false" enctype="multipart/form-data" id="formpos">
          <input type="hidden" name="id" value="<?php echo $this->session->userdata('id_proyek') ?>">
          <div class="form-group">
            <label>Pos Legal (Rp)</label>
            <input type="text" name="posLegal" id="posLegal" placeholder="Masukkan Budget ..." required="" class="form-control form-sm rp">
            <p id="posLegal_alert" style="color: red"></p>
          </div>
          <div class="form-group">
            <label>Pos Fasum & Fasos (Rp)</label>
            <input type="text" name="posFasumFasos" id="posFasumFasos" placeholder="Masukkan Budget ..." required="" class="form-control form-sm rp">
            <p id="posFasumFasos_alert" style="color: red"></p>
          </div>
          <div class="form-group">
            <label>Pos Operasional (Rp)</label>
            <input type="text" name="posOperasional" id="posOperasional" placeholder="Masukkan Budget ..." required="" class="form-control form-sm rp">
            <p id="posOperasional_alert" style="color: red"></p>
          </div>
          <div class="form-group">
            <label>Pos Promosi (Rp)</label>
            <input type="text" name="posPromosi" id="posPromosi" placeholder="Masukkan Budget ..." required="" class="form-control form-sm rp">
            <p id="posPromosi_alert" style="color: red"></p>
          </div>
          <div class="form-group">
            <label>Pos Bangunan (Rp)</label>
            <input type="text" name="posBangunan" id="posBangunan" placeholder="Masukkan Budget ..." required="" class="form-control form-sm rp">
            <p id="posBangunan_alert" style="color: red"></p>
          </div>
          <div class="form-group">
            <label>Pos Biaya Pajak (Rp)</label>
            <input type="text" name="posBiayaPajak" id="posBiayaPajak" placeholder="Masukkan Budget ..." required="" class="form-control form-sm rp">
            <p id="posBiayaPajak_alert" style="color: red"></p>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
        <a class="btn btn-primary" id="btn-ubahpos" onclick="ubahpos()" href="#!">Ubah</a>
        <a class="btn btn-primary" id="btn-tambahpos" onclick="tambahpos()" href="#!">Simpan</a>
      </div>
    </div>
  </div>
</div>
<!-- EN OF SET POS -->

<!-- MODAL ADD RUMAH -->
<div class="modal fade" id="form_add_rumah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title text-center" id="modal-titlerumah"></h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body text-xs">
          <form actionrole="form" id="form_rumah" class="foem-xs" onSubmit="return false" enctype="multipart/form-data" id="form">
            <input type="hidden" name="id_rumah" value="">
            <div class="form-group">
              <label>Nama Rumah</label>
              <input type="text" name="nama_rumah" id="nama_rumah" placeholder="Nama Rumah ..." required="" class="form-control form-sm">
              <p id="namarumah_alert" style="color: red"></p>
            </div>
            <div class="form-group">
              <label>Tipe</label>
              <input type="text" name="tipe_rumah" id="tipe_rumah" placeholder="Tipe Rumah ..." required="" class="form-control form-sm">
              <p id="tiperumah_alert" style="color: red"></p>
            </div>
            <div class="form-group">
              <label>Harga</label>
              <input type="text" name="harga_rumah" id="harga_rumah" placeholder="Harga ..." required="" class="form-control form-sm rp">
              <p id="hargarumah_alert" style="color: red"></p>
            </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
          <a class="btn btn-primary" id="btn-ubahrumah" onclick="ubahrumah()" href="#!">Ubah</a>
          <a class="btn btn-primary" id="btn-tambahrumah" onclick="tambahrumah()" href="#!">Simpan</a>
        </div>
      </form>
      </div>
    </div>
  </div>
<!-- END OF MODAL ADD RUMAH -->

<!-- MODAL BELI RUMAH -->
<div class="modal fade" id="form_beli_rumah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title text-center" id="modal-titlebelirumah"></h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body text-xs">
          <form actionrole="form" id="form_pembeli_rumah" class="form-xs" onSubmit="return false" enctype="multipart/form-data" id="form">
            <div class="row">
              <div class="col-md-6">
            <input type="hidden" name="id_rumah2" value="">
            <div class="form-group">
              <label>Nama Pembeli</label>
              <input type="text" name="nama_pembeli" id="nama_pembeli" placeholder="Nama Pembeli ..." required="" class="form-control form-sm">
              <p id="namapembeli_alert" style="color: red"></p>
            </div>
            <div class="form-group">
              <label>Alamat</label>
              <input type="text" name="alamat_pembeli" id="alamat_pembeli" placeholder="Alamat ..." required="" class="form-control form-sm">
              <p id="alamatpembeli_alert" style="color: red"></p>
            </div>
            <div class="form-group">
              <label>No. KTP</label>
              <input type="text" name="noktp_pembeli" id="noktp_pembeli" placeholder="No. KTP ..." required="" class="form-control form-sm">
              <p id="noktp_alert" style="color: red"></p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>No. Kartu Keluarga</label>
              <input type="text" name="nokk_pembeli" id="nokk_pembeli" placeholder="No. Kartu Keluarga ..." required="" class="form-control form-sm">
              <p id="nokk_alert" style="color: red"></p>
            </div>
            <div class="form-group">
              <label>No. HP</label>
              <input type="text" name="nohp_pembeli" id="nohp_pembeli" placeholder="No. HP ..." required="" class="form-control form-sm">
              <p id="nohp_pembeli_alert" style="color: red"></p>
            </div>
            </div>
            </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
          <a class="btn btn-primary" id="btn-ubahbelirumah" onclick="ubahbelirumah()" href="#!">Ubah</a>
          <a class="btn btn-primary" id="btn-tambahbelirumah" onclick="tambahbelirumah()" href="#!">Simpan</a>
        </div>
      </form>
      </div>
    </div>
  </div>
<!-- END OF MODAL BELI RUMAH -->