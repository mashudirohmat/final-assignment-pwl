<?php foreach ($this->data['project'] as $key):?>
<?php require_once(APPPATH.'views/layout/nav_proyek.php'); ?>
<?php endforeach ?>
<!-- budget -->
<div class="container"  style="padding-bottom: 150px">
<?php if ($this->session->userdata('hak_akses') == 'pimpro'): ?>
<div class="card">
  <?php foreach ($this->data['request'] as $key) :?>
    <div class="alert alert-primary" role="alert">
      <i class="fas fa-fw fa-tag"></i>
        <?php echo $key['username'] ?> melakukan request <?php echo $key['aksi'] ?> pada laporan pengeluaran,
        <a href="<?php echo base_url('index.php/laporan_pengeluaran/process_request/'.$key['id_request']) ?>">
          <b>klik untuk approvement</b>
        </a>
    </div>
  <?php endforeach; ?>
</div>
<?php endif ?>
<div class="card">
  <div class="card-header">
    <div class="row">
      <div class="col-md-1">
        <div class="text-center">
          <img src="<?php echo base_url('assets/img/'.$this->session->userdata('foto')) ?>" width="80">
        </div>
      </div>
      <div class="col-md-4">
        <font size="5" face="bold"><?php echo $this->session->userdata('nama_proyek') ?></font><br><br>
        <div class="row">
          <div class="col-md-4">
            <label>Admin :</label><br>
            <?php
            $i=1;
            foreach ($this->data['adminproject'] as $key):?>
              <b class="text-info text-xs"><i class="flaticon2-user-1"></i>&nbsp; <?php echo $key['username']; ?></b><br>
              <?php
              $i++;
            endforeach ?>
          </div>
          <div class="col-md-4">
            <label>Pimpro :</label><br>
            <?php
            $i=1;
            foreach ($this->data['mandorproject'] as $key):?>
              <b class="text-info text-xs"><i class="flaticon-avatar"></i>&nbsp; <?php echo $key['username']; ?></b><br>
              <?php
              $i++;
            endforeach ?>
          </div>
          <div class="col-md-4">
            <i class="flaticon2-pin"></i>&nbsp; <?php echo $this->session->userdata('lokasi') ?>
          </div>
        </div>
      </div>
      <div class="col-md-7">
        <?php if ($this->session->userdata('hak_akses')== 'owner') {?>
        <div class="text-right">
          <a class="btn btn-sm btn-warning" href="#formaddproject" onclick="submitpr(<?php echo $this->session->userdata('id_proyek') ?>)" data-toggle="modal">Update</a>
          <a onClick="konfirmasiArsip('<?php echo site_url('index.php/project/Arsip/'.$this->session->userdata('id_proyek')) ?>')" href="#!" class="btn btn-sm btn-danger">Arsipkan</a>
          <?php if ($this->data['pos']==null) {?>
          <a href="#setpos" class="btn btn-sm btn-primary text-right" onclick="submitpos('tambah')" data-toggle="modal">Set Pos</a>
          <?php }else{ ?>
            <a href="#setpos" class="btn btn-sm btn-primary text-right" onclick="submitpos(<?php echo $this->session->userdata('id_proyek') ?>)" data-toggle="modal">Ubah Pos</a>
            <?php } ?>
        </div>
        <?php } ?>
        <br>
        <div class="text-right">
          <h6>Tanggal mulai : <?php echo $this->session->userdata('tanggal') ?></h6>
        </div>
      </div>
    </div>
  </div>
  <div class="card-header">
    <div class="row">
      <?php @$budget = $this->data['pos'][0];?>
      <div class="col-md-2 text-center">
        <i class="flaticon2-pie-chart-1"></i>&nbsp; Pos 1 (Legal)<br>
        <p><?php echo 'Rp. '.number_format($budget['pos_legal'],2,',','.') ?></p>
      </div>
      <div class="col-md-2 text-center">
        <i class="flaticon2-pie-chart-1"></i>&nbsp; Pos 2 (Fasum Fasos)<br>
        <p><?php echo 'Rp. '.number_format($budget['pos_fasumfasos'],2,',','.') ?></p>
      </div>
      <div class="col-md-2 text-center">
        <i class="flaticon2-pie-chart-1"></i>&nbsp; Pos 3 (Operasional)<br>
        <p><?php echo 'Rp. '.number_format($budget['pos_operasional'],2,',','.') ?></p>
      </div>
      <div class="col-md-2 text-center">
        <i class="flaticon2-pie-chart-1"></i>&nbsp; Pos 4 (Promosi)<br>
        <p><?php echo 'Rp. '.number_format($budget['pos_promosi'],2,',','.') ?></p>
      </div>
      <div class="col-md-2 text-center">
        <i class="flaticon2-pie-chart-1"></i>&nbsp; Pos 5 (Bangunan)<br>
        <p><?php echo 'Rp. '.number_format($budget['pos_bangunan'],2,',','.') ?></p>
      </div>
      <div class="col-md-2 text-center">
        <i class="flaticon2-pie-chart-1"></i>&nbsp; Pos 6 (Pajak)<br>
        <p><?php echo 'Rp. '.number_format($budget['pos_pajak'],2,',','.') ?></p>
      </div>
    </div>
  </div>
  <div class="card-body">
    <div class="row">
      <div class="col-lg-7 col-xxl-7">
        <!--begin::Advance Table Widget 1-->
        <div class="card card-custom card-stretch gutter-b">
          <!--begin::Header-->
          <div class="card-header border-0 py-5">
            <h3 class="card-title align-items-start flex-column">
              <span class="card-label font-weight-bolder text-dark">Perumahan</span>
            </h3>
            <div class="card-toolbar">
              <a href="#form_add_rumah" onclick="submitrumah('tambah')" data-toggle="modal" class="btn btn-primary font-weight-bolder font-size-sm">
                <span class="svg-icon svg-icon-md svg-icon-white">
                  <!--end::Svg Icon-->
                </span>Tambah</a>
              </div>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body py-0">
              <!--begin::Table-->
              <div class="table-responsive">
                <table class="table table-head-custom table-vertical-center" id="kt_advance_table_widget_1">
                  <thead>
                    <tr class="text-center">
                      <th width="20%">Rumah</th>
                      <th width="15%">Harga</th>
                      <th width="10%">Pembeli/Pemilik</th>
                      <th width="5%">Status</th>
                      <th width="5%">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($this->data['rumah'] as $key): ?>
                    <tr>
                      <td>
                        <p><?php echo $key['nama_rumah'] ?></p>
                      </td>
                      <td>
                        <p><?php echo 'Rp. '.number_format($key['harga_rumah'],0,',','.') ?></p>
                      </td>
                      <td>
                        <small><?php echo (!empty($key['nama_pembeli'])) ? $key['nama_pembeli'] : "Belum dibeli" ?></small>
                      </td>
                      <td align="center">
                        <?php if ($key['is_buy'] == 1 && $key['is_lunas'] == 0) {?>
                            <span class="badge badge-danger">Progress</span>
                          <?php }elseif ($key['is_buy'] == 1 && $key['is_lunas'] == 1){ ?>
                            <span class="badge badge-info">Lunas</span>
                          <?php }elseif ($key['is_buy'] == 0){ ?>
                            <span class="badge badge-primary">Bebas</span>
                          <?php } ?>
                      </td>
                      <td class="pr-0 text-right">
                        <?php if ($key['is_buy'] != 1 ): ?>
                          <a href="#form_add_rumah" onclick="submitrumah(<?php echo $key['id_rumah'] ?>)" data-toggle="modal" class="btn btn-sm btn-warning text-xs p-1">Edit
                          </a>
                          <a onClick="konfirmasiArsip('<?php echo site_url('index.php/project/arsipRumah/'.$key['id_rumah']) ?>')" href="#!" class="btn btn-sm btn-danger text-xs p-1">Arsip
                          </a>
                          <a href="#form_beli_rumah" onclick="submitbelirumah(<?php echo $key['id_rumah'] ?>)" data-toggle="modal" class="btn btn-sm btn-success text-xs p-1">Beli
                          </a>
                        <?php else: ?>
                          <a href="<?php echo base_url('index.php/perjanjian?id='.$key['id_transaksi']) ?>" class="btn btn-sm btn-info text-xs p-1">View
                          </a>
                        <?php endif ?>
                      </td>
                    </tr>
                    <?php endforeach ?>
                  </tbody>
                </table>
              </div>
              <!--end::Table-->
            </div>
            <!--end::Body-->
          </div>
          <!--end::Advance Table Widget 1-->
        </div>

        <div class="col-lg-5 col-xxl-5">
        <!--begin::Advance Table Widget 1-->
        <div class="card card-custom card-stretch gutter-b">
          <!--begin::Header-->
          <div class="card-header border-0 py-5">
            <h3 class="card-title align-items-start flex-column">
              <span class="card-label font-weight-bolder text-dark">Keuangan</span>
            </h3>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body py-0">
                 <canvas id="myChart" width="400" height="400"></canvas>

            </div>
            <!--end::Body-->
          </div>
          <!--end::Advance Table Widget 1-->
        </div>
      </div>
    </div>
  </div>
  <!-- end of budget -->

</div>