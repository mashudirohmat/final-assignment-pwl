<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container">
      <div class="card">
        <div class="card-header">
          Projects
          <hr style="height:2px;border-width:0;color:blue;background-color:blue;width:50px;text-align:left;margin-left:0">
          <div class="text-right">
            <?php if ($this->session->userdata('hak_akses')== 'owner') {?>
              <a href="#formaddproject" class="btn btn-sm btn-primary text-right" onclick="submitpr('tambah')" data-toggle="modal"><i class="fas fa-plus fa-sm text-right"></i>Tambah Project</a>
            <?php } ?>
          </div>
        </div>
      </div>
      <!-- <div class="card-body"> --><br>
        <!-- <div class="card-body"> --><br>
          <div class="row">
            <!--begin::Col-->
            <?php foreach ($this->data['project'] as $key):?>
              <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                      <!--begin::Body-->
                      <div class="card-body pt-4">
                        <!--begin::Toolbar-->
                        <div class="d-flex justify-content-end">
                          <div class="dropdown dropdown-inline" data-toggle="tooltip" title="" data-placement="left" data-original-title="Quick actions">
                            <a href="#" class="btn btn-clean btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <!-- <i class="ki ki-bold-more-hor"></i> -->
                            </a>
                          </div>
                        </div>
                        <!--end::Toolbar-->
                        <!--begin::User-->
                        <div class="d-flex align-items-center mb-7">
                          <!--begin::Pic-->
                          <div class="flex-shrink-0 mr-4">
                            <div class="symbol symbol-circle symbol-lg-75">
                              <img src="<?php echo base_url('assets/img/'.$key['foto']) ?>" alt="image">
                            </div>
                          </div>
                          <!--end::Pic-->
                          <!--begin::Title-->
                          <div class="d-flex flex-column">
                            <a href="<?php echo base_url('index.php/project/detailProject/'.$key['id_proyek']) ?>" class="text-dark font-weight-bold text-hover-primary font-size-h4 mb-0"><?php echo $key['nama_proyek']; ?></a>
                            <span class="text-muted font-weight-bold">
                          <?php echo !empty($key['lokasi']) ? $key['lokasi'] : "Mohon update lokasi"; ?></span>
                          </div>
                          <!--end::Title-->
                        </div>
                        <!--end::User-->
                        <!--begin::Info-->
                        <div class="mb-7">
                          <div class="d-flex justify-content-between align-items-center">
                            <span class="text-dark-75 font-weight-bolder mr-2">Admin:</span>
                            <a href="#" class="text-muted text-hover-primary">
                              <?php 
                              foreach ($key['admin'] as $k => $v) {
                                echo $v." ,";
                              }
                               ?>
                            </a>
                          </div>
                          <div class="d-flex justify-content-between align-items-cente my-1">
                            <span class="text-dark-75 font-weight-bolder mr-2">Mandor:</span>
                            <a href="#" class="text-muted text-hover-primary">
                              <?php 
                              foreach ($key['mandor'] as $k => $v) {
                                echo $v." ,";
                              }
                               ?>
                            </a>
                          </div>
                          <div class="d-flex justify-content-between align-items-center">
                            <span class="text-danger font-weight-bolder mr-2">Budget:</span>
                            <span class="text-danger font-weight-bold"><?php echo (!empty($key['budget'])) ? $key['budget'] : 0 ?></span>
                          </div>
                        </div>
                        <!--end::Info-->
                        <a href="<?php echo base_url('index.php/project/detailProject/'.$key['id_proyek']) ?>" class="btn btn-block btn-sm btn-light-success font-weight-bolder text-uppercase py-4">DETAIL</a>
                      </div>
                      <!--end::Body-->
                    </div>
                <!--end:: Card-->
              </div>
            <?php endforeach ?>
            <!--end::Col-->
          </div>
          <!--end::Row-->
        </div>
  </div>
  <!--end::Entry-->
</div>
<!--end::Content-->
<!-- end of content -->