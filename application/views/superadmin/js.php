<script type="text/javascript">
	 $('.mandor').selectize({
      maxItems: 3,
      delimiter:',',
      create: true,
      sortField: 'text'
    });
    $('.admin').selectize({
      maxItems:3,
      delimiter:',',
      persist:false,
      create: function(input){
        return{
          value:input,
          text:input
        }
      }
    });


    function submitadm(x){
      if (x=='tambah') {
        $('#btn-tambah').show();
        $('#btn-ubah').hide();
        $('#modal-title').html('<b>Form Tambah Admin</b>');

        $("[name='username']").val('');
        $("[name='password']").val('');
        $("[name='nohp']").val('');
        $("[name='email']").val('');
        $('.hakakses').setValue("");

      }else{
        $('#btn-tambah').hide();
        $('#btn-ubah').show();
        $('#btn-ubah').attr('disabled',false);
        $('#btn-ubah').html('Ubah <i class="fa fa-paper-plane-o ml-1"></i>');
        $('#modal-title').html('<b>Form Ubah Admin</b>');

        $.ajax({
          type:"POST",
          data:'id='+x,
          url:'<?php echo base_url('index.php/users/getAdminById') ?>',
          dataType:'JSON',
          success: function(data){
            $("[name='username']").val(data[0].username);
            $("[name='password']").val(data[0].password);
            $("[name='password_lama']").val(data[0].password);
            $("[name='nohp']").val(data[0].no_hp);
            $("[name='email']").val(data[0].email);
            $("[name='id']").val(data[0].id_user);
            $("[name='hakakses']").val(data[0].hak_akses);
          // $('.hakakses').setValue(data[0].hak_akses);
        }
      })
      }
    }
    function submitmnd(x){
      if (x=='tambah') {
        $('#btn-tambah').show();
        $('#btn-ubah').hide();
        $('#modal-title').html('<b>Form Tambah Pimpro</b>');

        $("[name='username']").val('');
        $("[name='password']").val('');
        $("[name='nohp']").val('');
        $("[name='email']").val('');
        $('.hakakses').setValue("");

      }else{
        $('#btn-tambah').hide();
        $('#btn-ubah').show();
        $('#btn-ubah').attr('disabled',false);
        $('#btn-ubah').html('Ubah <i class="fa fa-paper-plane-o ml-1"></i>');
        $('#modal-title').html('<b>Form Ubah Pimpro</b>');

        $.ajax({
          type:"POST",
          data:'id='+x,
          url:'<?php echo base_url('index.php/users/getMandorById') ?>',
          dataType:'JSON',
          success: function(data){
            $("[name='username']").val(data[0].username);
            $("[name='password']").val(data[0].password);
            $("[name='password_lama']").val(data[0].password);
            $("[name='nohp']").val(data[0].no_hp);
            $("[name='email']").val(data[0].email);
            $("[name='id']").val(data[0].id_user);
            $("[name='hakakses']").val(data[0].hak_akses);
          // $('.hakakses').setValue(data[0].hak_akses);
        }
      })
      }
    }

     function submitspa(x){
      if (x=='tambah') {
        $('#btn-tambah').show();
        $('#btn-ubah').hide();
        $('#modal-title').html('<b>Form Tambah Owner</b>');

        $("[name='username']").val('');
        $("[name='password']").val('');
        $("[name='nohp']").val('');
        $("[name='email']").val('');
        $('.hakakses').setValue("");

      }else{
        $('#btn-tambah').hide();
        $('#btn-ubah').show();
        $('#btn-ubah').attr('disabled',false);
        $('#btn-ubah').html('Ubah <i class="fa fa-paper-plane-o ml-1"></i>');
        $('#modal-title').html('<b>Form Ubah Owner</b>');

        $.ajax({
          type:"POST",
          data:'id='+x,
          url:'<?php echo base_url('index.php/users/getSuperadminById') ?>',
          dataType:'JSON',
          success: function(data){
            $("[name='username']").val(data[0].username);
            $("[name='password']").val(data[0].password);
            $("[name='password_lama']").val(data[0].password);
            $("[name='nohp']").val(data[0].no_hp);
            $("[name='email']").val(data[0].email);
            $("[name='id']").val(data[0].id_user);
            $("[name='hakakses']").val(data[0].hak_akses);
          // $('.hakakses').setValue(data[0].hak_akses);
        }
      })
      }
    }

    function tambahuser(){
      if ($("[name='username']").val()=='') {
        $('#username_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='password']").val()==''){
        $('#password_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='hakakses']").val()=='Pilih hak akses . . .'){
        $('#hakakses_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='nohp']").val()==''){
        $('#nohp_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='email']").val()==''){
        $('#email_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else{
        $('#btn-tambah').html("<i class='fa fa-spinner fa-spin'></i> Saving...");
        $('#btn-tambah').attr('disabled',true);
        var formData = new FormData($('#form')[0]);

        $.ajax({
          type:'POST',
          data: formData,
          contentType: false,
          processData: false,
          url:'<?php echo base_url('index.php/users/addUser') ?>',
          dataType : 'JSON',
          success : function(hasil){
            console.log(hasil);
            if (hasil.status=='success') {
              $("#formadd").modal('hide');

              $("[name='username']").val('');
              $("[name='password']").val('');
              $("[name='password_lama']").val('');
              $("[name='nohp']").val('');
              $("[name='email']").val('');
              $("[name='hakakses']").val('');

              window.location.href = hasil.redirect_url;
            }
          }
        });
      }
    }

    function ubahuser(){
      if ($("[name='username']").val()=='') {
        $('#username_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='password']").val()==''){
        $('#password_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='hakakses']").val()=='Pilih hak akses . . .'){
        $('#hakakses_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='nohp']").val()==''){
        $('#nohp_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else if($("[name='email']").val()==''){
        $('#email_alert').text('Field ini tidak boleh kosong').fadeIn().delay(3000).fadeOut();
      }else{
        $('#btn-ubah').html("<i class='fa fa-spinner fa-spin'></i> Updating...");
        $('#btn-ubah').attr('disabled',true);
        var formData = new FormData($('#form')[0]);

        $.ajax({
          type:'POST',
          data: formData,
          contentType: false,
          processData: false,
          url:'<?php echo base_url('index.php/users/updateUser') ?>',
          dataType : 'JSON',
          success : function(hasil){
            if (hasil.status=='success') {
              $("#formadd").modal('hide');

              $("[name='username']").val('');
              $("[name='password']").val('');
              $("[name='password_lama']").val('');
              $("[name='nohp']").val('');
              $("[name='email']").val('');
              $("[name='hakakses']").val('');

              window.location.href = hasil.redirect_url;
            }
          }
        });
      }
    }
</script>

<!--end::Page Scripts-->
</body>
<!--end::Body-->
</html>