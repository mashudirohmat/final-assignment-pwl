<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container" style="margin-bottom: 40px;">

      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              Pimpro Management
              <hr style="height:2px;border-width:0;color:blue;background-color:blue;width:50px;text-align:left;margin-left:0">
              <div class="text-right">
                <a href="#formadd" class="btn btn-sm btn-primary text-right" onclick="submitmnd('tambah')" data-toggle="modal"><i class="fas fa-plus fa-sm text-right"></i>Tambah Pimpro</a>
              </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="tabel2">
                  <thead>
                    <tr>
                      <th>Hak akses</th>
                      <th>Nama</th>
                      <th>No HP</th>
                      <th>Email</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($this->data['mandor'] as $key): ?>
                      <tr>
                        <td><?php echo $key['hak_akses'] ?></td>
                        <td><?php echo $key['username'] ?></td>
                        <td><?php echo $key['no_hp'] ?></td>
                        <td><?php echo $key['email'] ?></td>
                        <td>
                          <a class="btn text-sm text-success" href="#formadd" onclick="submitmnd(<?php echo $key['id_user'] ?>)" data-toggle="modal">
                            <i class="fas fa-edit"></i>Edit
                          </a>
                          <a onClick="konfirmasiHapus('<?php echo site_url('index.php/users/deleteMandor/'.$key['id_user']) ?>')" href="#!" class="btn text-sm text-danger">
                            <i class="fas fa-trash"></i>Hapus
                          </a>
                        </td>
                      </tr>
                    <?php endforeach ?>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--end::Entry-->
</div>