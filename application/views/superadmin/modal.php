<!-- MODAL ADD USER -->
<div class="modal fade" id="formadd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="modal-title"></h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body text-xs">
        <form actionrole="form" id="form" class="form-xs" onSubmit="return false" enctype="multipart/form-data" id="form">
          <input type="hidden" name="id" value="">
          <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" id="username" placeholder="Username ..." required="" class="form-control form-sm">
            <p id="username_alert" style="color: red"></p>
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="password" name="password" id="password" placeholder="Password ..." required="" class="form-control form-sm">
            <input type="hidden" name="password_lama">
            <p id="password_alert" style="color: red"></p>
          </div>
          <div class="form-group">
            <label>Hak akses</label>
            <select class="form-control hakakses" name="hakakses" id="hakakses">
              <option selected="" disabled="" value="">Pilih hak akses . . .</option>
              <option value="owner">Owner</option>
              <option value="admin">Admin</option>
              <option value="pimpro">Pimpro</option>
            </select>
            <p id="hakakses_alert" style="color: red"></p>
          </div>
          <div class="form-group">
            <label>No HP</label>
            <input type="text" name="nohp" id="nohp" placeholder="No HP ..." required="" class="form-control form-sm">
            <p id="nohp_alert" style="color: red"></p>
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="email" name="email" id="email" placeholder="Email ..." required="" class="form-control form-sm">
            <p id="email_alert" style="color: red"></p>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
          <a class="btn btn-primary" id="btn-ubah" onclick="ubahuser()" href="#!">Ubah</a>
          <a class="btn btn-primary" id="btn-tambah" onclick="tambahuser()" href="#!">Simpan</a>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- END OF MODAL ADD USER -->