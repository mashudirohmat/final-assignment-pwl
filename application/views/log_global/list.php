<div class="container">
      <!-- pemasukan -->
      <div class="card" style="margin-bottom: 50px">
        <div class="card-header">
          <div class="row">
            <div class="col-md-2">
              <div class="text-left">
                <p>Log Global</p>
                <hr style="height:2px;border-width:0;color:blue;background-color:blue;width:50px;text-align:left;margin-left:0">
              </div>
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
              <div class="text-right">
              </div>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="table-responsive">
              <table class="table table-bordered" id="tabel1">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Nama Pegawai</th>
                    <th>Hak akses</th>
                    <th>Jenis Log</th>
                    <th>Tanggal</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $i=1;
                  foreach ($this->data['log_global'] as $key): ?>
                  <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo $key['nama_pegawai'] ?></td>
                    <td><?php echo $key['hak_akses'] ?></td>
                    <td><?php echo $key['jenis_log'] ?></td>
                    <td><?php echo $key['tanggal'] ?></td>
                  </tr>
                  <?php $i++; endforeach ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>