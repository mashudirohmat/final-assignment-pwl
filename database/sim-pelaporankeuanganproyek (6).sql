-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 19, 2021 at 06:16 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sim-pelaporankeuanganproyek`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_user` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `hak_akses` varchar(10) NOT NULL,
  `is_aktif` int(1) NOT NULL DEFAULT '1',
  `no_hp` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_user`, `username`, `password`, `hak_akses`, `is_aktif`, `no_hp`, `email`) VALUES
(1, 'admin1', 'e00cf25ad42683b3df678c61f42c6bda', 'admin', 1, '12345', 'admin@gmail.com'),
(2, 'admin2', 'c84258e9c39059a89ab77d846ddab909', 'admin', 1, '123', 'mashudi@gmail.com'),
(3, 'bambang', 'a9711cbb2e3c2d5fc97a63e45bbe5076', 'admin', 1, '123456', 'bambang@gmail.com'),
(4, 'admin33', '31f4a4b4b228d4c42404f1a5e7f5de72', 'admin', 1, '123', 'admin3@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `aksi_request_pengeluaran`
--

CREATE TABLE `aksi_request_pengeluaran` (
  `id_request` int(11) NOT NULL,
  `id_admin` int(11) NOT NULL,
  `aksi` enum('edit','hapus') NOT NULL,
  `id_proyek` int(11) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `budget`
--

CREATE TABLE `budget` (
  `id_budget` int(11) NOT NULL,
  `id_proyek` int(11) NOT NULL,
  `pos_legal` int(20) NOT NULL,
  `pos_fasumfasos` int(20) NOT NULL,
  `pos_operasional` int(20) NOT NULL,
  `pos_promosi` int(20) NOT NULL,
  `pos_bangunan` int(20) NOT NULL,
  `pos_pajak` int(20) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `budget`
--

INSERT INTO `budget` (`id_budget`, `id_proyek`, `pos_legal`, `pos_fasumfasos`, `pos_operasional`, `pos_promosi`, `pos_bangunan`, `pos_pajak`, `tanggal`) VALUES
(1, 9, 200000, 400000, 200000, 200000, 200000, 200000, '2020-03-06 08:48:08'),
(2, 12, 400000000, 400000000, 400000000, 400000000, 400000000, 400000000, '2020-03-06 10:40:42'),
(3, 15, 300000000, 200000000, 100000000, 100000000, 100000000, 100000000, '2021-01-05 07:13:13'),
(4, 19, 300000000, 200000000, 200000000, 200000000, 200000000, 100000000, '2020-03-25 06:11:05'),
(5, 20, 200000000, 200000000, 200000000, 200000000, 200000000, 200000000, '2020-07-07 03:37:56'),
(6, 17, 100000000, 100000000, 100000000, 100000000, 100000000, 100000000, '2020-07-07 12:12:36'),
(7, 18, 200000000, 200000000, 200000000, 200000000, 200000000, 200000000, '2020-07-08 00:40:10'),
(9, 11, 2, 2, 2, 0, 2, 2, '2020-07-27 15:33:18'),
(10, 16, 800000000, 600000000, 700000000, 500000000, 250000000, 300000000, '2020-07-30 05:11:26'),
(11, 22, 300000000, 300000000, 400000000, 400000000, 400000000, 500000000, '2021-01-19 04:16:29'),
(12, 23, 200000000, 200000000, 200000000, 200000000, 200000000, 200000000, '2021-01-19 04:20:49');

-- --------------------------------------------------------

--
-- Table structure for table `direksi`
--

CREATE TABLE `direksi` (
  `id_user` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `hak_akses` varchar(10) NOT NULL,
  `is_aktif` int(1) NOT NULL DEFAULT '1',
  `no_hp` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `direksi`
--

INSERT INTO `direksi` (`id_user`, `username`, `password`, `hak_akses`, `is_aktif`, `no_hp`, `email`) VALUES
(7, 'mashudi', '47517a601e007a98e01d6b59d7e29061', 'owner', 1, '123456', 'mashudirohmat.mr@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id_log` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `nama_pegawai` varchar(50) NOT NULL,
  `hak_akses` varchar(10) NOT NULL,
  `id_proyek` int(11) DEFAULT NULL,
  `jenis_log` varchar(250) DEFAULT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id_log`, `id_pegawai`, `nama_pegawai`, `hak_akses`, `id_proyek`, `jenis_log`, `tanggal`) VALUES
(3, 0, '', '', 15, 'ubah', '2020-04-11 03:25:21'),
(4, 0, '', '', 15, 'hapus', '2020-04-11 03:26:37'),
(5, 0, '', '', 20, 'ubah', '2020-06-28 13:13:31'),
(6, 0, '', '', 15, 'ubah', '2020-07-08 02:42:53'),
(7, 7, 'dimas', 'owner', 15, 'Tambah data pengeluaran', '2020-07-25 14:37:36'),
(8, 7, 'dimas', 'owner', 15, 'Update data pengeluaran', '2020-07-26 02:42:57'),
(9, 7, 'dimas', 'owner', 15, 'Update data pengeluaran', '2020-07-26 02:43:57'),
(10, 7, 'dimas', 'owner', 15, 'Tambah data pengeluaran', '2020-07-27 04:21:22'),
(11, 7, 'dimas', 'owner', 15, 'Update data pengeluaran', '2020-07-27 04:21:45'),
(12, 7, 'dimas', 'owner', 20, 'Tambah termin: rumah makan, pemilik: ANTO', '2020-07-27 15:58:27'),
(13, 7, 'dimas', 'owner', 20, 'Tambah termin: rumah makan, pemilik: ANTO', '2020-07-27 16:13:16'),
(14, 7, 'dimas', 'owner', 15, 'Update data admin admin1', '2020-07-28 03:51:40'),
(15, 7, 'dimas', 'owner', 15, 'Logout', '2020-07-28 03:51:48'),
(16, 7, 'dimas', 'owner', 15, 'Login', '2020-07-28 03:52:13'),
(17, 7, 'dimas', 'owner', 15, 'Update data admin admin1', '2020-07-28 03:52:24'),
(18, 7, 'dimas', 'owner', 15, 'Logout', '2020-07-28 03:52:31'),
(19, 1, 'admin1', 'admin', 15, 'Login', '2020-07-28 03:52:37'),
(20, 1, 'admin1', 'admin', 15, 'Logout', '2020-07-28 03:52:43'),
(21, 7, 'dimas', 'owner', 15, 'Login', '2020-07-28 03:52:48'),
(22, 7, 'dimas', 'owner', 15, 'Update data admin admin1', '2020-07-28 03:53:08'),
(23, 7, 'dimas', 'owner', 15, 'Logout', '2020-07-28 03:53:13'),
(24, 7, 'dimas', 'owner', 15, 'Login', '2020-07-28 03:53:25'),
(25, 7, 'dimas', 'owner', NULL, 'Logout', '2020-07-28 04:10:24'),
(26, 7, 'dimas', 'owner', NULL, 'Login', '2020-07-28 04:10:36'),
(27, 7, 'dimas', 'owner', NULL, 'Update data admin admin1', '2020-07-28 04:17:59'),
(28, 7, 'dimas', 'owner', NULL, 'Logout', '2020-07-28 04:18:02'),
(29, 7, 'dimas', 'owner', NULL, 'Login', '2020-07-28 04:18:23'),
(30, 7, 'dimas', 'owner', NULL, 'Update data admin admin1', '2020-07-28 04:21:02'),
(31, 7, 'dimas', 'owner', NULL, 'Update data admin admin1', '2020-07-28 04:21:08'),
(32, 7, 'dimas', 'owner', NULL, 'Logout', '2020-07-28 04:21:12'),
(33, 1, 'admin1', 'admin', NULL, 'Login', '2020-07-28 04:21:19'),
(34, 1, 'admin1', 'admin', NULL, 'Logout', '2020-07-28 04:21:24'),
(35, 7, 'dimas', 'owner', NULL, 'Login', '2020-07-28 04:21:28'),
(36, 7, 'dimas', 'owner', NULL, 'Login', '2020-07-29 00:54:58'),
(37, 7, 'dimas', 'owner', 15, 'Export laporan pajak', '2020-07-29 03:58:14'),
(38, 7, 'dimas', 'owner', 15, 'Export laporan pajak', '2020-07-29 04:06:12'),
(39, 7, 'dimas', 'owner', 15, 'Export laporan pajak', '2020-07-29 04:08:20'),
(40, 7, 'dimas', 'owner', 15, 'Export laporan pajak', '2020-07-29 04:12:02'),
(41, 7, 'dimas', 'owner', 15, 'Export laporan pajak', '2020-07-29 04:14:13'),
(42, 7, 'dimas', 'owner', 15, 'Export laporan pajak', '2020-07-29 04:14:46'),
(43, 7, 'dimas', 'owner', 15, 'Export laporan pajak', '2020-07-29 04:15:25'),
(44, 7, 'dimas', 'owner', 15, 'Export laporan bulanan', '2020-07-29 05:24:28'),
(45, 7, 'dimas', 'owner', 15, 'Export laporan bulanan', '2020-07-29 05:25:05'),
(46, 7, 'dimas', 'owner', 15, 'Export laporan tahunan', '2020-07-29 05:29:24'),
(47, 7, 'dimas', 'owner', 15, 'Export laporan tahunan', '2020-07-29 05:30:29'),
(48, 7, 'dimas', 'owner', 15, 'Export laporan tahunan', '2020-07-29 05:31:50'),
(49, 7, 'dimas', 'owner', 15, 'Export laporan tahunan', '2020-07-29 05:32:43'),
(50, 7, 'dimas', 'owner', NULL, 'Login', '2020-07-29 08:57:21'),
(51, 7, 'dimas', 'owner', 15, 'Export', '2020-07-29 09:15:56'),
(52, 7, 'dimas', 'owner', 15, 'Export', '2020-07-29 09:16:51'),
(53, 7, 'dimas', 'owner', 15, 'Export', '2020-07-29 09:18:07'),
(54, 7, 'dimas', 'owner', 15, 'Export', '2020-07-29 09:19:00'),
(55, 7, 'dimas', 'owner', 15, 'Export', '2020-07-29 09:20:02'),
(56, 7, 'dimas', 'owner', 15, 'Export', '2020-07-29 09:20:58'),
(57, 7, 'dimas', 'owner', 12, 'Arsip project', '2020-07-29 10:31:40'),
(58, 7, 'dimas', 'owner', 15, 'Update Project', '2020-07-29 10:40:40'),
(59, 7, 'dimas', 'owner', 15, 'Update Project', '2020-07-29 10:42:54'),
(60, 7, 'dimas', 'owner', 15, 'Update Project', '2020-07-29 10:44:33'),
(61, 7, 'dimas', 'owner', 15, 'Update Project', '2020-07-29 10:44:43'),
(62, 7, 'dimas', 'owner', 15, 'Update Project', '2020-07-29 10:47:14'),
(63, 7, 'dimas', 'owner', 15, 'Update Project', '2020-07-29 10:47:50'),
(64, 7, 'dimas', 'owner', 15, 'Update Project', '2020-07-29 10:50:00'),
(65, 7, 'dimas', 'owner', 15, 'Update POS', '2020-07-29 10:50:24'),
(66, 7, 'dimas', 'owner', 15, 'Tambah termin: Rumah Diana 2, pemilik:Ari', '2020-07-29 10:50:45'),
(67, 7, 'dimas', 'owner', 15, 'Ubah termin: Rumah Diana 2, pemilik: Ari', '2020-07-29 10:51:13'),
(68, 7, 'dimas', 'owner', 15, 'Archive termin: Rumah Diana 2, pemilik: Ari', '2020-07-29 10:53:33'),
(69, 7, 'dimas', 'owner', 15, 'Add pengeluaran', '2020-07-29 10:54:07'),
(70, 7, 'dimas', 'owner', 15, 'Update data pengeluaran', '2020-07-29 10:54:17'),
(71, 7, 'dimas', 'owner', 15, 'Update data pengeluaran', '2020-07-29 10:54:23'),
(72, 7, 'dimas', 'owner', 15, 'Archive data pengeluaran', '2020-07-29 10:55:59'),
(73, 7, 'dimas', 'owner', NULL, 'Logout', '2020-07-29 10:59:56'),
(74, 1, 'admin1', 'admin', NULL, 'Login', '2020-07-29 11:00:03'),
(75, 7, 'dimas', 'owner', NULL, 'Login', '2020-07-30 02:20:54'),
(76, 7, 'dimas', 'owner', 20, 'Export laporan proyeksi', '2020-07-30 03:50:22'),
(77, 7, 'dimas', 'owner', 20, 'Export laporan proyeksi', '2020-07-30 03:52:10'),
(78, 7, 'dimas', 'owner', 20, 'Export laporan proyeksi', '2020-07-30 03:53:23'),
(79, 7, 'dimas', 'owner', 20, 'Export laporan proyeksi', '2020-07-30 03:54:59'),
(80, 7, 'dimas', 'owner', 20, 'Export laporan proyeksi', '2020-07-30 03:56:14'),
(81, 7, 'dimas', 'owner', 20, 'Export laporan proyeksi', '2020-07-30 03:56:44'),
(82, 7, 'dimas', 'owner', 20, 'Export laporan proyeksi', '2020-07-30 03:57:16'),
(83, 7, 'dimas', 'owner', 16, 'Set POS', '2020-07-30 05:10:16'),
(84, 7, 'dimas', 'owner', 16, 'Update POS', '2020-07-30 05:10:35'),
(85, 7, 'dimas', 'owner', 16, 'Update POS', '2020-07-30 05:11:28'),
(86, 7, 'dimas', 'owner', 16, 'Add Rumah', '2020-07-30 05:12:06'),
(87, 7, 'dimas', 'owner', 16, 'Update Rumah', '2020-07-30 05:12:19'),
(88, 7, 'dimas', 'owner', 16, 'Transaksi pembelian rumah', '2020-07-30 05:13:04'),
(89, 7, 'dimas', 'owner', 16, 'Tambah termin: RUMAH-A, pemilik:joe', '2020-07-30 05:13:43'),
(90, 7, 'dimas', 'owner', 16, 'Tambah termin: RUMAH-A, pemilik:joe', '2020-07-30 05:14:18'),
(91, 7, 'dimas', 'owner', 16, 'Ubah termin: RUMAH-A, pemilik: joe', '2020-07-30 05:15:06'),
(92, 7, 'dimas', 'owner', 16, 'Ubah termin: RUMAH-A, pemilik: joe', '2020-07-30 05:15:36'),
(93, 7, 'dimas', 'owner', 16, 'Ubah termin: RUMAH-A, pemilik: joe', '2020-07-30 05:15:44'),
(94, 7, 'dimas', 'owner', 16, 'Ubah termin: RUMAH-A, pemilik: joe', '2020-07-30 05:16:02'),
(95, 7, 'dimas', 'owner', 16, 'Ubah termin: RUMAH-A, pemilik: joe', '2020-07-30 05:18:16'),
(96, 7, 'dimas', 'owner', 16, 'Ubah termin: RUMAH-A, pemilik: joe', '2020-07-30 05:21:11'),
(97, 7, 'dimas', 'owner', 16, 'Ubah termin: RUMAH-A, pemilik: joe', '2020-07-30 05:21:48'),
(98, 7, 'dimas', 'owner', 16, 'Ubah termin: RUMAH-A, pemilik: joe', '2020-07-30 05:22:16'),
(99, 7, 'dimas', 'owner', 16, 'Ubah termin: RUMAH-A, pemilik: joe', '2020-07-30 05:23:17'),
(100, 7, 'dimas', 'owner', 16, 'Ubah termin: RUMAH-A, pemilik: joe', '2020-07-30 05:23:27'),
(101, 7, 'dimas', 'owner', 16, 'Ubah termin: RUMAH-A, pemilik: joe', '2020-07-30 05:25:09'),
(102, 7, 'dimas', 'owner', NULL, 'Login', '2020-07-30 06:28:39'),
(103, 1, 'mandor1', 'pimpro', NULL, 'Login', '2020-07-30 08:07:41'),
(104, 1, 'mandor1', 'pimpro', NULL, 'Logout', '2020-07-30 08:13:12'),
(105, 7, 'dimas', 'owner', NULL, 'Login', '2020-07-30 08:13:16'),
(106, 7, 'dimas', 'owner', NULL, 'Logout', '2020-07-30 08:15:42'),
(107, 1, 'mandor1', 'pimpro', NULL, 'Login', '2020-07-30 08:15:52'),
(108, 7, 'dimas', 'owner', NULL, 'Login', '2020-08-04 01:19:58'),
(109, 7, 'dimas', 'owner', 15, 'Update data pengeluaran', '2020-08-04 01:21:00'),
(110, 7, 'dimas', 'owner', NULL, 'Logout', '2020-08-04 01:21:09'),
(111, 1, 'admin1', 'admin', NULL, 'Login', '2020-08-04 01:21:16'),
(112, 1, 'admin1', 'admin', NULL, 'Logout', '2020-08-04 02:55:40'),
(113, 1, 'mandor1', 'pimpro', NULL, 'Login', '2020-08-04 02:55:48'),
(114, 1, 'mandor1', 'pimpro', 15, 'Update data pengeluaran', '2020-08-04 03:05:39'),
(115, 1, 'mandor1', 'pimpro', 15, 'Update data pengeluaran', '2020-08-04 03:05:55'),
(116, 1, 'mandor1', 'pimpro', 15, 'Update data pengeluaran', '2020-08-04 03:06:31'),
(117, 1, 'mandor1', 'pimpro', NULL, 'Logout', '2020-08-04 03:19:21'),
(118, 7, 'dimas', 'owner', NULL, 'Login', '2020-08-04 03:19:28'),
(119, 7, 'dimas', 'owner', NULL, 'Logout', '2020-08-04 03:40:44'),
(120, 1, 'admin1', 'admin', NULL, 'Login', '2020-08-04 03:40:56'),
(121, 1, 'admin1', 'admin', NULL, 'Login', '2020-08-04 03:42:14'),
(122, 1, 'admin1', 'admin', NULL, 'Logout', '2020-08-04 03:43:44'),
(123, 1, 'mandor1', 'pimpro', NULL, 'Login', '2020-08-04 03:44:00'),
(124, 1, 'mandor1', 'pimpro', 15, 'Update data pengeluaran', '2020-08-04 03:44:21'),
(125, 1, 'mandor1', 'pimpro', 15, 'Archive data pengeluaran', '2020-08-04 03:45:07'),
(126, 1, 'mandor1', 'pimpro', NULL, 'Logout', '2020-08-04 03:48:13'),
(127, 1, 'admin1', 'admin', NULL, 'Login', '2020-08-04 03:48:25'),
(128, 7, 'dimas', 'owner', NULL, 'Login', '2020-08-04 15:20:48'),
(129, 7, 'dimas', 'owner', NULL, 'Login', '2020-08-05 02:27:31'),
(130, 7, 'dimas', 'owner', NULL, 'Login', '2020-08-11 01:14:36'),
(131, 7, 'dimas', 'owner', 15, 'Transaksi pembelian rumah', '2020-08-11 01:24:24'),
(132, 7, 'dimas', 'owner', 15, 'Tambah kontrak termin: RUMAH-A, pemilik:Patrich Wanggai', '2020-08-11 02:50:07'),
(133, 7, 'dimas', 'owner', 15, 'Tambah termin: RUMAH-A, pemilik:Patrich Wanggai', '2020-08-11 02:51:11'),
(134, 7, 'dimas', 'owner', 15, 'Ubah kontrak termin: RUMAH-A, pemilik: Patrich Wanggai', '2020-08-11 03:01:56'),
(135, 7, 'dimas', 'owner', 15, 'Tambah kontrak termin: Rumah Diana 2, pemilik:Ari', '2020-08-11 03:23:20'),
(136, 7, 'dimas', 'owner', NULL, 'Login', '2020-08-11 06:14:58'),
(137, 7, 'dimas', 'owner', 15, 'Export laporan proyeksi', '2020-08-11 06:15:12'),
(138, 7, 'dimas', 'owner', NULL, 'Login', '2020-08-12 02:36:20'),
(139, 7, 'dimas', 'owner', NULL, 'Login', '2020-08-12 04:30:39'),
(140, 7, 'dimas', 'owner', NULL, 'Login', '2020-09-10 01:36:34'),
(141, 7, 'dimas', 'owner', 15, 'Add Rumah', '2020-09-10 01:37:19'),
(142, 7, 'dimas', 'owner', 15, 'Transaksi pembelian rumah', '2020-09-10 01:37:53'),
(143, 7, 'dimas', 'owner', 15, 'Tambah kontrak termin: Rumah Dahlia, pemilik:Mustofa', '2020-09-10 01:39:44'),
(144, 7, 'dimas', 'owner', 15, 'Tambah kontrak termin: Rumah Dahlia, pemilik:Mustofa', '2020-09-10 01:40:12'),
(145, 7, 'dimas', 'owner', NULL, 'Login', '2020-09-17 02:22:24'),
(146, 7, 'dimas', 'owner', 15, 'Export laporan proyeksi', '2020-09-17 02:26:20'),
(147, 7, 'dimas', 'owner', 15, 'Export laporan proyeksi', '2020-09-17 02:26:29'),
(148, 7, 'dimas', 'owner', NULL, 'Login', '2020-09-23 01:47:08'),
(149, 7, 'dimas', 'owner', 15, 'Export laporan tahunan', '2020-09-23 02:20:28'),
(150, 7, 'dimas', 'owner', 15, 'Export laporan tahunan', '2020-09-23 02:20:33'),
(151, 7, 'dimas', 'owner', NULL, 'Login', '2020-10-19 06:43:42'),
(152, 7, 'dimas', 'owner', NULL, 'Login', '2020-10-19 09:16:17'),
(153, 7, 'dimas', 'owner', NULL, 'Login', '2020-10-19 15:17:49'),
(154, 7, 'dimas', 'owner', NULL, 'Login', '2020-10-21 16:29:19'),
(155, 7, 'dimas', 'owner', NULL, 'Login', '2020-10-22 00:23:22'),
(156, 7, 'dimas', 'owner', NULL, 'Login', '2020-10-22 01:16:06'),
(157, 7, 'dimas', 'owner', NULL, 'Logout', '2020-10-22 02:53:03'),
(158, 7, 'dimas', 'owner', NULL, 'Login', '2020-10-22 02:53:10'),
(159, 7, 'dimas', 'owner', NULL, 'Login', '2020-10-24 01:18:16'),
(160, 7, 'dimas', 'owner', NULL, 'Logout', '2020-10-24 01:21:29'),
(161, 1, 'admin1', 'admin', NULL, 'Login', '2020-10-24 01:21:37'),
(162, 1, 'admin1', 'admin', NULL, 'Logout', '2020-10-24 01:32:38'),
(163, 7, 'dimas', 'owner', NULL, 'Login', '2020-10-24 01:32:43'),
(164, 7, 'dimas', 'owner', 20, 'Export', '2020-10-24 01:44:28'),
(165, 7, 'dimas', 'owner', NULL, 'Logout', '2020-10-24 02:03:48'),
(166, 7, 'dimas', 'owner', NULL, 'Login', '2020-10-24 02:18:13'),
(167, 7, 'dimas', 'owner', NULL, 'Login', '2020-10-28 08:11:17'),
(168, 7, 'dimas', 'owner', NULL, 'Login', '2020-10-29 05:30:40'),
(169, 7, 'dimas', 'owner', 18, 'Add Rumah', '2020-10-29 07:53:36'),
(170, 7, 'dimas', 'owner', NULL, 'Login', '2020-10-29 14:35:08'),
(171, 7, 'dimas', 'owner', NULL, 'Login', '2020-11-04 08:57:56'),
(172, 7, 'dimas', 'owner', NULL, 'Login', '2020-11-05 00:48:43'),
(173, 7, 'dimas', 'owner', NULL, 'Login', '2020-11-05 05:58:10'),
(174, 7, 'dimas', 'owner', NULL, 'Add Project', '2020-11-05 08:08:56'),
(175, 7, 'dimas', 'owner', NULL, 'Login', '2020-11-05 15:26:28'),
(176, 7, 'dimas', 'owner', 21, 'Add Rumah', '2020-11-05 16:51:35'),
(177, 7, 'dimas', 'owner', 21, 'Add Rumah', '2020-11-05 17:29:46'),
(178, 7, 'dimas', 'owner', 21, 'Transaksi pembelian rumah', '2020-11-05 17:46:16'),
(179, 7, 'dimas', 'owner', NULL, 'Login', '2020-11-07 02:03:35'),
(180, 7, 'dimas', 'owner', NULL, 'Login', '2020-11-07 09:32:18'),
(181, 7, 'dimas', 'owner', NULL, 'Login', '2020-11-10 02:55:06'),
(182, 7, 'dimas', 'owner', NULL, 'Login', '2020-12-22 09:38:15'),
(183, 7, 'dimas', 'owner', NULL, 'Add Project', '2020-12-22 09:39:35'),
(184, 7, 'dimas', 'owner', 22, 'Set POS', '2020-12-22 09:40:41'),
(185, 7, 'dimas', 'owner', 22, 'Add Rumah', '2020-12-22 09:41:18'),
(186, 7, 'dimas', 'owner', 22, 'Transaksi pembelian rumah', '2020-12-22 09:42:06'),
(187, 7, 'dimas', 'owner', NULL, 'Login', '2020-12-27 15:37:35'),
(188, 7, 'dimas', 'owner', NULL, 'Login', '2021-01-05 01:33:40'),
(189, 7, 'dimas', 'owner', 21, 'Arsip project', '2021-01-05 01:35:12'),
(190, 7, 'dimas', 'owner', NULL, 'Logout', '2021-01-05 02:44:09'),
(191, 1, 'mandor1', 'pimpro', NULL, 'Login', '2021-01-05 02:44:17'),
(192, 1, 'mandor1', 'pimpro', NULL, 'Logout', '2021-01-05 02:46:01'),
(193, 1, 'admin1', 'admin', NULL, 'Login', '2021-01-05 02:46:14'),
(194, 1, 'admin1', 'admin', NULL, 'Logout', '2021-01-05 02:46:30'),
(195, 1, 'mandor1', 'pimpro', NULL, 'Login', '2021-01-05 02:46:37'),
(196, 1, 'mandor1', 'pimpro', NULL, 'Logout', '2021-01-05 02:47:04'),
(197, 1, 'admin1', 'admin', NULL, 'Login', '2021-01-05 02:47:10'),
(198, 1, 'admin1', 'admin', NULL, 'Logout', '2021-01-05 02:47:26'),
(199, 7, 'dimas', 'owner', NULL, 'Login', '2021-01-05 02:47:30'),
(200, 7, 'dimas', 'owner', NULL, 'Add Project', '2021-01-05 02:51:55'),
(201, 7, 'dimas', 'owner', NULL, 'Logout', '2021-01-05 02:55:23'),
(202, 1, 'mandor1', 'pimpro', NULL, 'Login', '2021-01-05 02:55:37'),
(203, 1, 'mandor1', 'pimpro', NULL, 'Logout', '2021-01-05 02:56:38'),
(204, 1, 'admin1', 'admin', NULL, 'Login', '2021-01-05 02:56:49'),
(205, 1, 'admin1', 'admin', NULL, 'Logout', '2021-01-05 03:03:23'),
(206, 7, 'dimas', 'owner', NULL, 'Login', '2021-01-05 03:03:31'),
(207, 7, 'dimas', 'owner', 22, 'Update POS', '2021-01-05 03:15:26'),
(208, 7, 'dimas', 'owner', NULL, 'Logout', '2021-01-05 06:10:17'),
(209, 7, 'dimas', 'owner', NULL, 'Login', '2021-01-05 06:17:28'),
(210, 7, 'dimas', 'owner', 22, 'Add Rumah', '2021-01-05 06:23:10'),
(211, 7, 'dimas', 'owner', 15, 'Add pengeluaran', '2021-01-05 06:47:10'),
(212, 7, 'dimas', 'owner', NULL, 'Logout', '2021-01-05 07:09:46'),
(213, 7, 'dimas', 'owner', NULL, 'Login', '2021-01-05 07:10:00'),
(214, 7, 'dimas', 'owner', NULL, 'Add Project', '2021-01-05 07:10:55'),
(215, 7, 'dimas', 'owner', 15, 'Update POS', '2021-01-05 07:13:13'),
(216, 7, 'dimas', 'owner', 15, 'Add Rumah', '2021-01-05 07:13:47'),
(217, 7, 'dimas', 'owner', 15, 'Transaksi pembelian rumah', '2021-01-05 07:14:32'),
(218, 7, 'dimas', 'owner', 15, 'Tambah kontrak termin: RUMAH-B, pemilik:Gustu Maulana', '2021-01-05 07:15:23'),
(219, 7, 'dimas', 'owner', 15, 'Tambah termin: RUMAH-B, pemilik:Gustu Maulana', '2021-01-05 07:16:11'),
(220, 7, 'dimas', 'owner', 15, 'Add pengeluaran', '2021-01-05 07:18:05'),
(221, 7, 'dimas', 'owner', NULL, 'Login', '2021-01-19 04:15:51'),
(222, 7, 'dimas', 'owner', 22, 'Update POS', '2021-01-19 04:16:29'),
(223, 7, 'dimas', 'owner', 23, 'Set POS', '2021-01-19 04:20:49'),
(224, 7, 'dimas', 'owner', 23, 'Add pengeluaran', '2021-01-19 04:21:09'),
(225, 7, 'dimas', 'owner', 23, 'Add pengeluaran', '2021-01-19 04:21:17'),
(226, 7, 'dimas', 'owner', 23, 'Add pengeluaran', '2021-01-19 04:22:45'),
(227, 7, 'dimas', 'owner', 23, 'Add pengeluaran', '2021-01-19 04:23:06'),
(228, 7, 'dimas', 'owner', 23, 'Add pengeluaran', '2021-01-19 04:24:07'),
(229, 7, 'dimas', 'owner', 23, 'Add pengeluaran', '2021-01-19 04:52:02'),
(230, 7, 'dimas', 'owner', 23, 'Add pengeluaran', '2021-01-19 04:52:26'),
(231, 7, 'dimas', 'owner', 23, 'Add pengeluaran', '2021-01-19 05:02:14'),
(232, 7, 'dimas', 'owner', NULL, 'Update data owner mashudi', '2021-01-19 05:15:06'),
(233, 7, 'dimas', 'owner', NULL, 'Logout', '2021-01-19 05:15:24'),
(234, 7, 'mashudi', 'owner', NULL, 'Login', '2021-01-19 05:15:36');

-- --------------------------------------------------------

--
-- Table structure for table `mandor`
--

CREATE TABLE `mandor` (
  `id_user` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `hak_akses` varchar(10) NOT NULL,
  `is_aktif` int(1) NOT NULL DEFAULT '1',
  `no_hp` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mandor`
--

INSERT INTO `mandor` (`id_user`, `username`, `password`, `hak_akses`, `is_aktif`, `no_hp`, `email`) VALUES
(1, 'mandor1', '3653dce7642e6fb7eeea3bc1f1121d5a', 'pimpro', 1, '123', 'mandor@gmail.com'),
(2, 'mandor2', 'mandor2', 'pimpro', 1, '123', 'diams@gmail.com'),
(3, 'butet', '256561095799c4191a8995893629edbf', 'pimpro', 1, '12345', 'mukaweb4381@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `pemasukan`
--

CREATE TABLE `pemasukan` (
  `id_pemasukan` int(11) NOT NULL,
  `id_proyek` int(11) NOT NULL,
  `id_transaksi` int(11) NOT NULL,
  `id_rumah` int(11) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tanggal_pemasukan` date NOT NULL,
  `is_archive` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemasukan`
--

INSERT INTO `pemasukan` (`id_pemasukan`, `id_proyek`, `id_transaksi`, `id_rumah`, `keterangan`, `jumlah`, `tanggal_pemasukan`, `is_archive`) VALUES
(1, 15, 1, 1, 'DP', 6000000, '2020-04-06', 0),
(2, 15, 1, 1, 'Cicilan Pertama', 3500000, '2020-04-09', 0),
(3, 20, 2, 5, 'KJH', 50000000, '2020-06-22', 0),
(4, 20, 2, 5, 'cicilan 1', 20000, '2020-06-28', 0),
(5, 15, 1, 1, 'cicilan ke 4', 20000000, '2020-07-04', 0),
(6, 15, 1, 1, 'cicilan ke 5', 20000000, '2020-07-04', 0),
(7, 15, 1, 1, 'pelunasan', 200500000, '2020-07-04', 0),
(8, 20, 2, 5, 'cicilan ke 4', 2000000, '2020-07-04', 0),
(9, 20, 2, 5, 'cicilan ke 5', 4000000, '2020-07-07', 0),
(10, 15, 3, 4, 'termin1', 2000000, '2020-07-07', 0),
(11, 17, 4, 6, 'keterangan', 6000000, '2020-07-07', 0),
(12, 17, 4, 6, 'keterangan', 6000000, '2020-07-07', 0),
(15, 19, 5, 8, 'Termin 1', 2000000, '2020-06-01', 0),
(16, 18, 6, 9, 'Termin1', 2000000, '2020-07-08', 0),
(17, 18, 6, 9, 'termin2', 3000000, '2020-07-08', 0),
(18, 20, 2, 5, 'asas', 232311104, '2020-07-27', 0),
(19, 20, 2, 5, 'ini keterangan', 2323, '2020-07-27', 0),
(20, 15, 3, 4, 'cicilan ke 5', 1500000, '2020-07-29', 1),
(21, 16, 8, 12, 'zxxz', 2222222, '2020-07-30', 0),
(22, 16, 8, 12, 'keterangan', 2222222, '2020-07-30', 0),
(23, 15, 9, 11, 'contoh', 2222222, '2020-08-11', 0),
(24, 15, 13, 19, 'termin1', 20000000, '2021-01-05', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran`
--

CREATE TABLE `pengeluaran` (
  `id_pengeluaran` int(11) NOT NULL,
  `id_proyek` int(11) NOT NULL,
  `no_nota` varchar(50) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `jenis_pengeluaran` varchar(50) NOT NULL,
  `pos_legal` int(20) DEFAULT '0',
  `pos_fasumfasos` int(20) DEFAULT '0',
  `pos_operasional` int(20) DEFAULT '0',
  `pos_promosi` int(20) DEFAULT '0',
  `pos_bangunan` int(20) DEFAULT '0',
  `pos_pajak` int(20) DEFAULT '0',
  `is_archive` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengeluaran`
--

INSERT INTO `pengeluaran` (`id_pengeluaran`, `id_proyek`, `no_nota`, `tgl_transaksi`, `jenis_pengeluaran`, `pos_legal`, `pos_fasumfasos`, `pos_operasional`, `pos_promosi`, `pos_bangunan`, `pos_pajak`, `is_archive`) VALUES
(1, 15, 'AMI/1/03/20', '2020-03-25', 'ATK', 0, 0, 1600000, 0, 0, 0, 0),
(2, 15, 'AMI/2/03/20', '2020-03-26', 'INSTALL WIFI', NULL, NULL, 1000000, NULL, NULL, NULL, 1),
(3, 18, 'pb/3/03/20', '2020-03-26', 'ATK', NULL, NULL, 1000000, NULL, NULL, NULL, 0),
(4, 18, 'pb/4/03/20', '2020-03-26', 'BAYAR AIR', NULL, NULL, 1000000, NULL, NULL, NULL, 0),
(5, 18, 'pb/5/03/20', '2020-03-26', 'BAYAR LISTRIK', NULL, NULL, 1000000, NULL, NULL, NULL, 0),
(6, 15, 'A/3/03/20', '2020-03-26', 'IKLAN', 0, 0, 0, 2000000, 0, 0, 0),
(7, 15, 'A/7/03/20', '2020-03-26', 'PROMOSI', 0, 0, 0, 1000000, 0, 0, 0),
(8, 15, 'A/8/03/20', '2020-03-26', 'IKLAN RADIO', 0, 0, 0, 3000000, 0, 0, 0),
(9, 15, 'A/9/04/20', '2020-04-06', 'DP,Rumah Mawar 4', 0, 0, 0, 0, 0, 900000, 0),
(10, 15, 'A/10/04/20', '2020-04-09', 'Cicilan Pertama,Rumah Mawar 4', 0, 0, 0, 0, 0, 525000, 0),
(11, 20, 'PA/1/06/20', '2020-06-28', 'makan', 0, 0, 200000, 0, 0, 0, 0),
(12, 20, 'PA/2/06/20', '2020-06-28', 'makan lagi', 0, 0, 100000, 0, 0, 0, 0),
(13, 20, 'PA/3/06/20', '2020-06-22', 'KJH,rumah makan', 0, 0, 0, 0, 0, 7500000, 0),
(14, 20, 'PA/4/06/20', '2020-06-28', 'cicilan 1,rumah makan', 0, 0, 0, 0, 0, 5000, 0),
(15, 20, 'PA/5/07/20', '2020-07-04', 'cicilan ke 4,rumah makan', 0, 0, 0, 0, 0, 300000, 0),
(16, 20, 'PA/7/07/20', '2020-07-07', 'cicilan ke 5,rumah makan', 0, 0, 0, 0, 0, 600000, 0),
(17, 15, 'A/1/07/20', '2020-07-07', 'fasum fasos', 0, 2000000, 0, 0, 0, 0, 0),
(18, 15, 'A/8/07/20', '2020-07-07', 'termin1,Rumah Diana 2', 0, 0, 0, 0, 0, 300000, 0),
(19, 17, 'PC/1/07/20', '2020-07-07', 'keterangan,citra1', 0, 0, 0, 0, 0, 900000, 0),
(20, 17, 'PC/10/07/20', '2020-07-07', 'keterangan,citra1', 0, 0, 0, 0, 0, 900000, 0),
(21, 19, 'PGL/1/06/20', '2020-06-01', 'Termin 1,Griya 2', 0, 0, 0, 0, 0, 300000, 0),
(22, 19, 'PGL/2/07/20', '2020-07-08', 'Termin 2,Griya 2', 0, 0, 0, 0, 0, 375000, 0),
(23, 19, 'PGL/3/06/20', '2020-06-01', 'Termin 1,Griya 2', 0, 0, 0, 0, 0, 300000, 0),
(24, 18, 'PB/6/07/20', '2020-07-08', 'Termin1,Bunga1', 0, 0, 0, 0, 0, 300000, 0),
(25, 18, 'PB/5/07/20', '2020-07-08', 'termin2,Bunga1', 0, 0, 0, 0, 0, 450000, 1),
(26, 15, 'A/9/07/20', '2020-07-25', 'pengeluaran', 0, 13000000, 0, 0, 0, 0, 0),
(27, 15, 'A/7/07/20', '2020-07-27', 'keterangan', 0, 182600000, 0, 0, 0, 0, 1),
(28, 15, 'A/8/07/20', '2020-07-29', 'tytyyt', 2, 0, 0, 0, 0, 0, 1),
(29, 15, 'A/7/01/21', '2021-01-05', 'asdasd', 0, 1000000, 0, 0, 0, 0, 0),
(30, 15, 'A/10/01/21', '2021-01-05', 'OPERASIONAL', 0, 0, 50000000, 0, 0, 0, 0),
(36, 23, 'PPB/1/01/21', '2021-01-19', 'asas', 2300000, 0, 0, 0, 0, 0, 0),
(37, 23, 'PPB/37/01/21', '2021-01-19', 'asas', 2300000, 0, 0, 0, 0, 0, 0),
(38, 23, 'PPB/3/01/21', '2021-01-19', 'sss', 2300000, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `perjanjian`
--

CREATE TABLE `perjanjian` (
  `id_perjanjian` int(11) NOT NULL,
  `id_proyek` int(11) NOT NULL,
  `id_transaksi` int(11) NOT NULL,
  `id_rumah` int(11) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tanggal_perjanjian` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_archive` int(1) NOT NULL DEFAULT '0',
  `tgl_jatuh_tempo` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `perjanjian`
--

INSERT INTO `perjanjian` (`id_perjanjian`, `id_proyek`, `id_transaksi`, `id_rumah`, `keterangan`, `jumlah`, `tanggal_perjanjian`, `is_archive`, `tgl_jatuh_tempo`) VALUES
(1, 15, 9, 11, 'l;klnkkkkkkkkkkk', 20000000, '2020-08-11 03:01:56', 0, '2020-08-11'),
(2, 15, 3, 4, 'test', 20000000, '2020-08-11 03:23:20', 0, '2020-08-11'),
(3, 15, 10, 13, 'termin1', 10000000, '2020-09-10 01:39:44', 0, '2020-09-10'),
(4, 15, 10, 13, 'termin2', 100000000, '2020-09-10 01:40:12', 0, '2020-10-14'),
(5, 15, 13, 19, 'DP', 20000000, '2021-01-05 07:15:22', 0, '2021-01-05');

-- --------------------------------------------------------

--
-- Table structure for table `proyek`
--

CREATE TABLE `proyek` (
  `id_proyek` int(11) NOT NULL,
  `nama_proyek` varchar(100) NOT NULL,
  `is_archive` int(1) NOT NULL DEFAULT '0',
  `foto` varchar(250) DEFAULT NULL,
  `lokasi` varchar(200) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proyek`
--

INSERT INTO `proyek` (`id_proyek`, `nama_proyek`, `is_archive`, `foto`, `lokasi`, `date`) VALUES
(10, 'a', 1, '', NULL, '2020-10-28 08:24:03'),
(11, 'a', 1, '', NULL, '2020-10-28 08:24:03'),
(12, 'coba', 1, '', NULL, '2020-10-28 08:24:03'),
(15, 'Amikom', 0, '1596019633913.png', 'Yogyakarta', '2020-10-29 06:30:59'),
(16, 'dummyklklk', 1, '1584957059107.png', NULL, '2020-10-28 08:24:03'),
(17, 'perumahan citra', 0, '1584876947476.png', NULL, '2020-10-28 08:24:03'),
(18, 'perumahan bunga', 0, '1584884739823.png', NULL, '2020-10-28 08:24:03'),
(19, 'perumahan griya lestari', 0, '1585116585175.png', NULL, '2020-10-28 08:24:03'),
(20, 'Paling Anyar', 0, '1594095107249.png', NULL, '2020-10-28 08:24:03'),
(21, 'Diggy Office', 1, '1604563736106.png', 'Yogyakarta', '2021-01-05 01:35:11'),
(22, 'HAMBALANG', 0, '1608629975167.png', 'JAKARTA', '2020-12-22 09:39:35'),
(23, 'PERUMAHAN PURI BALI', 0, '1609815114955.png', 'Seminyak, Bali', '2021-01-05 02:51:55'),
(24, 'PERUMAHAN GRAHA ASRI', 0, '1609830654870.png', 'BEKASI', '2021-01-05 07:10:55');

-- --------------------------------------------------------

--
-- Table structure for table `rumah`
--

CREATE TABLE `rumah` (
  `id_rumah` int(11) NOT NULL,
  `id_proyek` int(11) NOT NULL,
  `nama_rumah` varchar(50) NOT NULL,
  `tipe_rumah` varchar(50) NOT NULL,
  `harga_rumah` int(11) NOT NULL,
  `is_archive` int(1) NOT NULL DEFAULT '0',
  `is_buy` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rumah`
--

INSERT INTO `rumah` (`id_rumah`, `id_proyek`, `nama_rumah`, `tipe_rumah`, `harga_rumah`, `is_archive`, `is_buy`) VALUES
(1, 15, 'Rumah Mawar 4', 'L100', 250000000, 0, 1),
(2, 15, 'Rumah Anggrek 2', 'L200', 200000000, 0, 1),
(3, 15, 'Rumah Delima 1', 'T200', 350000000, 1, 0),
(4, 15, 'Rumah Diana 2', 'L100', 150000000, 0, 1),
(5, 20, 'rumah makan', 'L300', 250000000, 0, 1),
(6, 17, 'citra1', 'l300', 200000000, 0, 1),
(7, 19, 'Griya 1', 'L300', 200000000, 0, 0),
(8, 19, 'Griya 2', 'L400', 250000000, 0, 1),
(9, 18, 'Bunga1', 'L90', 250000000, 0, 1),
(10, 20, 'RUMAH-A', 'L300', 350000000, 0, 0),
(11, 15, 'RUMAH-A', 'L300', 500000000, 0, 1),
(12, 16, 'RUMAH-A', 'L300', 400000000, 0, 1),
(13, 15, 'Rumah Dahlia', 'a4', 800000000, 0, 1),
(14, 18, 'RUMAH-A', 'L300', 350000000, 0, 0),
(15, 21, 'Rumah Idaman', 'E3', 700000000, 0, 1),
(16, 21, 'Rumah Mawar', 'E4', 650000000, 0, 0),
(17, 22, 'RUMAH-A', 'L300', 350000000, 0, 1),
(18, 22, 'RUMAH-B', 'E3', 350000000, 0, 0),
(19, 15, 'RUMAH-B', 'E3', 350000000, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_rumah`
--

CREATE TABLE `transaksi_rumah` (
  `id_transaksi` int(11) NOT NULL,
  `nama_pembeli` varchar(50) NOT NULL,
  `alamat_pembeli` varchar(100) NOT NULL,
  `no_ktp` varchar(50) NOT NULL,
  `no_kk` varchar(50) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `id_rumah` int(11) NOT NULL,
  `is_lunas` int(1) NOT NULL DEFAULT '0',
  `trx_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `jatuh_tempo` varchar(2) NOT NULL,
  `termin_perbulan` int(11) NOT NULL,
  `plan_termin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_rumah`
--

INSERT INTO `transaksi_rumah` (`id_transaksi`, `nama_pembeli`, `alamat_pembeli`, `no_ktp`, `no_kk`, `no_telp`, `id_rumah`, `is_lunas`, `trx_date`, `jatuh_tempo`, `termin_perbulan`, `plan_termin`) VALUES
(1, 'Aldo', 'Jogja', '98989898989', '9898989999', '1234', 1, 1, '2020-10-29 08:29:11', '25', 2000000, 20),
(2, 'ANTO', 'JOGJA', '123', '123', '1234', 5, 0, '2020-07-26 05:49:29', '25', 2500000, 20),
(3, 'Ari', 'Pesido', '1234', '1234', '1234', 4, 0, '2020-07-26 05:49:29', '25', 1500000, 20),
(4, 'luluk', 'jogja', '1234', '1234', '12345', 6, 0, '2020-07-26 05:49:29', '25', 2500000, 20),
(5, 'Burhan', 'Papua', '1234', '1234', '1234', 8, 0, '2020-07-26 05:49:29', '25', 2000000, 20),
(6, 'Jhon', 'Semarang', '1234', '1234', '12345', 9, 0, '2020-07-26 05:49:29', '25', 3000000, 20),
(7, 'Lisa', 'Korea', '89898989898', '898989898989', '0123456789', 2, 0, '2020-07-27 15:20:56', '25', 2500000, 20),
(8, 'joe', 'wn', '1234', '1234', '1234', 12, 0, '2020-07-30 05:13:04', '24', 2500000, 30),
(9, 'Patrich Wanggai', 'Papua', '1234', '12345', '123456', 11, 0, '2020-08-11 01:24:24', '', 0, 0),
(10, 'Mustofa', 'Pudak', '123456', '123456', '08080808080', 13, 0, '2020-09-10 01:37:53', '', 0, 0),
(11, 'Dina Fauziyah', 'Sleman', '123456', '123456', '123456', 15, 0, '2020-11-05 17:46:16', '', 0, 0),
(12, 'DIMAS', 'JOGJA', '12345678', '12345678', '12345678', 17, 0, '2020-12-22 09:42:06', '', 0, 0),
(13, 'Gustu Maulana', 'Patuk', '12345', '12345', '12345', 19, 0, '2021-01-05 07:14:32', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `trans_admin_proyek`
--

CREATE TABLE `trans_admin_proyek` (
  `id_admin` int(11) NOT NULL,
  `id_proyek` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_admin_proyek`
--

INSERT INTO `trans_admin_proyek` (`id_admin`, `id_proyek`) VALUES
(1, 9),
(2, 9),
(1, 9),
(2, 9),
(1, 13),
(2, 13),
(1, 14),
(2, 14),
(3, 17),
(1, 18),
(2, 18),
(2, 16),
(1, 19),
(3, 19),
(3, 20),
(1, 15),
(2, 15),
(3, 15),
(3, 21),
(2, 21),
(1, 22),
(3, 22),
(1, 23),
(1, 24),
(2, 24);

-- --------------------------------------------------------

--
-- Table structure for table `trans_mandor_proyek`
--

CREATE TABLE `trans_mandor_proyek` (
  `id_mandor` int(11) NOT NULL,
  `id_proyek` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_mandor_proyek`
--

INSERT INTO `trans_mandor_proyek` (`id_mandor`, `id_proyek`) VALUES
(1, 9),
(2, 9),
(1, 9),
(2, 9),
(1, 13),
(2, 13),
(1, 14),
(2, 14),
(2, 17),
(1, 18),
(1, 0),
(2, 16),
(1, 19),
(2, 19),
(1, 20),
(1, 15),
(2, 15),
(3, 21),
(1, 21),
(3, 22),
(1, 22),
(1, 23),
(1, 24),
(2, 24);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `aksi_request_pengeluaran`
--
ALTER TABLE `aksi_request_pengeluaran`
  ADD PRIMARY KEY (`id_request`);

--
-- Indexes for table `budget`
--
ALTER TABLE `budget`
  ADD PRIMARY KEY (`id_budget`);

--
-- Indexes for table `direksi`
--
ALTER TABLE `direksi`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id_log`);

--
-- Indexes for table `mandor`
--
ALTER TABLE `mandor`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `pemasukan`
--
ALTER TABLE `pemasukan`
  ADD PRIMARY KEY (`id_pemasukan`),
  ADD KEY `id_proyek` (`id_proyek`),
  ADD KEY `id_rumah` (`id_rumah`),
  ADD KEY `id_transaksi` (`id_transaksi`);

--
-- Indexes for table `pengeluaran`
--
ALTER TABLE `pengeluaran`
  ADD PRIMARY KEY (`id_pengeluaran`),
  ADD KEY `id_proyek` (`id_proyek`);

--
-- Indexes for table `perjanjian`
--
ALTER TABLE `perjanjian`
  ADD PRIMARY KEY (`id_perjanjian`);

--
-- Indexes for table `proyek`
--
ALTER TABLE `proyek`
  ADD PRIMARY KEY (`id_proyek`);

--
-- Indexes for table `rumah`
--
ALTER TABLE `rumah`
  ADD PRIMARY KEY (`id_rumah`),
  ADD KEY `id_proyek` (`id_proyek`);

--
-- Indexes for table `transaksi_rumah`
--
ALTER TABLE `transaksi_rumah`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_rumah` (`id_rumah`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `aksi_request_pengeluaran`
--
ALTER TABLE `aksi_request_pengeluaran`
  MODIFY `id_request` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `budget`
--
ALTER TABLE `budget`
  MODIFY `id_budget` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `direksi`
--
ALTER TABLE `direksi`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=235;

--
-- AUTO_INCREMENT for table `mandor`
--
ALTER TABLE `mandor`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pemasukan`
--
ALTER TABLE `pemasukan`
  MODIFY `id_pemasukan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `pengeluaran`
--
ALTER TABLE `pengeluaran`
  MODIFY `id_pengeluaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `perjanjian`
--
ALTER TABLE `perjanjian`
  MODIFY `id_perjanjian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `proyek`
--
ALTER TABLE `proyek`
  MODIFY `id_proyek` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `rumah`
--
ALTER TABLE `rumah`
  MODIFY `id_rumah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `transaksi_rumah`
--
ALTER TABLE `transaksi_rumah`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pemasukan`
--
ALTER TABLE `pemasukan`
  ADD CONSTRAINT `pemasukan_ibfk_1` FOREIGN KEY (`id_proyek`) REFERENCES `proyek` (`id_proyek`),
  ADD CONSTRAINT `pemasukan_ibfk_2` FOREIGN KEY (`id_rumah`) REFERENCES `rumah` (`id_rumah`),
  ADD CONSTRAINT `pemasukan_ibfk_3` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksi_rumah` (`id_transaksi`);

--
-- Constraints for table `pengeluaran`
--
ALTER TABLE `pengeluaran`
  ADD CONSTRAINT `pengeluaran_ibfk_2` FOREIGN KEY (`id_proyek`) REFERENCES `proyek` (`id_proyek`);

--
-- Constraints for table `rumah`
--
ALTER TABLE `rumah`
  ADD CONSTRAINT `rumah_ibfk_1` FOREIGN KEY (`id_proyek`) REFERENCES `proyek` (`id_proyek`);

--
-- Constraints for table `transaksi_rumah`
--
ALTER TABLE `transaksi_rumah`
  ADD CONSTRAINT `transaksi_rumah_ibfk_1` FOREIGN KEY (`id_rumah`) REFERENCES `rumah` (`id_rumah`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
